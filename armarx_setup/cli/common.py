import os
import tempfile
import shutil
import sys

import typing as ty
import rich_click as click

from armarx_setup import console, PACKAGE_ROOT, RELEASE_CHANNEL_STABLE
from armarx_setup.core import logo
from armarx_setup.core.config import config as axii_config
from armarx_setup.core.util import commands


disable_prompts = False


def ensure_prompts_enabled():
    if disable_prompts:
        raise click.UsageError("Prompts were disabled.")


def input_confirm(prompt, default) -> bool:
    ensure_prompts_enabled()

    return input_option(prompt, options=["y", "n"], default_option="y" if default else "n") == "y"


def input_edit(prompt, edit):
    ensure_prompts_enabled()

    import readline
    readline.set_startup_hook(lambda: readline.insert_text(edit))
    try:
        return input(prompt)
    finally:
        readline.set_startup_hook()


def input_option(prompt, options, default_option) -> str:
    """
    Query the user for one of several options.

    Parameters
    ----------
    prompt: Prompt to show to the user.
    options: The options the user should choose from.
    default_option: The default option (if user enters nothing). Well be prepended if not element of options.

    Returns
    -------
    decision, which is element of options (or default_option or abort_option)

    """

    ensure_prompts_enabled()

    if default_option not in options:
        options = [default_option] + options

    options_str = [
        f"[b]{option.upper()}[/]" if option == default_option else option for option in options
    ]

    decision = ""
    while decision not in options:
        decision = console.input(f"{prompt} [{'/'.join(options_str)}] > ").lower()
        if decision == "":
            decision = default_option
    return decision


def format_help(self, ctx: click.Context, formatter: click.HelpFormatter) -> None:
    print_logo()

    from rich_click import rich_click
    rich_click.rich_format_help(self, ctx, formatter)


class AxiiClickGroup(click.Group):
    def format_help(self, ctx: click.Context, formatter: click.HelpFormatter) -> None:
        format_help(self, ctx, formatter)

    def get_command(self, ctx, cmd_name):
        def edit_distance_is_deletions_only(command, shorthand):
            """Returns true if the edit distance of command to shorthand consists of deletions only."""

            def lcs(str1, str2, m, n):
                L = [[0 for _ in range(n + 1)]
                     for _ in range(m + 1)]

                for i in range(m + 1):
                    for j in range(n + 1):
                        if i == 0 or j == 0:
                            L[i][j] = 0
                        elif str1[i - 1] == str2[j - 1]:
                            L[i][j] = L[i - 1][j - 1] + 1
                        else:
                            L[i][j] = max(L[i - 1][j], L[i][j - 1])

                return L[m][n]

            m = len(command)
            n = len(shorthand)
            length = lcs(command, shorthand, m, n)

            deletions = m - length
            insertions = n - length

            return deletions >= 0 and insertions == 0

        rv = click.Group.get_command(self, ctx, cmd_name)
        if rv is not None:
            return rv
        matches = [command for command in self.list_commands(ctx)
                   if edit_distance_is_deletions_only(command, cmd_name) and command.startswith(cmd_name[0])]
        if not matches:
            return None
        elif len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])
        ctx.fail(f"Too many matches. Did you mean any of these: {', '.join(sorted(matches))}?")

    def resolve_command(self, ctx, args):
        # always return the full command name
        _, cmd, args = super().resolve_command(ctx, args)
        return cmd.name, cmd, args


class AxiiClickCommand(click.Command):
    def format_help(self, ctx: click.Context, formatter: click.HelpFormatter) -> None:
        format_help(self, ctx, formatter)


group_kwargs = dict(cls=AxiiClickGroup)
command_kwargs = dict(cls=AxiiClickCommand)


logo_printed = False


def ensure_installation():
    axii_installed = os.environ.get("_axii_installed", False)
    if not axii_installed:
        axii_exec = os.path.join(PACKAGE_ROOT, "bin", "axii")
        console.print(f"[red]Error:[/] Axii is not properly installed. Do so by running the following command and "
                      f"follow the steps:")
        print_commands([
            f"{axii_exec} self install --$0"
        ])

        console.print("Afterwards, try re-executing the current command:")
        print_commands([
            f"axii {' '.join(sys.argv[1:])}"
        ])

        sys.exit(1)


def print_logo():
    # Avoid printing the logo multiple times.
    global logo_printed
    if not logo_printed:
        from armarx_setup.core.git import repo_status

        logo_printed = True
        dirty = True
        dev_mode = axii_config.get_global("dev_mode")
        release_channel = "unknown"
        stable_release_channel = RELEASE_CHANNEL_STABLE
        axii_repo_status = repo_status(PACKAGE_ROOT, fetch=False)
        commits_ahead = 0
        commits_behind = 0
        if axii_repo_status is not None:
            dirty = axii_repo_status.active_branch != stable_release_channel
            release_channel = axii_repo_status.active_branch
            commits_ahead = axii_repo_status.commits_ahead
            commits_behind = axii_repo_status.commits_behind
        alt_mode = not dev_mode and dirty
        console.print(logo.make_logo_in_panel(alt_mode=alt_mode, title=f"[i]Welcome to Axii[/]"))

        diagnostics = []

        if dev_mode:
            diagnostics.append(f"[b]Developer mode, on '{release_channel}'[/] (ahead {commits_ahead}, behind "
                               f"{commits_behind}).")
        elif dirty:
            diagnostics.append("[b][red]UNSUPPORTED VERSION[/], switch to stable channel with "
                               f"'axii self change-release {stable_release_channel}'[/].")

        if commits_behind > 0:
            diagnostics.append(f"[yellow]Update available![/] Please update Axii using 'axii self update'.")

        if len(diagnostics) > 0:
            for line in diagnostics:
                console.print(line, justify="center")
            console.print()


# Local aliases to use names as keyword arguments in bootstrap and still be able to call those functions.
ensure_installation_function = ensure_installation
print_logo_function = print_logo


def bootstrap(ensure_installation: bool = True, print_logo: bool = True):
    if ensure_installation:
        ensure_installation_function()
    if print_logo:
        print_logo_function()


def open_file_in_editor(config_file_path: str, kind: str = "", tmp_file_name=""):
    assert os.path.isfile(config_file_path), f"Not a file: {config_file_path}."

    editor = os.environ.get("EDITOR", "editor")

    console.print(f"Edit {kind} file '{config_file_path}' ...")

    # rb as tf.write() requires binary content.
    with open(config_file_path, "rb") as source:
        content = source.read()

    tmp_file_name = tmp_file_name.replace("/", "∕")

    with tempfile.NamedTemporaryFile(prefix="tmpfile: ", suffix=f" - {tmp_file_name}.json", delete=False) as tf:
        tf.write(content)
        tf.flush()

        commands.run(f'{editor} "{tf.name}"', quiet=True)
        tf.close()

        console.print(f"Save {kind} at '{config_file_path}' ...")
        shutil.move(tf.name, config_file_path)


def print_list(list_, numbered=False):
    console.print()
    number = 1
    for item in list_:
        bullet = number if numbered else "-"
        console.print(f"  {bullet} {item}")
        number += 1
    console.print()


def print_dictionary(dictionary):
    console.print()
    for key, value in dictionary.items():
        console.print(f"  - {key}: [i #777777]{value}[/]")
    console.print()


def print_commands(command_list):
    rich_print_command_args = {
        "crop": False,
        "no_wrap": True,
        "overflow": "ignore"
    }

    console.print()
    for command in command_list:
        console.print(f"    {command}", **rich_print_command_args)
    console.print()


def make_decorator(options: ty.List[ty.Callable]) -> ty.Callable:
    def decorator(func):
        for option in reversed(options):
            func = option(func)
        return func

    return decorator


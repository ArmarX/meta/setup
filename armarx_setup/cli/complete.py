# Important! For performance reasons, do not import any modules in the top level here, but only in the function scope.


def filter_completion(options: "Iterable[str]", incomplete: str):
    return list(filter(lambda o: o.startswith(incomplete), options))


def list_modules(ctx, param, incomplete: str) -> "List[str]":
    from armarx_setup.core.module import Module

    return filter_completion(Module.get_all_module_names(), incomplete)


def list_features(ctx: "click.Context", param, incomplete: str) -> "List[str]":
    from armarx_setup.core import error
    from armarx_setup.core.module import Module

    module_name: str = ctx.params["module_name"]
    feature_names: "List[str]" = ctx.params["feature_names"]

    try:
        m = Module.load_info(module_name)
    except error.ModuleInfoNotFound:
        return []

    features = set(m.features).difference(feature_names)
    return sorted(filter_completion(features, incomplete))


def list_modules_in_workspace(ctx, param, incomplete: str) -> "List[str]":
    from armarx_setup.core.workspace import Workspace

    try:
        ws = Workspace.load_active_workspace()
        return filter_completion(ws.config.modules, incomplete)
    except:
        return []


def list_all_modules_in_workspace(ctx, param, incomplete: str) -> "List[str]":
    from armarx_setup.core.workspace import Workspace

    try:
        ws = Workspace.load_active_workspace()
        names = []
        ws.for_each_module_in_alphabetical_order(lambda m: names.append(m.name))
        return filter_completion(names, incomplete)
    except:
        return []

def list_workspaces(ctx, param, incomplete: str) -> "List[str]":
    from armarx_setup.core.workspace import Workspace

    return filter_completion(Workspace.get_all_workspace_names(), incomplete)

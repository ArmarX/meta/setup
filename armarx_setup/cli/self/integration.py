import enum

import armarx_setup
from armarx_setup import console
from armarx_setup.cli import common as common_cli
from armarx_setup.core import error
from armarx_setup.self import install as self_install


def install(auto_detect=True, zsh=False, bash=False):
    shell_list = self_install.shell_list(auto_detect=auto_detect, zsh=zsh, bash=bash)

    for shell in shell_list:
        install_axii_script_path = self_install.get_install_axii_script_path()
        rc_file_path = self_install.get_rc_file_path(shell)
        console.print(f"Install Axii for {shell} into '{rc_file_path}'.")

        try:
            install_for_shell(shell, rc_file_path, install_axii_script_path)
        except error.NoShellRunComFile as e:
            console.print(f"[red]Error[/]: {e}")


def install_for_shell(shell, rc_file_path, install_axii_script_path):
    if not self_install.is_snippet_installed(shell, rc_file_path, install_axii_script_path):
        installation_snippet = self_install.get_installation_snippet(install_axii_script_path)

        console.print(f"This will append the following lines to '{rc_file_path}':")
        common_cli.print_commands(installation_snippet[1:-1])
        maybe_y = console.input(f"Continue? y/n: ")
        if maybe_y.lower() in ['y', 'yes']:
            console.print(f"Append snippet to '{rc_file_path}'.")

            self_install.install_snippet_into(shell, rc_file_path, installation_snippet)

            console.print(f"Installed Axii for {shell}.")
            console.print()
            console.print(f"[yellow]Important[/]: For the changes to take effect, please close and re-open all "
                          f"terminals. Alternatively, re-source your '{rc_file_path}' in each of them:")
            common_cli.print_commands([
                f"source {rc_file_path}"
            ])
    else:
        console.print(f"Axii for {shell} already installed in '{rc_file_path}'.")
        console.print()


def update(**kwargs):
    from armarx_setup import self

    self.update(**kwargs)


def change_release(release_channel):
    from armarx_setup import self
    from armarx_setup.cli.common import print_logo

    self.change_release(release_channel=release_channel)

    # Print logo here after branch is switched.
    print_logo()

    console.print(f"Successfully switched to release channel '{release_channel}'.")




class MessageFormat(enum.Enum):
    PLAIN = enum.auto()
    MARKDOWN = enum.auto()
    HTML = enum.auto()


def print_release_message_with_options(
        **kwargs,
):
    from armarx_setup.core import error

    selected = []
    for mf in MessageFormat:
        if kwargs.pop(mf.name.lower()):
            selected.append(mf)

    if len(selected) > 1:
        raise error.ArmarXSetupError(
            f"Please specify only one format (instead of {', '.join(f.name.lower() for f in selected)}).")
    if len(selected) == 1:
        format_ = selected[0]
    else:
        format_ = MessageFormat.PLAIN

    print_release_message(format_=format_, **kwargs)


def print_release_message(
        format_: MessageFormat,
        clipboard: bool,
):
    from rich.rule import Rule
    import subprocess as sp

    version = armarx_setup.__version__

    release_notes_url = f"https://gitlab.com/ArmarX/meta/setup/-/tags/{version}"
    create_issue_url = f"https://gitlab.com/ArmarX/meta/setup/-/issues/new"

    template = """
    Hi everybody,

    We just released Axii version {version}. You can just update via `axii self update`.

    For more information and detailed update procedure, see the {release_notes}.

    As always, if you experience any problems, feel free to {create_an_issue} or contact us.

    Thanks to everyone who contributed with module definitions and updates.

    Best,

    Christian and Rainer
    """
    template = "\n".join(line.strip() for line in template.strip().split("\n"))

    format_kwargs = dict(version=version)
    if format_ == MessageFormat.HTML:
        text = template.format(
            release_notes=f'<a href="{release_notes_url}">release notes</a>',
            create_an_issue=f'<a href="{create_issue_url}">create an issue</a>',
            **format_kwargs,
        )
        text = text.replace("\n", "<br>")

    elif format_ == MessageFormat.MARKDOWN:
        text = template.format(
            release_notes=f"\\[release notes]({release_notes_url})",
            create_an_issue=f"\\[create an issue]({create_issue_url})",
            **format_kwargs,
        )

    elif format_ == MessageFormat.PLAIN:
        text = template.format(
            release_notes=f"release notes [1]",
            create_an_issue=f"create an issue [2]",
            **format_kwargs,
        )
        text += "".join([
            f"\n",
            f"\n[1] {release_notes_url}",
            f"\n[2] {create_issue_url}",
        ])

    else:
        raise ValueError(format_)

    console.print(Rule(title=f"Release message for version '{version}'"))
    console.print(text)
    console.print(Rule())

    if clipboard:
        command = "xclip -selection clipboard"

        if format_ == MessageFormat.HTML:
            command += " -t text/html"
        if format_ == MessageFormat.MARKDOWN:
            # These escapes were just for rich.
            text = text.replace("\\[", "[")

        with sp.Popen([command], shell=True, stdin=sp.PIPE) as process:
            process.communicate(input=text.encode())
            console.print("Copied the text to the clipboard.")

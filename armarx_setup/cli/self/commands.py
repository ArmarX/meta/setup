import rich_click as click

from armarx_setup.cli import common
from armarx_setup.cli.self import integration
from armarx_setup.core.config import config as axii_config


COMMAND_GROUPS = []  # Default.


@click.group(**common.group_kwargs)
def self():
    """Interact with Axii itself."""

    pass


@self.command("install", **common.command_kwargs)
@click.option("--auto-detect/--no-auto-detect", is_flag=True, default=True,
              help="Install and auto-detect shell from $SHELL.")
@click.option("--zsh", is_flag=True, default=False, help="Install for zsh.")
@click.option("--bash", is_flag=True, default=False, help="Install for bash.")
def self_install(**kwargs):
    """Install Axii in your shell."""

    common.bootstrap(ensure_installation=False, print_logo=True)

    integration.install(**kwargs)


@self.command("update", **common.command_kwargs)
@click.option("--only-check", is_flag=True, default=False,
              help="Only check if updates are available, but do nothing.")
def self_update(**kwargs):
    """Update Axii itself."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    integration.update(**kwargs)


@self.command("change-release", **common.command_kwargs)
@click.argument("release-channel")
def self_change_release(**kwargs):
    """Change Axii's release channel."""

    # Don't print logo here but after switching the branch.
    common.bootstrap(ensure_installation=True, print_logo=False)

    integration.change_release(**kwargs)


if axii_config.get_global("dev_mode"):
    @self.group("maintain", **common.group_kwargs)
    def self_maintain():
        """Tools for Axii maintenance."""
        pass

    @self_maintain.command("print-release-message", **common.command_kwargs)
    @click.option("--plain", is_flag=True, default=False,
                  help="Print in plain format (default).")
    @click.option("--html", is_flag=True, default=False,
                  help="Print in HTML format.")
    @click.option("--markdown", is_flag=True, default=False,
                  help="Print in markdown format.")
    @click.option("--clipboard", is_flag=True, default=False,
                  help="Copy the message to the clipboard.")
    def self_maintain_print_release_message(**kwargs):
        """Print the release message for the current version."""

        common.bootstrap(ensure_installation=True, print_logo=False)
        integration.print_release_message_with_options(**kwargs)

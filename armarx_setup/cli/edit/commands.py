import rich_click as click

from armarx_setup.cli import common, complete
from armarx_setup.cli.edit import integration
from armarx_setup.core import error


COMMAND_GROUPS = []  # Default.


@click.group("edit", **common.group_kwargs)
def edit():
    """Edit configuration files."""

    pass


@edit.command("module-definition", **common.command_kwargs)
@click.argument("module_name", shell_complete=complete.list_modules)
def edit_module_definition(**kwargs):
    """Open the definition file of a specific module."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    from armarx_setup.cli.module import integration as module_integration

    try:
        integration.edit_module_config(**kwargs)
    except error.ModuleInfoNotFound as e:
        module_integration.module_info_not_found(e, suggest_create_new=True)


@edit.command("workspace-config", **common.command_kwargs)
@click.argument("workspace_name", default=None, required=False, shell_complete=complete.list_workspaces)
def edit_workspace_config(**kwargs):
    """Open the configuration file of the active workspace or one supplied via parameter."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    from armarx_setup.cli.workspace import integration as workspace_integration

    try:
        integration.edit_workspace_config(**kwargs)
    except error.NoSuchWorkspace as e:
        workspace_integration.no_such_workspace(e)
    except error.NoWorkspaceActive:
        raise click.UsageError("No workspace active and no workspace name supplied.")


@edit.command("global-config", **common.command_kwargs)
def edit_global_config(**kwargs):
    """Open the global Axii configuration file."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    integration.edit_global_config(**kwargs)

from armarx_setup.cli import common


def edit_module_config(module_name):
    """Open the configuration of a specific module."""

    # Edit the module_config.
    from armarx_setup.core.module.module import Module
    m = Module(module_name)
    tmp_file_name = f"module: {module_name}"
    return common.open_file_in_editor(m.get_info_path(module_name), kind="module definition",
                                      tmp_file_name=tmp_file_name)


def edit_workspace_config(workspace_name):
    from armarx_setup.core.workspace import Workspace

    if workspace_name is not None:
        ws = Workspace.load_workspace_by_name(workspace_name)
    else:
        ws = Workspace.load_active_workspace()

    tmp_file_name = f"workspace: {ws.name} - armarx-workspace"
    return common.open_file_in_editor(ws.get_config_path(), kind="workspace configuration", tmp_file_name=tmp_file_name)


def edit_global_config(**kwargs):
    from armarx_setup.core.config import config

    tmp_file_name = "global_config"
    common.open_file_in_editor(config.global_config_file_path, kind="global configuration", tmp_file_name=tmp_file_name)

import os
import sys
from typing import Any, Callable, Optional

from xdg import xdg_cache_home

from armarx_setup import console
from armarx_setup.cli import common
from armarx_setup.core import error


def get_workspace_by_name_or_active(workspace_name: Optional[str] = None) -> "Workspace":
    from armarx_setup.core.workspace import Workspace

    if workspace_name is not None:
        return Workspace.load_workspace_by_name(workspace_name)
    else:
        return Workspace.load_active_workspace()


def add(**kwargs):
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    ws.add_module(**kwargs)


def remove(**kwargs):
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    ws.remove_module(**kwargs)


def add_features(**kwargs):
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    ws.add_module_features(**kwargs)


def remove_features(**kwargs):
    raise error.NotYetImplemented(feature="remove module features")


def create_workspace(workspace_path=None, workspace_name=None, disable_prompts=False) -> "Workspace":
    if workspace_path is None:
        workspace_path = "~/code"

    if workspace_name is None:
        if not disable_prompts:
            ws = create_workspace_dialog(workspace_path)
        else:
            console.print("Cannot create workspace.")
            sys.exit(1)
    else:
        from armarx_setup.core.workspace import Workspace
        from armarx_setup.core.workspace.config import Config
        ws = Workspace(name=workspace_name, path=workspace_path, config=Config.get_defaults())

    console.print()
    console.print(f"Create workspace at '{ws.path}' with config: ")
    info = ws.create()
    common.print_dictionary(info)
    console.print("Successfully created the workspace. If you want to make this workspace the default workspace, run: ")
    common.print_commands([
        f"axii config --global default_workspace {ws.name}"
    ])

    return ws


def create_workspace_dialog(path) -> "Workspace":
    from armarx_setup.core.workspace.config import Config
    from armarx_setup.core.workspace.workspace import Workspace, ARMARX_WORKSPACE_ENV_VAR

    current_workspace_env_var = os.environ.get(ARMARX_WORKSPACE_ENV_VAR, None)
    if current_workspace_env_var:
        path = current_workspace_env_var

    console.print()
    console.print("Specify the workspace.")

    # Query path.
    path = common.input_edit("Workspace directory: ", path) or path  # Avoid console.input due to bug #78.
    path = os.path.abspath(os.path.expandvars(os.path.expanduser(path)))

    # Query for the workspace name given the path, allow user to modify.
    name = os.path.basename(os.path.normpath(path))
    name_ok = False
    known_workspaces = Workspace.read_known_workspaces()
    while not name_ok:
        name = common.input_edit("Workspace name: ", name) or name
        if name in known_workspaces:
            console.print(f"The name {name} is already taken and pointing to '{known_workspaces[name]['path']}'.")
            name_ok = common.input_confirm("Overwrite?", default=True)
        else:
            name_ok = True

    full_path = os.path.expanduser(os.path.expandvars(path))
    config = Config.get_defaults()

    return Workspace(name=name, path=full_path, config=config)


def list_workspaces(**kwargs):
    from armarx_setup.core.workspace import Workspace

    console.print("Known workspaces:")
    common.print_dictionary({name: info["path"] for name, info in Workspace.read_known_workspaces().items()})


def activate_workspace(workspace_name, cd: bool = True):
    from armarx_setup.core.workspace import Workspace

    cache_dir = os.path.join(xdg_cache_home(), "axii")
    ccmd_file_name = "cached_command.sh"
    ccmd_file = os.path.join(cache_dir, ccmd_file_name)

    os.makedirs(cache_dir, exist_ok=True)

    known_workspaces = Workspace.read_known_workspaces()
    if workspace_name not in known_workspaces:
        raise error.NoSuchWorkspace(workspace_name=workspace_name)

    path = known_workspaces[workspace_name]["path"]
    with open(f"{ccmd_file}~", "w") as f:
        f.write(f"export ARMARX_WORKSPACE=\"{path}\"\n")
        if cd:
            f.write(f"cd \"{path}\"\n")
    os.rename(f"{ccmd_file}~", ccmd_file)


def deactivate_workspace(**kwargs):
    from armarx_setup.core.workspace import Workspace

    cache_dir = os.path.join(xdg_cache_home(), "axii")
    ccmd_file_name = "cached_command.sh"
    ccmd_file = os.path.join(cache_dir, ccmd_file_name)

    os.makedirs(cache_dir, exist_ok=True)

    # Throws if no workspace is active.
    Workspace.get_active_workspace_dir()

    with open(f"{ccmd_file}~", "w") as f:
        f.write(f"_axii_env_reset\n")
        f.write(f"unset ARMARX_WORKSPACE ARMARX_WORKSPACE_NAME\n")
    os.rename(f"{ccmd_file}~", ccmd_file)


def which(**kwargs):
    from armarx_setup.core.workspace import Workspace

    console.print(Workspace.get_active_workspace_dir())


def auto_remove(workspace_name: Optional[str] = None, yes=False):
    ws = get_workspace_by_name_or_active(workspace_name)

    paths_to_remove = ws.auto_remove(dry_run=True)

    if len(paths_to_remove) == 0:
        console.print("Nothing to do.")
        sys.exit(0)

    console.print("The following directories do not correspond to any module in the workspace:")
    common.print_list(paths_to_remove)
    console.print("These directories may originate from old modules that are not active or required anymore.")

    if not yes:
        yes = common.input_confirm("Proceed deleting?", default=False)

    if yes:
        ws.auto_remove(dry_run=False)
        console.print("Done cleaning up.")
    else:
        console.print("Aborted.")


def env(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.generate_env(**kwargs)


def system(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.system(**kwargs)


def update(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.update(**kwargs)


def prepare(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.prepare(**kwargs)


def build(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.build(**kwargs)


def install(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.install(**kwargs)


def conclude(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.conclude(**kwargs)


def upgrade(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.upgrade(**kwargs)


def clean(**kwargs):
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    ws.clean(**kwargs)


def test(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.test(**kwargs)


def git_status(**kwargs):
    from armarx_setup.core import git
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    git.status(ws, **kwargs)


def git_reconfigure(default_checkout: bool = False, yes: bool = False, self: bool = False):
    from armarx_setup.core import git
    from armarx_setup.core.workspace import Workspace
    from armarx_setup.core.util import prefix_color_map
    from rich.progress import Progress

    ws = Workspace.load_active_workspace()
    total = ws.module_graph.number_of_nodes() + (1 if self else 0)
    all_issues = {}

    with Progress(console=console) as progress:
        task = progress.add_task("Assess Git repositories in workspace", total=total)
        for report in git.correct_workspace_configuration_issues(ws, default_checkout=default_checkout, self=self,
                                                                 dry_run=True):
            if report is not None and len(report.issues) > 0:
                all_issues[report.module.name] = report
            progress.update(task, advance=1)

    # Generate report.
    if len(all_issues) > 0:
        console.print()
        if len(all_issues) == 1:
            console.print("An issue with the current workspace was detected.")
        else:
            console.print("Several issues with the current workspace were detected.")
        for module_name, report in all_issues.items():
            module_name_c = prefix_color_map.module_to_colored_text(module_name)
            console.print(f"Configuration issue{'s' if len(report.issues) > 1 else ''} in {module_name_c}:")
            issues_str = [i.to_string_problem() for i in report.issues]
            common.print_list(issues_str)
    else:
        console.print("No configuration issues detected.")
        return

    # Ask user how to proceed.
    if not yes:
        modules = len(all_issues) > 1
        module_or_modules = "all modules" if modules else "module"
        values = modules or len(list(all_issues.values())[0].issues) == 1
        value_or_values = "values" if values else "value"
        prompt = f"Reconfigure {module_or_modules} to expected {value_or_values}?"
        options = ["y", "n"]
        if values:
            options.append("individual")
        decision = common.input_option(prompt, options=options, default_option="y")
        if decision == "n":
            console.print("No configuration corrections were applied.")
            return
        yes = decision == "y"
        console.print()

    # Proceed according to user decision.
    amount_fixed = 0
    for module_name, report in all_issues.items():
        module_name_c = prefix_color_map.module_to_colored_text(module_name)
        for issue in report.issues:
            console.print(f"{module_name_c}, {issue.to_string_correct()}")
            if yes or common.input_confirm("Reconfigure accordingly?", default=True):
                git.correct_module_configuration_issue(report.module, report.repo_status, issue=issue, dry_run=False)
                amount_fixed += 1

    # Print report.
    console.print()
    if amount_fixed == 0:
        console.print("No configuration corrections were applied.")
    else:
        console.print(f"Applied {amount_fixed} configuration correction{'s' if amount_fixed > 1 else ''}.")


def info(workspace_name: Optional[str] = None, **kwargs):
    ws = get_workspace_by_name_or_active(workspace_name)
    ws.info(**kwargs)


def purge(workspace_name: Optional[str] = None):
    ws = get_workspace_by_name_or_active(workspace_name)

    confirm = common.input_confirm(f"Are you sure you want to purge the workspace '{ws.path}'?", default=False)

    if confirm:
        # TODO: Also remove from known_workspaces.json.
        console.print(f"Purging workspace '{ws.path}' ...")
        for filename in os.listdir(ws.path):
            file_path = os.path.join(ws.path, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    import shutil
                    shutil.rmtree(file_path)
            except Exception as e:
                print(f"Failed to delete {filename}. Reason: {e}")


def exec_(**kwargs):
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()
    ws.exec(**kwargs)


def list_modules(deps: bool = False, tree: bool = True):
    from armarx_setup import console
    from armarx_setup.core.workspace import Workspace

    ws = Workspace.load_active_workspace()

    if deps:

        names = [m.name for m in ws.get_modules_in_alphabetical_order()]
        label = "Added modules and their dependencies:"

    else:
        names = list(ws.config.modules)
        label = "Added modules:"

    if len(names) > 0:
        if tree:
            from armarx_setup.core.module import introspection
            tree = introspection.get_module_tree_list(names, label=label)
            console.print(tree)
        else:
            console.print(label)
            common.print_list(names)


def no_workspace_active(e, action=None):
    reason = "No workspace is active. You can activate a workspace with:"
    if action is not None:
        reason = f"Cannot {action} because no workspace is active. You can activate a workspace with:"

    console.print(f"{reason}")
    common.print_commands([
        "axii workspace activate <workspace-name>"
    ])

    console.print(f"To see which workspaces are known to Axii, run:")
    common.print_commands([
        f"axii workspace list"
    ])

    console.print(f"To activate a workspace permanently, run:")
    common.print_commands([
        f"axii config --global default_workspace <workspace-name>"
    ])

    console.print(f"Alternatively, you can create a new workspace using:")
    common.print_commands([
        f"axii workspace create"
    ])

    sys.exit(1)


def no_such_workspace(e):
    from armarx_setup.core.workspace import Workspace

    known_workspaces = Workspace.read_known_workspaces()
    console.print(f"No such workspace '{e.workspace_name}'. Known workspaces are:")
    common.print_dictionary({name: info["path"] for name, info in known_workspaces.items()})

    sys.exit(1)


if __name__ == '__main__':
    # To suppress highlighting of "Workspace" type annotations not being found.
    from armarx_setup.core.workspace import Workspace

import rich_click as click

from armarx_setup.cli import common, complete
from armarx_setup.cli.workspace import integration
from armarx_setup.cli.module import integration as module_integration
from armarx_setup.core import error


COMMAND_GROUPS = [
    {
        "name": "Manage workspaces",
        "commands": ["activate", "deactivate", "list", "create", "purge"]
    },
    {
        "name": "Modify workspace",
        "commands": ["add", "remove", "add-features", "remove-features", "info", "auto-remove", "which"]
    },
    {
        "name": "Commands for setup",
        "commands": ["upgrade", "env", "system", "update", "prepare", "build", "install", "conclude"]
    },
    {
        "name": "Additional commands",
        "commands": ["clean", "exec", "git", "test", "list-modules"]
    }
]


@click.group("workspace", **common.group_kwargs)
def workspace(**kwargs):
    """Interact with a workspace known to Axii."""

    pass


@workspace.command("list", **common.command_kwargs)
def workspace_list(**kwargs):
    """List all known workspaces."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    integration.list_workspaces(**kwargs)


@workspace.command("add", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def workspace_add(**kwargs):
    """Add a module to the workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.add(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="add module")
    except error.ModuleInfoNotFound as e:
        module_integration.module_info_not_found(e)


@workspace.command("remove", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules_in_workspace)
def workspace_remove(**kwargs):
    """Remove a module from the workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.remove(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="remove module")


@workspace.command("activate", **common.command_kwargs)
@click.argument("workspace-name", shell_complete=complete.list_workspaces)
@click.option("--cd/--no-cd", default=True, help="Navigate into the workspace after activating.")
def workspace_activate(**kwargs):
    """Activate a workspace and navigate into it."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.activate_workspace(**kwargs)
    except error.NoSuchWorkspace as e:
        integration.no_such_workspace(e)


@workspace.command("deactivate", **common.command_kwargs)
def workspace_deactivate(**kwargs):
    """Deactivate the active workspace and reset the environment."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.deactivate_workspace(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="deactivate workspace")


@workspace.command("info", **common.command_kwargs)
@click.option("--graph/--no-graph", default=False, help="Show the module dependency graph (requires matplotlib).")
@click.option("--layout", help="Specify the graph layout algorithm.")
@click.option("--reduce/--no-reduce", default=False, help="Transitively reduce the module graph before showing it.")
@click.option("--all", "-a", default=False, is_flag=True, help="Show modules which are excluded by default.")
def workspace_info(**kwargs):
    """Give information about the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.info(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="query workspace info")


@workspace.command("env", **common.command_kwargs)
def workspace_env(**kwargs):
    """Refresh and source the environment in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.env(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform env step")



def module_specifying_options(verb: str):
    verb_cap = verb.capitalize()
    options = [
        click.option("--module", "-m", help=f"{verb_cap} the given module.",
                     shell_complete=complete.list_all_modules_in_workspace),
        click.option("--path", "-p", help=f"{verb_cap} the module this path belongs to."),
        click.option("--this", "-t", is_flag=True, help=f"{verb_cap} this module in which Axii is called."),
        click.option("--no-deps", "-n", is_flag=True, help=f"{verb_cap} the module without its dependencies."),

        click.option("--file", "-f", help="Deprecated. Use --path instead."),
    ]
    return common.make_decorator(options)


def ignore_errors_option(verb: str):
    return common.make_decorator([
        click.option("--ignore-errors", "-i", is_flag=True, help=f"Try to {verb} as much as possible, ignoring errors.")
    ])


@workspace.command("system", **common.command_kwargs)
@module_specifying_options(verb="setup the system for")
def workspace_system(**kwargs):
    """Install system requirements (e.g. apt packages) required by all modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.system(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform system step")
    except error.NoSuchModule as e:
        module_integration.no_such_module(e)


@workspace.command("update", **common.command_kwargs)
@click.option("--parallel/--no-parallel", default=True, help="Update multiple modules in parallel.")
@module_specifying_options(verb="update")
def workspace_update(**kwargs):
    """Update sources and artifacts of all modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.update(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform update step")
    except error.NoSuchModule as e:
        module_integration.no_such_module(e)


@workspace.command("prepare", **common.command_kwargs)
@ignore_errors_option(verb="prepare")
@module_specifying_options(verb="prepare")
def workspace_prepare(**kwargs):
    """Prepare all modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.prepare(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform prepare step")
    except error.NoSuchModule as e:
        module_integration.no_such_module(e)


@workspace.command("build", **common.command_kwargs)
@ignore_errors_option(verb="build")
@module_specifying_options(verb="build")
def workspace_build(**kwargs):
    """Build all modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.build(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform build step")
    except error.NoSuchModule as e:
        module_integration.no_such_module(e)


@workspace.command("install", **common.command_kwargs)
@ignore_errors_option(verb="install")
@module_specifying_options(verb="install")
def workspace_install(**kwargs):
    """Install all modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.install(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform install step")
    except error.NoSuchModule as e:
        module_integration.no_such_module(e)


@workspace.command("conclude", **common.command_kwargs)
def workspace_conclude(**kwargs):
    """Run concluding steps affecting multiple modules in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.conclude(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform conclude step")


@workspace.command("upgrade", **common.command_kwargs)
@click.option("--env/--no-env", default=True, help="Enable/disable the environment (.rc) generation.")
@click.option("--system/--no-system", default=True, help="Enable/disable the system setup step.")
@click.option("--update/--no-update", default=True, help="Enable/disable the update step.")
@click.option("--prepare/--no-prepare", default=True, help="Enable/disable the prepare step.")
@click.option("--build/--no-build", default=True, help="Enable/disable the build step.")
@click.option("--install/--no-install", default=True, help="Enable/disable the install step.")
@click.option("--conclude/--no-conclude", default=True, help="Enable/disable the conclude step.")
@click.option("--test/--no-test", default=False, help="Enable/disable running tests.")
@ignore_errors_option(verb="upgrade")
@module_specifying_options(verb="upgrade")
def workspace_upgrade(**kwargs):
    """Performs all of these steps in the active workspace: env, system, update, prepare, build, install, conclude."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.upgrade(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="perform upgrade")


@workspace.command("create", **common.command_kwargs)
@click.argument("workspace-path", default=None, required=False)
@click.argument("workspace-name", default=None, required=False)
def workspace_create(**kwargs):
    """Create a new workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    integration.create_workspace(**kwargs)


@workspace.command("purge", **common.command_kwargs)
def workspace_purge(**kwargs):
    """Purge a workspace from existence."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.purge(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="purge workspace")


@workspace.command("test", **common.command_kwargs)
@click.option("--build/--no-build", default=False,
              help="Build before running tests (default is --no-build).")
@click.option("--rerun-failed", "-r", is_flag=True,
              help="Run only the tests that failed previously (see 'ctest --help').")
@click.option("--output-on-failure", "-o", is_flag=True,
              help="Output anything outputted by the test program if the test should fail (see 'ctest --help').")
def workspace_test(**kwargs):
    """Run tests on all modules with a build step in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.test(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="execute tests")


@workspace.group("git", **common.group_kwargs)
def workspace_git():
    """Interact with Git repositories in the active workspace."""

    pass


@workspace_git.command("status", **common.command_kwargs)
@click.option("--fetch/--skip-fetch", default=False, help="En-/disable fetching.")
@click.option("--files/--no-files", default=False, help="En-/disable full listing of files.")
@click.option("--self/--no-self", default=False, help="Include Axii itself.")
def workspace_git_status(**kwargs):
    """Show the status of all Git repositories."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.git_status(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="query Git status")


@workspace_git.command("reconfigure", **common.command_kwargs)
@click.option("--default-checkout/--no-default-checkout", default=False,
              help="Also ensure repositories are on Axii-default checkouts.")
@click.option("--yes", "-y", default=False, is_flag=True, help="Apply all suggested fixes automatically.")
@click.option("--self/--no-self", default=False, help="Include Axii itself.")
def workspace_git_reconfigure(**kwargs):
    """Reconfigure workspace to ensure up-to-date Git repository settings."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.git_reconfigure(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="reconfigure Git repositories")


@workspace.command("exec", **common.command_kwargs)
@click.argument("command")
@click.option("--in", "-i", help="Only execute in modules under the given prefix.")
@click.option("--exclude", "-e", help="Exclude modules under the given prefix.")
@click.option("--git", "-g", default=False, is_flag=True, help="Only run in modules with a Git update step.")
def workspace_exec(**kwargs):
    """Execute a command in the root directory of each module in the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        kwargs["in_"] = kwargs.pop("in")
        integration.exec_(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="execute command")


@workspace.command("list-modules", **common.command_kwargs)
@click.option("--deps", "-d", default=False, is_flag=True, help="Include dependencies of added modules.")
@click.option("--tree/--flat", default=True, help="Present output in a tree or list.")
def workspace_list_modules(**kwargs):
    """List the modules added to the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.list_modules(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="query Git status")


@workspace.command("which", **common.command_kwargs)
def workspace_which(**kwargs):
    """Print the directory of the active workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.which(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="print workspace path")


@workspace.command("clean", **common.command_kwargs)
def workspace_clean(**kwargs):
    """Remove all build artifacts."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.clean(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="clean modules")


@workspace.command("auto-remove", **common.command_kwargs)
@click.option("--yes", default=False, is_flag=True, help="Remove unneeded modules without asking.")
def workspace_auto_remove(**kwargs):
    """Automatically remove stray or unneeded modules to free space."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.auto_remove(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="autoremove modules")


@workspace.command("add-features", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
@click.argument("feature-names", nargs=-1, shell_complete=complete.list_features)
def workspace_add_features(**kwargs):
    """Add optional features provided by a module to the workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.add_features(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="add feature")


@workspace.command("remove-features", **common.command_kwargs)
def workspace_remove_features(**kwargs):
    """Remove optional features provided by a module from the workspace."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.remove_features(**kwargs)
    except error.NoWorkspaceActive as e:
        integration.no_workspace_active(e, action="remove feature")

import os

import rich_click as click

# Try to only import lightweight imports here.
from armarx_setup import __version__
from armarx_setup.cli import common, workspace, module, database, self, config, edit


click.rich_click.COMMAND_GROUPS = {
    "axii": [
        {
            "name": "Main commands",
            "commands": ["workspace", "module", "database"]
        },
        {
            "name": "Axii",
            "commands": ["self", "config", "edit"]
        },
        {
            "name": "Deprecated commands",
            "commands": ["activate", "add", "remove", "add-features", "remove-features", "init", "purge", "info",
                         "list", "show", "env", "system", "update", "prepare", "build", "install", "conclude",
                         "upgrade",
                         "clean", "exec", "test", "git", "which", "list-workspaces"]
        }
    ],

    "axii workspace": workspace.COMMAND_GROUPS,
    "axii module": module.COMMAND_GROUPS,
    "axii database": database.COMMAND_GROUPS,

    "axii self": self.COMMAND_GROUPS,
    "axii config": config.COMMAND_GROUPS,
    "axii edit": edit.COMMAND_GROUPS,
}


@click.group(**common.group_kwargs)
@click.version_option(__version__)
@click.option("--disable-prompts", default=False, is_flag=True, help="Disables all prompts.")
def entry_point(disable_prompts):
    common.disable_prompts = os.environ.get("_AXII_DISABLE_PROMPTS", disable_prompts)


entry_point.add_command(workspace.group)
entry_point.add_command(module.group)
entry_point.add_command(database.group)
entry_point.add_command(self.group)
entry_point.add_command(config.group)
entry_point.add_command(edit.group)


if __name__ == '__main__':
    entry_point()

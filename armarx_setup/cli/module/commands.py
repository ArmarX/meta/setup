import rich_click as click

from armarx_setup.cli import common, complete
from armarx_setup.cli.module import integration
from armarx_setup.core import error


COMMAND_GROUPS = []  # Default.


@click.group("module", **common.group_kwargs)
def module(**kwargs):
    """Interact with a module known to Axii."""

    pass


@module.command("list", **common.command_kwargs)
@click.option("--tree/--flat", default=True, help="Present output in a tree or list.")
def module_list(**kwargs):
    """List all modules known to Axii."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    integration.list_modules(**kwargs)


@module.command("show", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def module_show(**kwargs):
    """Show a module definition."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.show_module(**kwargs)
    except error.ModuleInfoNotFound as e:
        integration.module_info_not_found(e)


@module.command("create", **common.command_kwargs)
@click.argument("module_name", shell_complete=complete.list_modules)
def module_create(**kwargs):
    """Create a new module."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.create_module(**kwargs)
    except error.ModuleAlreadyExists as e:
        integration.module_already_exists(e)


@module.command("readme", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def module_readme(**kwargs):
    """Print the README of a module."""

    common.bootstrap(ensure_installation=True, print_logo=True)

    try:
        integration.print_module_readme(**kwargs)
    except error.ModuleInfoNotFound as e:
        integration.module_info_not_found(e)


@module.group("open", **common.group_kwargs)
def module_open(**kwargs):
    """Open information regarding a module."""

    pass


@module_open.command("homepage", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def module_open_homepage(**kwargs):
    """Open the homepage of the module in the web browser (if URL available)."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.open_homepage(**kwargs)
    except error.ModuleInfoNotFound as e:
        integration.module_info_not_found(e)


@module_open.command("issues", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def module_open_homepage(**kwargs):
    """Open the issues page of the module in the web browser (if URL available)."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.open_issues(**kwargs)
    except error.ModuleInfoNotFound as e:
        integration.module_info_not_found(e)


@module_open.command("merge-requests", **common.command_kwargs)
@click.argument("module-name", shell_complete=complete.list_modules)
def module_open_homepage(**kwargs):
    """Open the merge request page of the module in the web browser (if URL available)."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.open_merge_requests(**kwargs)
    except error.ModuleInfoNotFound as e:
        integration.module_info_not_found(e)

import os
import sys

import typing as ty

from armarx_setup import console
from armarx_setup.cli import common
from armarx_setup.core import error
from armarx_setup.core.module import module


def default_module_config(module_name):
    return f"""{{
  "general": {{
    "url": "https://gitlab.com/{module_name}"
  }},

  "update": {{
    "git": {{
      "ssh_url": "git@gitlab.com:{module_name}.git",
      "https_url": "https://gitlab.com/{module_name}.git"
    }}
  }},
  "prepare": {{
    "cmake": {{
      "project_name": "<Enter CMake Project Name>",
      "definitions": {{
        "CMAKE_C_COMPILER": "$AXII_DEFAULT_C_COMPILER",
        "CMAKE_CXX_COMPILER": "$AXII_DEFAULT_CXX_COMPILER"
      }}
    }}
  }},
  "build": "cmake",

  "required_modules": {{
    "meta/default_compiler": {{}},

    "armarx-minimal": {{}}
  }}
}}
"""


def list_modules(tree: bool = True):
    from armarx_setup.core.module import Module, introspection

    names = Module.get_all_module_names()
    names.sort()

    if len(names) > 0:
        if tree:
            tree = introspection.get_module_tree_list(names, label="Known modules:")
            console.print(tree)
        else:
            console.print("Known modules:")
            common.print_list(names)


def create_module(module_name):
    from armarx_setup.core.module.module import Module

    if module_name in Module.get_all_module_names():
        raise error.ModuleAlreadyExists(name=module_name)

    module_config_res = f"{module_name}"
    module_config = f"{module_name}.json"

    cwd = os.getcwd()

    if cwd not in module.MODULE_DATA_DIRS:
        console.print(f"Current working directory is not any module database known to Axii:")
        common.print_list(module.MODULE_DATA_DIRS)
        console.print(f"Creating the module definition in any other directory than those will prevent it from being "
                      f"found.")
        console.print()
        console.print("To fix this problem you can:")
        common.print_list([
            "Navigate in one of the paths listed above, depending on which database you want to add your module, "
            "and re-issue this command.",
            "Create a new Axii module database and add its path to '$ARMARX_MODULES'."
        ], numbered=True)

        exit(1)

    if os.path.isfile(os.path.join(cwd, module_config)):
        console.print(f"Module '{module_config}' does already exist.")
        exit(1)

    console.print(f"Create module directory '{module_config_res}' in '{cwd}'.")
    os.makedirs(module_name, exist_ok=True)
    console.print(f"Create module definition '{module_config}' in '{cwd}'.")
    with open(os.path.join(cwd, module_config), 'w') as f:
        f.write(default_module_config(module_name=module_name))


def show_module(module_name: str):
    from rich.rule import Rule
    from armarx_setup.core.module import Module

    path = Module.get_info_path(module_name)
    mod = Module.load_info(module_name, ws=None)
    console.print(Rule(title=f"Definition of Module '{module_name}'"))
    console.print(f"Module defined at: '{path}'")
    console.print("")
    console.print(mod)
    console.print(Rule())


def print_module_readme(module_name: str):
    from rich.markdown import Markdown, Rule
    from armarx_setup.core.module import Module
    from armarx_setup.core.workspace import Workspace
    from armarx_setup.core.util.commands import run

    def look_up_readme_online(mod: Module, readme_file: str) -> str:
        output = run(f"git archive --remote={mod.update.git.ssh_url} HEAD {readme_file} | tar xO",
                     quiet=True, capture_output=True)
        if readme_file.endswith(".md"):
            content = Markdown(output)
        else:
            content = output
        return content

    def look_up_readme_in_workspace(mod: Module, readme_file: str) -> str:
        readme_file_path = os.path.join(mod.path, readme_file)
        with open(readme_file_path) as f:
            if readme_file.endswith(".md"):
                content = Markdown(f.read())
            else:
                content = f.read()
        return content

    try:
        ws = Workspace.load_active_workspace()
        mod = ws.module_graph.get_module_with_name(module_name)
    except (error.NoWorkspaceActive, error.NoSuchModule):
        ws = None
        mod = Module.load_info(module_name, ws=None)
        console.print("Look up README file...")

    readme_files = ["README.md", "README.txt", "readme.md", "readme.txt", "Readme.md", "Readme.txt"]

    for readme_file in readme_files:
        try:
            if ws is not None:
                content = look_up_readme_in_workspace(mod, readme_file)
                source = "current workspace"
            else:
                content = look_up_readme_online(mod, readme_file)
                source = "online repository"
        except:
            pass
        else:
            console.print()
            console.print(Rule(title=f"{module_name} {readme_file} (from {source})"))
            console.print()
            console.print(content)
            exit(0)
    else:
        console.print(f"Could not find a README file for '{module_name}' online or in the current workspace.")


def get_url(module_name: str) -> ty.Optional[str]:
    from armarx_setup.core.module import Module
    mod = Module.load_info(module_name, ws=None)
    if mod.general is not None:
        url = mod.general.url
        if mod.general.url is not None:
            if url.endswith(".git"):
                from pathlib import Path
                url = Path(url).stem
            return url

    console.print("No general URL specified in the module description.")
    return None


def open_homepage(module_name: str):
    url = get_url(module_name)
    if url is not None:
        import webbrowser
        console.print(f"Opening {url} in web browser")
        webbrowser.open(url, new=0, autoraise=True)
    

def open_issues(module_name: str):
    url = get_url(module_name)
    if url is not None:
        if "gitlab" in url:
            import webbrowser
            console.print(f"Opening Issues in web browser")
            webbrowser.open(os.path.join(url, "-/issues"), new=0, autoraise=True)
        else:
            console.print(f"Unknown repository type at {url}")


def open_merge_requests(module_name: str):
    url = get_url(module_name)
    if url is not None:
        if "gitlab" in url:
            import webbrowser
            console.print(f"Opening Merge Requests in web browser")
            webbrowser.open(os.path.join(url, "-/merge_requests"), new=0, autoraise=True)
        else:
            console.print(f"Unknown repository type at {url}")


def module_already_exists(e):
    console.print(e)
    exit(1)


def module_info_not_found(e, suggest_create_new=False):
    console.print(e)
    console.print()

    if e.database_module is not None:
        console.print(f"The module '{e.database_module.database_module_name}' claims the authority for the module "
                      f"namespace '{e.database_module.namespace}*', but is not part of the workspace. You can try "
                      f"adding this database to Axii as follows: ")
        common.print_commands([
            f"axii workspace add {e.database_module.database_module_name}",
            f"axii workspace update",
            f"axii workspace env",
            f"axii workspace add {e.name}"
        ])

    elif e.similar_modules:
        console.print("Similar module names: ")
        common.print_list(e.similar_modules)

        if suggest_create_new:
            console.print("You can also create a new module as follows: ")
            common.print_commands([
                f"axii module create {e.name}"
            ])

    sys.exit(1)


def no_such_module(e):
    console.print(e)
    console.print()

    if e.similar_modules:
        console.print("Similar module names: ")
        common.print_list(e.similar_modules)

    sys.exit(1)

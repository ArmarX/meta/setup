import sys

import rich_click as click

from armarx_setup import console
from armarx_setup.core import error
from armarx_setup.core.config import config as axii_config
from armarx_setup.cli import common


def config(**kwargs):
    if kwargs["global"]:
        del kwargs["global"]
        return config_global(**kwargs)

    raise error.NotYetImplemented(feature="configure non-global variables")


def config_global(variable_name, variable_value=None, unset=False):
    if variable_value is None:
        # Unset variable if --unset was set.
        if unset:
            axii_config.unset_global(variable_name)
            console.print(f"Unset '{variable_name}'.")
            axii_config.store_global_config()
        # Otherwise print it if found.
        else:
            value = axii_config.get_global(variable_name)
            console.print(f"{value}")
    else:
        if unset:
            raise click.UsageError("Only supply one variable name with --unset")

        axii_config.set_global(variable_name, variable_value)
        variable_value = axii_config.get_global(variable_name)
        console.print(f"Set '{variable_name}' to {variable_value}.")
        axii_config.store_global_config()


def no_such_config_variable(e, is_global=False):
    console.print(e)
    console.print()
    console.print("You can set it as follows:")
    global_flag = "--global " if is_global else ""
    common.print_commands([
        f"axii config {global_flag}{e.variable_name} \"<new_value>\""
    ])
    sys.exit(1)

import rich_click as click

from armarx_setup.cli import common, complete
from armarx_setup.cli.config import integration
from armarx_setup.core import error


COMMAND_GROUPS = []  # Default.


@click.group("config", **common.command_kwargs)
@click.argument("variable_name")
@click.argument("variable_value", default=None, required=False)
@click.option("--global", default=False, is_flag=True, help="Set/get/unset the global variables.")
@click.option("--unset", default=False, is_flag=True, help="Unsets the variable.")
def config(**kwargs):
    """Get or set global configurations."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    try:
        integration.config(**kwargs)
    except error.NoSuchConfigVariable as e:
        integration.no_such_config_variable(e, is_global=kwargs["global"])

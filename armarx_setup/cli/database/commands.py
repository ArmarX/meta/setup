import rich_click as click

from armarx_setup.cli import common
from armarx_setup.cli.database import integration


COMMAND_GROUPS = []  # Default.


@click.group("database", **common.group_kwargs)
def database(**kwargs):
    """Interact with an Axii module database."""

    pass


@database.command("list", **common.command_kwargs)
def database_list(**kwargs):
    """List all databases known to Axii."""

    common.bootstrap(ensure_installation=True, print_logo=False)

    integration.list_databases(**kwargs)

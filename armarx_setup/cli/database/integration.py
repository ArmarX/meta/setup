from armarx_setup import console
from armarx_setup.cli import common


def list_databases(**kwargs):
    from armarx_setup.core.module import module

    databases_list = module.MODULE_DATA_DIRS
    console.print("Known module databases:")
    common.print_list(databases_list)

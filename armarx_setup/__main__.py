#!/usr/bin/env python3

from armarx_setup.cli import entry_point


if __name__ == '__main__':
    entry_point()

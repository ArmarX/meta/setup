import os
import git

from dataclasses import dataclass
from typing import List, Optional, Union

from armarx_setup import console, PACKAGE_ROOT


class Symbols:
    clean = "[green]:heavy_check_mark:[/green]"
    dirty = ":pencil:"
    untracked = ":question:"
    out_of_sync = ":arrows_clockwise:"

    @classmethod
    def get_legend_items(cls):
        return [
            f"{cls.clean} Clean",
            f"{cls.dirty} Modified files",
            f"{cls.untracked} Untracked files",
            f"{cls.out_of_sync} Out of sync with origin",
        ]


def status(
        ws: "Workspace",
        fetch=True,
        files=False,
        self=False,
):
    from rich.console import Group
    from rich.progress import Progress
    from rich.table import Table
    from rich.tree import Tree
    from armarx_setup.core.util import prefix_color_map

    def func(
            module: Union["Module", str],
            progress: Progress,
            progress_task_id,
            name=None,
    ):
        def advance_progress():
            progress.update(progress_task_id, advance=1)

        rst = repo_status_of_module_or_directory(module, fetch)
        if rst is None:
            advance_progress()
            return

        name = prefix_color_map.module_to_colored_text(name or module.name)
        space = "   "

        status_symbols = []
        status_symbols.append(Symbols.clean if rst.clean else "")
        if not rst.clean:
            status_symbols.append(Symbols.dirty if rst.dirty else "")
            status_symbols.append(Symbols.untracked if rst.untracked else "")
            status_symbols.append(Symbols.out_of_sync if rst.out_of_sync else "")
        status = space.join(status_symbols)

        # rst.commit.author_tz_offset
        active_branch = rst.active_branch

        if not rst.on_default:
            active_branch = f"[yellow]{active_branch}[/]"
        elif rst.out_of_sync:
            active_branch = f"[cyan]{active_branch}[/]"

        if rst.is_accessible:
            name = f"[green]{name}[/]"

        columns = [name, active_branch, status]
        if files:
            trees = []

            def add_tree(label: str, items: List[str]):
                tree = Tree(f"[b]{label}[/]")
                for f in items:
                    tree.add(f)
                trees.append(tree)

            if rst.staged:
                add_tree("[green]Staged:[/]", rst.staged)
            if rst.modified:
                add_tree("[red]Modified:[/]", rst.modified)
            if rst.untracked:
                add_tree("[red]Untracked:[/]", rst.untracked)
            columns.append(Group(*trees))

        advance_progress()
        return columns # table.add_row(*columns)

    headers = ["Module", "Active Branch/Tag", "Status"]
    if files:
        headers.append("Files")
    table = Table(*headers)

    total = ws.module_graph.number_of_nodes() + (1 if self else 0)
    max_workers = 32
    with Progress() as progress:
        task_id = progress.add_task("Gather status information of modules", total=total)
        kwargs = dict(progress=progress, progress_task_id=task_id)

        from concurrent import futures
        with futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
            def submit_job(*args, **kwargs):
                return executor.submit(func, *args, **kwargs)
            
            result_futures = ws.for_each_module_in_alphabetical_order(submit_job, **kwargs)

            if self:
                f = submit_job(PACKAGE_ROOT, name="Axii", **kwargs)
                result_futures['Axii'] = f 
                
            for future in result_futures.values():
                try:
                    result = future.result()
                
                    if result is None:
                        continue
                    
                    table.add_row(*result)
                except:
                    pass
                
    console.print(f"[i]Legend:[/] " + " | ".join(
        ["[yellow]Branch/tag differing from default[/]"] + Symbols.get_legend_items()
    ))
    console.print(table)


@dataclass
class RepoStatus:
    active_branch: str

    commit: git.Commit

    # Changes to be committed.
    staged: List[str]
    # Changes not staged for commit.
    modified: List[str]
    # Untracked Files.
    untracked: List[str]
    dirty: bool

    # The branch is not in sync with origin.
    out_of_sync: bool

    has_upstream: bool

    is_accessible: bool

    # Whether the repo is on the default branch, tag or commit.
    on_default: bool

    # Default branch of this repo.
    default_branch: str

    # URL of origin.
    origin_url: str

    # Amount of commits ahead/behind of origin. Applies only if on branch, else both numbers are -1.
    commits_ahead: int
    commits_behind: int

    @property
    def clean(self):
        return not (bool(self.untracked) or self.dirty)


def correct_workspace_configuration_issues(ws: "Workspace", default_checkout: bool = False, self: bool = False,
                                           dry_run: bool = False):
    class ModuleConfigurationReport:
        def __init__(self, module, repo_status, issues):
            self.module = module
            self.repo_status = repo_status
            self.issues = issues

    fetch = True

    def func(module: "Module"):
        try:
            repo_status = repo_status_of_module_or_directory(module, fetch)
            if repo_status is None:
                return None
            issues = correct_module_configuration_issues(module, repo_status, default_checkout=default_checkout,
                                                         dry_run=dry_run)
            return ModuleConfigurationReport(module=module, repo_status=repo_status, issues=issues)
        except Exception as e:
            console.print(e)

    max_workers = 32
    from concurrent import futures
    with futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        def submit_job(*args, **kwargs):
            return executor.submit(func, *args, **kwargs)

        result_futures = ws.for_each_module_in_alphabetical_order(submit_job)

        if self:
            from armarx_setup.core.module import axii_module
            f = submit_job(module=axii_module)
            result_futures[axii_module.name] = f

        for future in result_futures.values():
            try:
                yield future.result()
            except Exception as e:
                print(e)


class ConfigurationIssue:

    remote_origin_url_mismatch = "Remote origin URL mismatch"
    default_checkout_mismatch = "Default checkout mismatch"

    def __init__(self, name="", actual="", expected="", problem_template=None, correct_template=None):
        self.name = name
        self.actual = actual
        self.expected = expected

        if problem_template is None:
            problem_template = "{n}: Expected '{e}', got '{a}'."
        self.problem_template = problem_template

        if correct_template is None:
            correct_template = "{n}: Set from '{a}' to '{e}'."
        self.correct_template = correct_template

    def to_string_problem(self):
        kwargs = {
            "n": self.name,
            "e": self.expected,
            "a": self.actual
        }
        return self.problem_template.format(**kwargs)

    def to_string_correct(self):
        kwargs = {
            "n": self.name,
            "e": self.expected,
            "a": self.actual
        }
        return self.correct_template.format(**kwargs)


def correct_origin_url_configuration_issue(module: "Module", repo_status: RepoStatus, dry_run: bool = False):
    # Detect issue.
    actual_url = repo_status.origin_url
    is_https = actual_url.startswith("https://")  # HTTPS is more reliable to detect, SSH username can be anything.
    if is_https:
        expected_url = module.update.git.https_url
    else:
        expected_url = module.update.git.ssh_url
    if actual_url == expected_url:
        return

    # Fix issue.
    if not dry_run:
        # We could use the Git Python bindings, but this ensures that the exact command is printed on the terminal
        # so users have a better feeling of what is going on and so they can revert it if something goes wrong.
        from armarx_setup.core.util.commands import run
        run(f"git remote set-url origin \"{expected_url}\" \"{actual_url}\"", cwd=module.update.ctx.path)

    return ConfigurationIssue(name=ConfigurationIssue.remote_origin_url_mismatch,
                              actual=actual_url, expected=expected_url)


def correct_default_checkout_configuration_issue(module: "Module", repo_status: RepoStatus, dry_run: bool = False):
    # Detect issue.
    if repo_status.on_default:
        return

    expected = module.update.git.default_checkout_ref or module.update.git.default_checkout_commit or \
               repo_status.default_branch

    # Fix issue.
    if not dry_run:
        console.print(f"This is not implemented yet! Please, manually switch to '{expected}' in "
                      f"{module.update.ctx.path}.")

    return ConfigurationIssue(name=ConfigurationIssue.default_checkout_mismatch,
                              actual=repo_status.active_branch, expected=expected)


def correct_module_configuration_issue(module: "Module", repo_status: RepoStatus, issue, dry_run: bool = False):
    issue_map = {
        ConfigurationIssue.remote_origin_url_mismatch: correct_origin_url_configuration_issue,
        ConfigurationIssue.default_checkout_mismatch: correct_default_checkout_configuration_issue
    }

    correct_issue_fn = None
    if isinstance(issue, ConfigurationIssue):
        correct_issue_fn = issue_map[issue.name]
    elif isinstance(issue, str):
        correct_issue_fn = issue_map[issue]

    if correct_issue_fn is not None:
        return correct_issue_fn(module, repo_status, dry_run=dry_run)


def correct_module_configuration_issues(module: "Module", repo_status: RepoStatus, default_checkout: bool = False,
                                        dry_run: bool = False):
    kwargs = dict(module=module, repo_status=repo_status, dry_run=dry_run)

    issues: List[ConfigurationIssue] = [
        correct_origin_url_configuration_issue(**kwargs),
        correct_default_checkout_configuration_issue(**kwargs) if default_checkout else None
    ]

    return [i for i in issues if i is not None]


def repo_statuses_of_workspace(
        ws: "Workspace",
        fetch: bool
):
    modules = list(ws.module_graph.nodes(data="module"))
    modules.sort()

    for _, module in modules:
        yield module, repo_status_of_module_or_directory(module, fetch=fetch)


def repo_status_of_module_or_directory(
        module_or_directory: Union["Module", str],
        fetch: bool,
) -> Optional[RepoStatus]:
    from armarx_setup.core.module import Module

    directory: str
    if isinstance(module_or_directory, Module):
        module = module_or_directory
        if module.update is None or module.update.git is None:
            return None
        else:
            directory = module.update.git.ctx.path
    else:
        module = None
        directory = module_or_directory

    return repo_status(directory=directory, fetch=fetch, module=module)


def repo_status(
        directory: str,
        fetch: bool,
        module: Optional["Module"] = None,
) -> Optional[RepoStatus]:
    # This function does not require importing Module.
    # It is used when printing the logo, so it should be lightweight.

    git_ssh_identity_file = os.path.expanduser("~/.ssh/id_rsa")  # or id_rsa
    git_ssh_cmd = f"ssh -i {git_ssh_identity_file}"

    with git.Git().custom_environment(GIT_SSH_COMMAND=git_ssh_cmd):

        out_of_sync = False
        has_upstream = False
        is_accessible = False
        active_branch_name = ""
        is_branch = False
        commits_ahead = -1
        commits_behind = -1
        origin_url = ""
        on_default = False
        default_branch_name = ""

        try:
            repo = git.Repo(directory)

            try:
                origin_url = repo.remotes.origin.url
            except Exception as e:
                print(e)

            if repo.head.is_detached:
                for tag in repo.tags:
                    if tag.commit == repo.head.commit:
                        active_branch_name = f"[#808080]tag:[/] {tag.name}"
                        break
                if not active_branch_name:  # else
                    active_branch_name = f"[#808080]commit:[/] {repo.head.commit.hexsha}"
            else:
                is_branch = True
                active_branch_name = repo.active_branch.name

            if is_branch:
                try:
                    commits_ahead = len(list(repo.iter_commits(f"origin/{active_branch_name}..{active_branch_name}")))
                except:
                    pass
                try:
                    commits_behind = len(list(repo.iter_commits(f"{active_branch_name}..origin/{active_branch_name}")))
                except:
                    pass

            current_hash = repo.head.commit.hexsha
            origin: git.Remote = repo.remotes.origin

            if fetch:
                # Repository might be unavailable, set all non-interaction flags.
                from armarx_setup.core.util import environ_context
                from armarx_setup.core.module.update import git_non_interaction_env_vars

                with environ_context.EnvironContext(git_non_interaction_env_vars, verbose=-1):
                    try:
                        origin.fetch()
                    except:
                        pass
                    else:
                        is_accessible = True

            try:
                branch_remote = origin.refs[active_branch_name]
            except IndexError:  # Does not exist on remote.
                pass
            else:
                out_of_sync = branch_remote.commit.hexsha != current_hash
                has_upstream = repo.active_branch.is_remote()

            origin_head: git.RemoteReference = origin.refs["HEAD"]
            default_branch: git.RemoteReference = origin_head.reference
            default_branch_name = default_branch.name

            def is_on_default():
                # Is the currently checked out branch the default?
                if module is not None:
                    git_step = module.update.git
                    if git_step.default_checkout_ref is not None:
                        # Is the current checkout == the default ref?
                        default_ref = repo.references[git_step.default_checkout_ref]
                        return repo.head.commit == default_ref.commit

                    elif git_step.default_checkout_commit is not None:
                        default_commit = repo.commit(git_step.default_checkout_commit)
                        return repo.head.commit == default_commit

                # If we are on a tag (and no tag is specified by the module (case above)),
                # we cannot be on the default branch.
                if repo.head.is_detached:
                    return False

                # We are on branch. Is it the default branch?
                active_branch = repo.active_branch
                if isinstance(active_branch, git.Head):
                    tracking_branch: git.RemoteReference = active_branch.tracking_branch()
                    return tracking_branch == default_branch
                else:
                    # What is the active branch if not a git.Head? Anyway, ...
                    return False

            on_default = is_on_default()

        except git.GitCommandError:
            pass
        except git.exc.InvalidGitRepositoryError:
            return None
        except git.NoSuchPathError:
            return None

        staged = [item.a_path for item in repo.index.diff("HEAD")]
        modified = [item.a_path for item in repo.index.diff(None)]
        untracked = repo.untracked_files

        return RepoStatus(
            active_branch=active_branch_name,
            commit=repo.head.commit,
            staged=staged,
            modified=modified,
            untracked=untracked,
            dirty=repo.is_dirty(),
            out_of_sync=out_of_sync,
            has_upstream=has_upstream,
            is_accessible=is_accessible,
            origin_url=origin_url,
            on_default=on_default,
            default_branch=default_branch_name,
            commits_ahead=commits_ahead,
            commits_behind=commits_behind
        )


if __name__ == '__main__':
    # Suppress warnings from type annotations.
    from armarx_setup.core.workspace import Workspace
    from armarx_setup.core.module import Module

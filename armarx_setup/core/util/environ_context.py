import os

from typing import Dict

from armarx_setup import console


class EnvironContext:

    def __init__(self, env: Dict[str, str], verbose=-1):
        self.env = env
        self.old_env = None
        self.verbose = verbose

    def __enter__(self):
        self.old_env = dict()
        if len(self.env) == 0:
            return
        if self.verbose >= 0:
            console.print(f"Set env vars: {self._print_env_dict(self.env)} ...")
        for key, val in self.env.items():
            if key in os.environ:
                self.old_env[key] = os.environ[key]
            os.environ[key] = val
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Restore environment
        if len(self.env) == 0:
            return
        if self.verbose >= 0:
            deleted = sorted(set(self.env).difference(set(self.old_env)))
            console.print(f"Restore env vars: {self._print_env_dict(self.old_env)} (delete {', '.join(deleted)}) ...")
        for key, val in self.env.items():
            if key in self.old_env:
                os.environ[key] = self.old_env[key]
            else:
                del os.environ[key]

    def print_env_dict(self):
        return self._print_env_dict(self.env)

    @staticmethod
    def _print_env_dict(env: Dict[str, str]) -> str:
        return " ".join([f"{k}='{v}'" for k, v in env.items()])

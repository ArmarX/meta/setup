import os
import subprocess

from typing import Dict, List


apt_package_cache: Dict[str, bool] = {}


def is_apt_package_installed(package_name: str) -> bool:
    if not apt_package_cache:
        apply_dpkg_list()

    if package_name in apt_package_cache:
        return apt_package_cache[package_name]
    else:
        is_installed = os.system(f'dpkg -V "{package_name}" > /dev/null 2>&1') == 0
        apt_package_cache[package_name] = is_installed
        return is_installed


def dpkg_list():
    process = subprocess.run("dpkg --list", shell=True, stdout=subprocess.PIPE)
    out = process.stdout.decode()
    lines = out.split("\n")
    lines = [line for line in lines if line.startswith("ii")]
    table = [list(filter(bool, line.split(" "))) for line in lines]
    names = [row[1] for row in table]
    return names


def apply_dpkg_list():
    global apt_package_cache
    for name in dpkg_list():
        apt_package_cache[name] = True


def flush_cache():
    global apt_package_cache
    apt_package_cache = {}


def install_apt_packages(packages: List[str]):
    from armarx_setup.core.util import cli, commands

    packages = sorted(set(packages))
    s_packages = "package" + ('s' if len(packages) > 1 else '')

    wss_list = ' '.join(packages)
    command = f"sudo apt-get install --no-install-recommends -y {wss_list}"

    from armarx_setup.cli import common
    if common.input_confirm(f"Run '{command}' to install {len(packages)} {s_packages}?", default=False):
        commands.run(command)
        flush_cache()

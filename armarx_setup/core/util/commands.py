import subprocess

from rich.rule import Rule

from armarx_setup.core import error
from armarx_setup import console
from armarx_setup.core.util.environ_context import EnvironContext


def run(cmd, cwd=None, capture_output=False, quiet=False, env_ctx: EnvironContext = None):
    in_cwd = "" if cwd is None else f" in '[link=file://{cwd}]{cwd}[/link]'"
    if not quiet:
        in_env_ctx = " "
        if env_ctx is not None:
            in_env_ctx = f" with environment variables {env_ctx.print_env_dict()} "
        console.print(f"Run '{cmd}'{in_cwd}{in_env_ctx}...")
        console.print(Rule(title="Command output"))

    kwargs = dict(cwd=cwd, shell=True)
    if cwd is not None:
        kwargs["cwd"] = cwd
    if capture_output:
        kwargs["stdout"] = subprocess.PIPE
        kwargs["stderr"] = subprocess.PIPE

    r = subprocess.run(cmd, **kwargs)
    if not quiet:
        color = 'green' if r.returncode == 0 else 'red'
        console.print(Rule(title=f"Command exited - Exit code [{color}]{r.returncode}[/{color}]"))

    if r.returncode != 0:
        raise error.CommandFailed(cmd=cmd, msg=f"with return code {r.returncode}{in_cwd}",
                                  stderr=r.stderr.decode().strip() if capture_output else None)

    if capture_output:
        stdout = r.stdout.decode().strip()
        stderr = r.stderr.decode().strip()
        return f"{stdout}\n{stderr}" if stderr else stdout

import shutil

from rich.text import Text

from armarx_setup import console


def print_full_width_heading(text: str, sep_color="white", indentation=3):
    text_width = len(Text.from_markup(text))
    # click.get_terminal_size() was removed in 8.1.0.
    # see https://click.palletsprojects.com/en/8.1.x/changes/#version-8-1-0
    term_width = shutil.get_terminal_size()[0]

    console.print()
    console.print(f"[{sep_color}]┏{'━' * (term_width - 2)}┓")
    console.print(f"[{sep_color}]┠{'─' * indentation}[/] {text} [{sep_color}]{'─' * (term_width - (indentation + 2) - text_width - 2)}┨[/]")
    console.print(f"[{sep_color}]┗{'━' * (term_width - 2)}┛")

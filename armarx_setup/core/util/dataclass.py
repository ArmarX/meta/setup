from typing import Any, Dict, List, Union


def sanitize_type(val, cls, **kwargs):
    if isinstance(val, dict):
        return cls(**val, **kwargs)
    elif isinstance(val, str):
        return cls(**{val: dict()}, **kwargs)


def sanitize_steps_type(val, cls, **kwargs):
    if val is None:
        return None
    else:
        assert isinstance(val, list), f"{cls}\n{val}"
        try:
            return cls(steps=val, **kwargs)
        except TypeError as e:
            from armarx_setup import console
            console.print(cls)
            raise


def sanitize_steps(
        data: List[Union[Dict, Any]],
        classes: List[Any],
        factory=None,
        **kwargs,
        ):
    if data is None:
        return None

    if factory is None:
        factory = {cls.type: cls for cls in classes}

    steps = []
    for step in data:
        if isinstance(step, dict):
            cls = factory[step["type"]]
            del step["type"]
            steps.append(cls(**step, **kwargs))
        else:
            steps.append(step)

    return steps



def profile_start():
    import cProfile
    pr = cProfile.Profile()
    pr.enable()
    return pr


def profile_end(pr, log_filepath: str):
    import pstats
    import io
    pr.disable()

    s = io.StringIO()
    pstats.Stats(pr, stream=s).sort_stats("cumulative").print_stats()
    with open(log_filepath, "w") as file:
        file.write(s.getvalue())


class ProfileContext:
    """
    To be used in a with statement like this:

    ```
    with ProfileContext("profile.txt"):
        my_operation()
    ```
    """

    def __init__(self, log_filepath: str):
        self.log_filepath = log_filepath
        self.pr = None

    def __enter__(self):
        self.pr = profile_start()

    def __exit__(self, exc_type, exc_val, exc_tb):
        profile_end(self.pr, self.log_filepath)

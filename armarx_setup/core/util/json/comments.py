from .core import JsonData

COMMENTS_MARKER = "#"


def is_comment(s: str, marker=COMMENTS_MARKER):
    return isinstance(s, str) and s.strip().startswith(marker)


def is_not_comment(s: str, marker=COMMENTS_MARKER):
    return not is_comment(s, marker=marker)


def remove_comments(data: JsonData) -> JsonData:
    """
    Remove any dict keys or list elements starting with a comment
    marker ("#" by default).
    """
    if isinstance(data, dict):
        return {k: remove_comments(v) for k, v in data.items()
                if is_not_comment(k)}
    elif isinstance(data, list):
        return [remove_comments(e) for e in data if is_not_comment(e)]
    else:
        return data

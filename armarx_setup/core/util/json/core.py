from typing import Any, Dict, List, Union

JsonData = Union[Dict, List, Any]


def make_axii_special_key(key: str):
    return "axii." + key

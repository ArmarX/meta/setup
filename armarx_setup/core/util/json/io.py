import json

from armarx_setup.core.util.json import comments, switch_os
from armarx_setup.core.util.json.core import JsonData

JSONEncoder = json.JSONEncoder
JSONDecodeError = json.JSONDecodeError


def load(
        filename: str,
        remove_comments=True,
        resolve_switch_os=True,
) -> JsonData:
    data = None
    with open(filename, "r") as file:
        try:
            data = json.load(file)
        except Exception:
            raise ValueError(f"Failed to load json file `{filename}`")
    assert data is not None

    if remove_comments:
        data = comments.remove_comments(data)

    if resolve_switch_os:
        data = switch_os.resolve_switch_os(data)

    return data


def store(
        data: JsonData,
        filename: str,
        indent=2,
        **kwargs,
):
    with open(filename, "w") as file:
        json.dump(data, file, indent=indent, **kwargs)
        file.write("\n")  # Add a new line at end of file.


def without_null(
        data: JsonData,
) -> JsonData:
    if isinstance(data, dict):
        return {
            k: without_null(v)
            for k, v in data.items()
            if v not in [None, ""]
        }
    else:
        return data

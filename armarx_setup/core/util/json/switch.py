from .core import JsonData, make_axii_special_key


def resolve_switch(
        data: JsonData,
        switch_key: str,
        case_name: str,
        default_case_name: str = "default",
        discard_none=True,
) -> JsonData:
    """
    Resolve switch constructs like this:
        {
           "some key": {
             "axii.switch_key": {
               "case_name": "case_value",
               "default_case_name": "default_value"
             }
           }
        }
    to (with case_name == "case_name")
        {
          "some key": "case_value"
        }
    or (with case_name != "case_name")
        {
          "some key": "default_value"
        }
    """

    # Keyword args for recursion.
    rec_kwargs = dict(switch_key=switch_key, case_name=case_name, default_case_name=default_case_name)

    key = make_axii_special_key(switch_key)

    if isinstance(data, dict):
        if key in data:
            cases = data[key]
            if case_name in cases:
                case = cases[case_name]
            else:
                case = cases[default_case_name]
            return resolve_switch(case, **rec_kwargs)

        else:
            result = dict()
            for k, v in data.items():
                v = resolve_switch(v, **rec_kwargs)
                if not (discard_none and v is None):
                    result[k] = v
            return result


    elif isinstance(data, list):
        result = []
        for e in data:
            e = resolve_switch(e, **rec_kwargs)
            if not (discard_none and e is None):
                result.append(e)
        return result

    else:
        return data

from .io import JSONEncoder, JSONDecodeError, load, store
from .extend import extend

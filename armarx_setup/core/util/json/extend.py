from .core import JsonData


def extend(base: JsonData, extension: JsonData) -> JsonData:
    if isinstance(extension, (dict, list)):
        if len(extension) == 0:
            return base.copy()

    if isinstance(extension, dict):
        if isinstance(base, str):
            # Turn e.g. `"build": "cmake"` into `"build": { "cmake: {} }`.
            base = {
                base: {}
            }

        assert isinstance(base, dict), f"base: \n{base}\ntype(base): {type(base)} \nextension: \n{extension}"
        base = base.copy()
        for k, v in extension.items():
            if k not in base:
                base[k] = v
            else:
                base[k] = extend(base[k], v)
        return base

    elif isinstance(extension, list):
        assert isinstance(base, list), f"base: \n{base}\nextension: \n{extension}"
        return base + extension

    else:
        # Just a value.
        return extension

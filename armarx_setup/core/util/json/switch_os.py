import dataclasses as dc
import typing as ty

from .core import JsonData
from .switch import resolve_switch


@dc.dataclass
class OsInfo:
    # e.g. "Ubuntu"
    distributor_id: str
    # e.g. "Ubuntu 20.04.4 LTS"
    description: str
    # "20.04"
    release: str
    # "focal"
    codename: str


# The OS usually does not change during execution,
# so we can cache the result.
os_info: ty.Optional[OsInfo] = None


def get_os_info() -> OsInfo:
    global os_info

    if os_info is None:
        import distro
        lsb = distro.lsb_release_info()
        os_info = OsInfo(**lsb)

    return os_info


def resolve_switch_os(
        data: JsonData,
        os_name: str = None,
) -> JsonData:
    if os_name is None:
        os_name = get_os_info().codename

    return resolve_switch(data, switch_key="switch_os", case_name=os_name)

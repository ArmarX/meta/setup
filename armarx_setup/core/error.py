import click
import typing as ty


class ArmarXSetupError(click.ClickException):
    """Base exception for this package."""


class CommandFailed(ArmarXSetupError):

    def __init__(self, cmd, msg="", stderr=None, summary=None):
        message = f"Running '{cmd}' failed"
        message += f" {msg}." if msg else "."
        super().__init__(message)

        self.stderr = stderr
        self.summary = summary


class CyclicDependencies(ArmarXSetupError):

    def __init__(self, caused_by: "nx.NetworkXUnfeasible"):
        super().__init__(caused_by)


class ModuleRequiresItself(ArmarXSetupError):

    def __init__(self, module_name: str):
        super().__init__(
            f"The module '{module_name}' requires itself. \n"
            f"Remove the entry '{module_name}' from the 'required modules' "
            f"of the definition of '{module_name}' and try again."
        )


class NoWorkspaceActive(ArmarXSetupError):

    def __init__(self):
        super().__init__(f"No workspace active.")


class NoSuchWorkspace(ArmarXSetupError):

    def __init__(self, workspace_name):
        super().__init__(f"No such workspace '{workspace_name}'.")

        self.workspace_name = workspace_name


class WorkspaceAlreadyExists(ArmarXSetupError):

    def __init__(self, name):
        super().__init__(f"Workspace '{name}' already exists.")

        self.name = name


class ModuleInfoNotFound(ArmarXSetupError):

    def __init__(self, name: str, tried_paths: ty.List[str], similar_modules=None, database_module=None):
        super().__init__(
            f"Module information for '{name}' not found. Tried: "
            + "".join(f"\n  {path}" for path in tried_paths)
        )

        self.name = name
        self.similar_modules = similar_modules
        self.database_module = database_module


class ModuleAlreadyExists(ArmarXSetupError):

    def __init__(self, name: str):
        super().__init__(f"Module '{name}' already exists.")

        self.name = name


class NoSuchModule(ArmarXSetupError):

    def __init__(self, name: str, similar_modules=None):
        super().__init__(f"There is no module with name '{name}.'")

        self.name = name
        self.similar_modules = similar_modules


class UnsupportedShell(ArmarXSetupError):

    def __init__(self, path):
        super().__init__(f"Unsupported shell in $SHELL='{path}'.")


class NoShellRunComFile(ArmarXSetupError):

    def __init__(self, shell, run_com_file):
        super().__init__(f"No shell run com (rc) file found for {shell} in '{run_com_file}'.")


class NoSuchConfigVariable(ArmarXSetupError):

    def __init__(self, variable_name):
        super().__init__(f"No such config variable '{variable_name}'.")

        self.variable_name = variable_name


class NotYetImplemented(ArmarXSetupError):

    def __init__(self, feature):
        super().__init__(f"This feature ({feature}) is not yet implemented. Search for open issues or open a new one "
                         f"to raise awareness.")

        self.feature = feature

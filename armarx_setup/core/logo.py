def make_logo(alt_mode=False):
    slants = "▟ ▙"
    slants = "▜ ▛"
    w = "#FFFFFF"  # white
    l = "#9dc3e7"  # light blue
    b = "#073763"  # dark blue
    if alt_mode:
        l = "#e9afb2"  # light red
        b = "#5a221e"  # dark red
    tw = f"[{w}]"  # text white
    tl = f"[{l}]"  # text light blue
    tb = f"[{b}]"  # text dark blue
    ow = f"[on {w}]"  # on white
    ol = f"[on {l}]"  # on light blue
    ob = f"[on {b}]"  # on dark blue
    s = "[/]"  # stop

    text = \
    rf"""
{tb}▟{s}{tw}{ob}                        {s}{tb}▙{s}{ob}|
  ▜{ow}                    {s}▛  |
   ▜{ow}  {s}▙                   |
    ▜{ow}  {s}▙          ▟{ow}  {s}▛    |
     ▜{ow}  {s}▙        ▟{ow}  {s}▛     |
      ▜{ow}  {s}▙      ▟{ow}  {s}▛      |
       ▜{ow}  {s}▙    ▟{ow}  {s}▛       |
        ▜{ow}  {s}▙  ▟{ow}  {s}▛        |
   {tl}▜{ol} {s}▙  ▟{ol} {s}{ow}▛ {ob}{tw}▙▟{s}{s}  {s}{tw}▛{s}         |
    {tl}▜{ol} {s}▙▟{ol} {s}▛{s}{tw}▜{ow}    {s}▛          |
    {tl}▟{ol} {s}▛▜{ol} {s}▙{s}{tw} ▜{ow}  {s}▛           |
   {tl}▟{ol} {s}▛  ▜{ol}_{s}▙{s}{tw} ▜{ow}{s}▛            |
{s}{s}{s}{s}{s}{tb}▜{s}{ob}                        {s}{tb}▛{s}{ob}|
"""

    # Remove right end markers.
    text = "\n".join(line.rstrip("|") for line in text.split("\n"))
    # Remove excessive new lines.
    text = text.strip("\n")
    return text


def make_logo_in_panel(
        alt_mode=False,
        **panel_kwargs,
):
    from rich.align import Align
    from rich.panel import Panel
    from rich import box

    panel_kwargs = {**dict(box=box.MINIMAL), **panel_kwargs}

    text = make_logo(alt_mode=alt_mode)
    panel = Panel(Align(text, align="center"), **panel_kwargs)
    return panel


if __name__ == '__main__':
    from rich.console import Console
    console = Console()
    console.print(make_logo_in_panel(alt_mode=False, title="This is AXII"))

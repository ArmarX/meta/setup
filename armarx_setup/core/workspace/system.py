import enum

from typing import Dict, List, Optional, Union

from armarx_setup import console
from armarx_setup.core import error
from armarx_setup.core.util import prefix_color_map
from armarx_setup.core.module import Module

from .workspace import Workspace


class SystemStep:

    class Type(enum.IntEnum):
        Run = 1
        Packages = 2
        Env = 3
        Source = 4

        @classmethod
        def of_module(cls, module: Module) -> "SystemStep.Type":
            if module.system.run:
                return cls.Run
            elif module.system.packages:
                return cls.Packages
            elif module.system.env:
                return cls.Env
            elif module.system.source:
                return cls.Source
            else:
                raise ValueError(f"Unhandled module {module.name} by enum {cls.__name__}.")

    def __init__(
            self,
            type: Type,
            modules: List[Module] = None,
            ws: Optional[Workspace] = None,
            ):
        self.type = type
        self.modules = modules or []
        self.ws = ws

    def __repr__(self):
        return "<{c} type={t} #modules={nm}>".format(
            c=self.__class__.__name__,
            t=self.type,
            nm=len(self.modules)
        )

    def run(self):
        assert self.type == self.Type.Run
        for module in self.modules:
            assert module.system is not None
            module.system.run()

    def install_packages(self):
        assert self.type == self.Type.Packages

        packages = self.gather_apt_packages()

        if packages["found"]:
            found: Dict[str, List[Module]] = packages["found"]
            Module.say_num_packages_are_installed(len(found))
            console.print()
            console.print(self.to_table_packages_and_dependent_modules(found))

        if packages["missing"]:
            from armarx_setup.core.util import system

            missing: Dict[str, List[Module]] = packages["missing"]

            console.print()
            console.print(f"The following {len(missing)} apt packages are required but not installed yet:")
            console.print(self.to_table_packages_and_dependent_modules(missing))

            system.install_apt_packages(list(missing))

    def gather_apt_packages(
            self,
            max_workers=8,
            ) -> Dict[str, Union[Dict[str, List[Module]], List[Module]]]:
        from concurrent import futures
        from rich.live import Live

        with Live("", refresh_per_second=30) as live:
            jobs = []
            with futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
                def job(module: Module):
                    module_text = prefix_color_map.module_to_colored_text(module.name)

                    live.update(f"Checking apt packages of module {module_text} ...")
                    checked: Dict[str, List[str]] = module.system.check_apt_packages()
                    return checked, module

                for module in self.modules:
                    assert module.system
                    assert module.system.packages
                    jobs.append(executor.submit(job, module))

        gathered = {
            "found": {},
            "missing": {}
        }
        for job in jobs:
            checked, module = job.result()
            for key in checked:
                packages_dict = gathered[key]
                for package in checked[key]:
                    if package not in packages_dict:
                        packages_dict[package] = []
                    packages_dict[package].append(module)

        return gathered

    def to_table_packages_and_dependent_modules(
            self,
            packages_and_dependent_modules: Dict[str, List[Module]],
            ):
        from rich.table import Table
        table = Table("#", "Apt Package Name", "Myodule(s)", "Directly Dependent Modules")
        table.columns[0].justify = "right"

        for i, (package, modules) in enumerate(sorted(packages_and_dependent_modules.items())):
            dependent_modules = set()
            for m in modules:
                for (source, target) in self.ws.module_graph.in_edges(m.name):
                    dependent_modules.add(source)
            table.add_row(
                str(i + 1),
                package,
                ", ".join(prefix_color_map.modules_to_colored_text(sorted([m.name for m in modules]))),
                ", ".join(prefix_color_map.modules_to_colored_text(sorted(dependent_modules)))
            )
        return table


class System:

    def __init__(self, ws: Workspace):
        self.ws = ws

    def plan_steps(self, modules: List[Module]) -> List[SystemStep]:

        steps: List[SystemStep] = [
            # Start with a run step, if required.
            SystemStep(type=SystemStep.Type.Run, ws=self.ws),
        ]

        def find_dependency(module_name: str):
            # This must always succeed as dependencies are processed before their dependents.
            # next(filter()) implements a find_if()
            filtered = filter(lambda i_step: module_name in [m.name for m in i_step[1].modules], enumerate(steps))
            try:
                return next(filtered)
            except StopIteration:
                console.print(f"module that was depended on: {module_name}")
                raise

        def check_dependencies(module_name_to_insert: str, target_step_index: int):
            for dep in self.ws.module_graph.adj[module_name_to_insert]:
                if self.ws.module_graph.nodes[dep]["module"].system is None:
                    module = f"'{module_name_to_insert}'"
                    suggested_new_module = f"'{module_name_to_insert}-system-requirements"
                    lines = [
                        f"The module {module} specifies a system step, "
                        f"but requires '{dep}', which does not specify a system step.",
                        f"System modules may only require modules with a system step. "
                        f"(Otherwise, respecting requirements is not possible when setting up the system up-front).",
                        f"To fix this issue:",
                        f"If {module} specifies a system/packages/apt step:",
                        f"  (1.) For each apt package 'package', add a required module 'apt/package' to {module}.",
                        f"       If there is no corresponding module description (.json file in the module database),",
                        f"       the required 'apt/...' module will be emulated dynamically.",
                        f"  (2.) Remove the system/packages/apt step.",
                        f"If there are other system steps in {module}:",
                        f"  (1.) Create a new module, e.g. {suggested_new_module}.",
                        f"  (2.) Move the system step from {module} to {suggested_new_module}.",
                        f"  (3.) Add {suggested_new_module} to the required modules of {module}.",
                    ]
                    raise error.ArmarXSetupError("\n".join(lines))

                # console.print(f"> {dep}")
                dep_i, dep_step = find_dependency(dep)
                if dep_i > target_step_index:
                    return False
            return True

        def add_to_first_fitting_step(module: Module):
            step_type = SystemStep.Type.of_module(module)
            for step_i, step in filter(lambda i_s: i_s[1].type == step_type, enumerate(steps)):
                if check_dependencies(module.name, step_i):
                    step.modules.append(module)
                    return True
            return False

        for i, module in enumerate(modules):
            # print(f"[{i}] {module.name}")
            # console.print(f"[b]{module.name}[/]")
            if not add_to_first_fitting_step(module):
                steps.append(SystemStep(type=SystemStep.Type.of_module(module), modules=[module], ws=self.ws))

        steps += [
            SystemStep(type=SystemStep.Type.Env, modules=[m for m in modules if m.system.env is not None], ws=self.ws)
        ]

        return steps

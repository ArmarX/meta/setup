import os

from typing import Dict, Optional

from armarx_setup.core.util import json, dataclass
from armarx_setup.core.module import Module


class Config:

    def __init__(
            self,
            modules: Dict[str, Dict] = None,
            global_: Dict = None,
            file_path: Optional[str] = None,
            **kwargs
    ):
        self.modules = modules or dict()
        if isinstance(self.modules, list):
            self.modules = {m: {} for m in self.modules}

        ws_path = os.path.dirname(file_path) if file_path is not None else ""
        self.global_json: Dict = kwargs.get("global", global_ or None)
        self.global_ = dataclass.sanitize_type(
            self.global_json, Module,
            name=Module.GLOBAL_MODULE_NAME, ws_path=ws_path, ws_config=self)

    @classmethod
    def get_defaults(cls):
        return cls(
            modules={
                "# apt/ccache": {},
                "armarx": {},
                "h2t/code_style": {}
            },
            global_={
                "prepare": {
                    "cmake": {
                        "definitions": {
                            "CMAKE_BUILD_TYPE": "RelWithDebInfo",
                            "# CMAKE_C_COMPILER_LAUNCHER": "$CCACHE",
                            "# CMAKE_CXX_COMPILER_LAUNCHER": "$CCACHE",
                        }
                    }
                },
                "build": {
                    "cmake": {
                        "env": {
                            "CMAKE_BUILD_PARALLEL_LEVEL": "4"
                        }
                    }
                }
            },
        )

    @classmethod
    def load(cls, file_path: str):
        if os.path.isfile(file_path):
            loaded = json.load(file_path)
            self = cls(**loaded)
            # print("Config:", self.dumps())
            return self

        else:
            raise IOError(f"The workspace config file '{file_path}' was not found.")

    def store(self, file_path: str):
        os.makedirs(os.path.dirname(file_path), exist_ok=True)
        json.store(self.as_dict(), file_path)

    def dumps(self):
        import json
        return json.dumps(self.as_dict(), indent=2)

    def as_dict(self):
        data = {
            "modules": self.modules,
        }

        if self.global_json is not None:
            data["global"] = self.global_json

        return data

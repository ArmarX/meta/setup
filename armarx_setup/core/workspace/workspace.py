import json
import os
import re
import typing as ty

from rich.text import Text
from rich.rule import Rule
from typing import Any, Callable, Dict, List, Optional
from xdg import xdg_config_home

from armarx_setup import console, PACKAGE_ROOT
from armarx_setup.core import error


ARMARX_WORKSPACE_ENV_VAR = "ARMARX_WORKSPACE"


class Workspace:

    def __init__(
            self,
            name: str,
            path: str,
            config: Optional["Config"] = None,
            enable_env_file=True,
            ):
        from armarx_setup.core.workspace.config import Config
        from armarx_setup.core.workspace.env import EnvFile

        if path.endswith("/"):
            path = path[:-len("/")]

        self.name = name
        self.path = path
        self.config = config or Config()

        if enable_env_file:
            self.env_file = EnvFile(ws_path=self.path)
            self.env_file.add(ARMARX_WORKSPACE_ENV_VAR, path, by="workspace", quiet=True)
            self.env_file.add(f"{ARMARX_WORKSPACE_ENV_VAR}_NAME", self.name, by="workspace", quiet=True)
            self.env_file.add("ARMARX_USER_CONFIG_DIR", os.path.join(path, "armarx_config"), by="workspace", quiet=True)
        else:
            self.env_file = None

        self._module_graph: Optional["ModuleGraph"] = None

    @classmethod
    def load_workspace_by_name(cls, name: str):
        known_workspaces = Workspace.read_known_workspaces()
        try:
            path = known_workspaces[name]["path"]
        except KeyError:
            raise error.NoSuchWorkspace(workspace_name=name)
        else:
            ws = Workspace(name=name, path=path)
            ws.load_config()
            return ws

    @classmethod
    def load_workspace_by_path(
            cls,
            path: str
            ) -> "Workspace":
        if not os.path.isdir(path):
            raise error.NoSuchWorkspace(workspace_name=path)

        name = path
        try:
            known_workspaces = Workspace.read_known_workspaces()
            for known_workspace_name, known_workspace in known_workspaces.items():
                if known_workspace["path"] == path:
                    name = known_workspace_name
                    break
            else:  # If workspace was not found in known_workspaces.json, add it with name=path.
                known_workspaces[path] = {
                    "path": path
                }
                Workspace.write_known_workspaces(known_workspaces)
        except Exception as e:
            print(e)

        ws = Workspace(name=name, path=path)
        ws.load_config()
        return ws

    @classmethod
    def get_active_workspace_dir(cls):
        path = os.environ.get(ARMARX_WORKSPACE_ENV_VAR, None)
        if not path:
            raise error.NoWorkspaceActive()
        path = os.path.expandvars(os.path.expanduser(path))
        return path

    @classmethod
    def load_active_workspace(cls):
        path = cls.get_active_workspace_dir()
        return cls.load_workspace_by_path(path)

    @classmethod
    def read_known_workspaces(cls):
        known_workspaces = {}

        armarx_setup_config_dir = os.path.join(xdg_config_home(), "axii")
        os.makedirs(armarx_setup_config_dir, exist_ok=True)
        known_workspaces_file = os.path.join(armarx_setup_config_dir, "known_workspaces.json")

        try:
            with open(known_workspaces_file) as f:
                known_workspaces = json.load(f)
        except:
            pass

        return known_workspaces

    @classmethod
    def write_known_workspaces(cls, known_workspaces):
        armarx_setup_config_dir = os.path.join(xdg_config_home(), "axii")
        known_workspaces_file = os.path.join(armarx_setup_config_dir, "known_workspaces.json")

        try:
            os.makedirs(armarx_setup_config_dir)
        except:
            pass

        with open(known_workspaces_file, 'w') as f:
            json.dump(known_workspaces, f, indent=2)

    @classmethod
    def get_all_workspace_names(cls):
        return cls.read_known_workspaces().keys()

    def create(self):
        """Create directories and store the config."""
        import collections
        info = collections.OrderedDict()

        name = self.name
        path = self.path
        config_path = self.get_config_path(path)

        info["Workspace name"] = name
        info["Workspace path"] = path

        os.makedirs(path, exist_ok=True)
        assert os.path.isdir(path), path

        info["Workspace config"] = config_path

        self.store_config()

        info["Workspace environment file"] = self.env_file.get_path()

        self.env_file.write()

        known_workspaces = Workspace.read_known_workspaces()
        known_workspaces[name] = {
            'path': path
        }
        Workspace.write_known_workspaces(known_workspaces)

        return info

    def load_config(self) -> "Config":
        from armarx_setup.core.workspace.config import Config

        try:
            self.config = Config.load(self.get_config_path())
        except IOError:
            raise
        return self.config

    def store_config(self):
        self.config.store(self.get_config_path())

    def get_config_path(self, path=None):
        if path is None:
            path = self.path
        return os.path.join(path, "armarx-workspace.json")

    def get_local_axii_config_path(self, path=None):
        if path is None:
            path = self.path
        return os.path.join(path, ".axii")

    def add_module(
            self,
            module_name: str,
            check=True,
            store=True,
    ):
        from armarx_setup.core.module import Module

        module_name = module_name.strip("/")
        if module_name in self.config.modules:
            console.print(f"Module '{module_name}' already added.")
            return False

        else:
            if check:
                Module.load_info(module_name, ws=self)

            self.config.modules[module_name] = {}
            self._module_graph = None
            if store:
                self.store_config()
                console.print(f"Added module '{module_name}'.")

        return True

    def add_module_features(
            self,
            module_name: str,
            feature_names: List[str],
            check=True,
            store=True,
    ):
        from armarx_setup.core.module import Module

        if module_name not in self.config.modules:
            if self.add_module(module_name=module_name, store=False):
                console.print(f"Added module '{module_name}'.")
            else:
                return  # Failed to add module.

        if check:
            module: Module = Module.load_info(module_name)
            for feature_name in feature_names:
                if feature_name not in module.features:
                    console.print(f"Module '{module_name}' does not provide a feature named '{feature_name}'.")

                    similar = Module.get_similar_module_names(feature_name, module.features.keys())
                    if similar:
                        console.print("\tSimilar feature names: " + ", ".join(f"'{c}'" for c in similar))

                    return False

        module_dict: Dict = self.config.modules[module_name]
        feature_names = set(feature_names)
        current_features = set(module_dict.get("features", []))

        merged_features = current_features.union(feature_names)
        already_added = current_features.intersection(feature_names)
        new_features = feature_names.difference(current_features)

        def str_feature_numerus(features):
            subject = "Feature"
            are = "is"
            if len(features) > 1:
                subject += "s"
                are = "are"
            return subject, are

        def str_feature_names(features) -> str:
            return ", ".join(f"'{f}'" for f in features)

        def str_already_added(features):
            subject, are = str_feature_numerus(features)
            return f"{subject} {str_feature_names(features)} of module '{module_name}' {are} already added."

        if len(new_features) == 0:
            console.print(str_already_added(feature_names))
            return False

        module_dict["features"] = list(merged_features)
        if store:
            self.store_config()
            subject, _ = str_feature_numerus(new_features)
            if already_added:
                console.print(str_already_added(already_added))
            console.print(f"Added {subject.lower()} {str_feature_names(new_features)} of module '{module_name}'.")

        return True

    def remove_module(self, module_name: str, store=True):
        module_name = module_name.strip("/")
        if module_name not in self.config.modules:
            console.print(f"Module '{module_name}' is not in the current workspace.")

            from armarx_setup.core.module import Module
            similar = Module.get_similar_module_names(module_name, self.config.modules)
            if similar:
                console.print("Similar module names: " + ", ".join(f"'{c}'" for c in similar))

        else:
            del self.config.modules[module_name]
            self._module_graph = None
            if store:
                self.store_config()
                console.print(f"Removed module '{module_name}'.")

    @property
    def module_graph(self) -> "ModuleGraph":
        if self._module_graph is None:
            self._module_graph = self.build_module_graph()
        return self._module_graph

    @module_graph.setter
    def module_graph(self, value: "ModuleGraph"):
        self._module_graph = value

    def build_module_graph(self) -> "ModuleGraph":
        from armarx_setup.core.module_graph import ModuleGraph

        # console.print("Build module graph ...", end="")
        graph = ModuleGraph.from_modules(self.config.modules, ws=self)
        # console.print(" Done.")
        return graph

    def for_each_module_in_dependency_chain(
            self, func: Callable[..., Any], *args, **kwargs
            ) -> Dict[str, Any]:
        return {module.name: func(module, *args, **kwargs)
                for module in self.module_graph.get_dependency_chain()}

    def get_modules_in_dependency_chain(self) -> List["Module"]:
        return list(self.module_graph.get_dependency_chain())

    def for_each_module_in_alphabetical_order(
            self, func: Callable[..., Any], *args, **kwargs
            ) -> Dict[str, Any]:
        
        modules = list(self.module_graph.nodes(data="module"))
        modules.sort()
        
        return {module.name: func(module, *args, **kwargs)
                for _, module in modules}

    def get_modules_in_alphabetical_order(self) -> List["Module"]:
        return [module for _, module in self.module_graph.nodes(data="module")]

    def info(self, graph=False, layout=None, reduce=False, all=False):
        from armarx_setup.core.util import prefix_color_map

        def make_table():
            from rich.table import Table
            table = Table("#", "Module", "Required Modules", title="Added Modules and their Direct Dependencies")
            table.columns[0].justify = "right"
            for i, module_name in enumerate(sorted(self.config.modules)):
                required = sorted(self.module_graph.nodes[module_name]["module"].required_modules)
                if not all:
                    required = self.module_graph.filter_hidden_modules(required)
                required_modules = ", ".join(prefix_color_map.modules_to_colored_text(required))
                if not required_modules:
                    required_modules = "[italic]none[/]"
                table.add_row(str(i), prefix_color_map.module_to_colored_text(module_name), required_modules)
            return table

        table = make_table()
        console.print(table)
        console.print()

        def make_table():
            from rich.table import Table
            table = Table("#", "Module", "Required by", title="Complete List of Modules")
            table.columns[0].justify = "right"
            for i, node in enumerate(sorted(self.module_graph.nodes)):
                module = self.module_graph.nodes[node]["module"]

                if not all and self.module_graph.is_hidden_module(module.name):
                    continue

                required_by = {self.module_graph.nodes[f]["module"].name: d for f, _, d in
                               self.module_graph.in_edges(node, data="dependency")}

                required_by_list = []
                if module.name in self.config.modules:
                    required_by_list = ['[strong]Workspace config[/]']

                for dependency_name, dependency in required_by.items():
                    text = prefix_color_map.module_to_colored_text(dependency_name)
                    if dependency.features:
                        features = ', '.join(f'{f}' for f in dependency.features)
                        text += f' (with features: [italic]{features}[/])'
                    elif dependency.feature_name is not None:
                        text += f' (from feature [italic]{dependency.feature_name}[/])'
                    required_by_list.append(text)

                table.add_row(str(i), prefix_color_map.module_to_colored_text(module.name), ", ".join(required_by_list))
            return table

        table = make_table()
        console.print(table)

        import networkx as nx
        try:
            chain = self.module_graph.get_dependency_chain()
        except nx.NetworkXUnfeasible as e:
            console.print(f"Could not infer dependency chain due to cyclic dependencies: {e}")
        else:
            if not all:
                chain = self.module_graph.filter_hidden_modules(chain)

            text_chain = " -> ".join(prefix_color_map.modules_to_colored_text(chain))
            console.print()
            console.print(f"Flattened dependency chain:\n{text_chain}")

        if not all:
            console.print()
            console.print(f"This is a condensed view excluding some types of modules (e.g. pure system modules). "
                          f"To include them, re-run with '--all'.")

        if graph:
            console.print()
            console.print("Show module graph ...")
            self.module_graph.visualize(all=all, layout=layout, reduce=reduce)

    def prepare_local_axii_config_path(self):
        import shutil

        path = os.path.join(self.get_local_axii_config_path(), "bin")
        try:
            shutil.rmtree(path)
        except FileNotFoundError:
            pass
        except OSError:
            # Might happen on file server with .nfs0000... files which cannot be deleted.
            pass
        os.makedirs(path, exist_ok=True)

    def system(self, **kwargs):
        from armarx_setup.core.util import prefix_color_map
        from armarx_setup.core.workspace.system import System, SystemStep

        # First gather all apt packages required by modules, then perform the install step.
        def enlist(module: "Module"):
            if module.system is not None:
                return module

        modules = [m for m in self._step_with_options(enlist, verb="setup the system for", **kwargs).values()
                   if m is not None]

        system = System(ws=self)
        steps = system.plan_steps(modules)

        verbose = 1
        if verbose >= 1:
            console.print("[b]System steps:[/]")
            for i, step in enumerate(steps):
                console.print(f"[{i+1}] {step.type.name + ':':<9}", ", ".join(prefix_color_map.modules_to_colored_text([m.name for m in step.modules])))

        for step in steps:

            if step.type == SystemStep.Type.Run:
                console.print("Run system scripts for modules", ", ".join(prefix_color_map.modules_to_colored_text([m.name for m in step.modules])))
                step.run()

            elif step.type == SystemStep.Type.Packages:
                console.print("Install system packages for modules", ", ".join(prefix_color_map.modules_to_colored_text([m.name for m in step.modules])))
                step.install_packages()

        console.print("Done with system steps.\n")

    def update(
            self,
            parallel=False,
            **kwargs,
    ):
        self.update_parallel(max_workers=32 if parallel else 1, **kwargs)
        # Old school way:
        # self.for_each_module_in_dependency_chain(self.update_module)

    def update_parallel(
            self,
            max_workers=8,
            **kwargs,
    ):
        from concurrent import futures
        from threading import Lock

        with futures.ThreadPoolExecutor(max_workers=max_workers) as executor:

            # These variables are only used by the main thread.
            pending = set()

            # These variables are shared by threads.
            lock = Lock()
            module_states = dict()

            # Fetch on Axii's repo to display update notifications.
            def axii_fetch():
                from armarx_setup.self import self_update
                try:
                    self_update.check_for_updates()
                except:
                    pass
            executor.submit(axii_fetch)

            def submit(m: "Module", ignore_errors=False):

                if m.update:
                    min_result_width = 40

                    module_states[m.name] = {
                        "status": "[grey]Pending",
                        "summary": "".ljust(min_result_width),
                        "output": ""
                    }
                    pending.add(m.name)

                    def job():
                        try:
                            with lock:
                                module_states[m.name]["status"] = "[blue]Running ...[/]"
                            color = "green"
                            status = "Succeeded"
                            summary = None

                            try:
                                summary, output = m.update(capture_output=True, quiet=True)
                            except error.CommandFailed as e:
                                color = "red"
                                status = "Failed"
                                output = str(e) + "\n" + e.stderr
                                summary = e.summary
                            except Exception as e:
                                color = "red"
                                status = "Failed"
                                output = f"Caught exception ({type(e)}) while updating module '{m.name}': {e}"
                                summary = f"{e}"
                            else:
                                output = "\n".join([line.rstrip() for line in output.split("\n")])

                            if summary is None:
                                summary = "(see output)"

                            with lock:
                                state = module_states[m.name]
                                state["status"] = f"[{color}]{status}[/]"
                                state["summary"] = summary.strip().ljust(min_result_width)
                                state["output"] = output

                        except Exception as e:  # Only as a last resort.
                            console.print(f"Caught exception while updating module '{m.name}': {e}")
                            raise e

                    return executor.submit(job)

            module_to_future = self._step_with_options(
                submit, verb="update", **kwargs, sort=True,
            )

            import time
            from rich.table import Table
            from rich.live import Live
            from armarx_setup.core.util import prefix_color_map

            def generate_table(time_passed: Optional[float] = None):
                spin_states = r'\|/–'
                update_spin_each = 0.2
                spinner = spin_states[int(time_passed / update_spin_each) % len(spin_states)] \
                    if time_passed is not None else ""
                ellipsis_states = ['...', '   ', '.  ', '.. ']
                update_ellipsis_each = 0.5
                ellipsis = ellipsis_states[int(time_passed / update_ellipsis_each) % len(ellipsis_states)] \
                    if time_passed is not None else '...'
                table = Table("Module", f"Status {spinner}", "Summary",
                              title=f"Updating modules ...")
                table.columns[1].width = 12
                for module, data in module_states.items():
                    table.add_row(prefix_color_map.module_to_colored_text(module), data["status"].replace('...', ellipsis), data["summary"])
                return table

            with Live(generate_table(), refresh_per_second=10, console=console) as live:
                time_passed = 0
                time_increment = 0.1

                while pending:
                    for module_name, future in module_to_future.items():
                        future: futures.Future
                        if module_name in pending and (future.done() or future.cancelled()):
                            # Fetch the result and free the future.
                            future.result()
                            console.print()
                            console.print(f"[b]Update module[/] {prefix_color_map.module_to_colored_text(module_name)} ...")
                            console.print(f"{module_states[module_name]['output']}")
                            pending.remove(module_name)

                    time.sleep(time_increment)
                    time_passed += time_increment
                    live.update(generate_table(time_passed=time_passed))
                live.update(generate_table())

    def update_module(self, module: "Module"):
        if module.update:
            console.print(f"[b]Update module '{module.name}' ...[/]")
            module.update()

    def prepare(self, **kwargs):
        self._step_with_options(self.prepare_module, verb="prepare", **kwargs)

    def prepare_module(self, module: "Module", ignore_errors=False):
        from armarx_setup.core.util import prefix_color_map

        if module.prepare:
            console.print()
            console.print(f"[b]Prepare module[/] {prefix_color_map.module_to_colored_text(module.name)} ...")
            try:
                module.prepare()

            except error.CommandFailed as e:
                self.notify(f"Preparing {module.name} failed", str(Text.from_markup(f"{e}")), "failure")
                console.print()
                console.print(f"[yellow]Preparing {module.name} failed: \n{e}[/]")
                if not ignore_errors:
                    raise

    def build(self, **kwargs):
        self._step_with_options(self.build_module, verb="build", **kwargs)

    def build_module(self, module: "Module", ignore_errors=False, clean=False):
        from armarx_setup.core.util import prefix_color_map

        if module.build:
            if clean:
                module.clean()
            else:
                console.print()
                console.print(f"[b]Build module[/] {prefix_color_map.module_to_colored_text(module.name)} ...")
                try:
                    module.build()

                except error.CommandFailed as e:
                    self.notify(f"Building {module.name} failed", str(Text.from_markup(f"{e}")), "failure")
                    console.print()
                    console.print(f"[yellow]Building {module.name} failed: \n{e}[/]")
                    if not ignore_errors:
                        raise

    def _step_with_options(
            self,
            module_fn: ty.Callable,
            verb: str = "",
            module: str = None,
            path: str = None,
            file: str = None,
            no_deps=False,
            this=False,
            sort=False,
            **kwargs
    ) -> ty.Dict[str, ty.Any]:
        from armarx_setup.core.util import prefix_color_map

        # Handle the deprecated file arg.
        if file is not None:
            if path is None:
                path = file
            else:
                raise error.ArmarXSetupError(
                    "The option 'file' is a legacy alias for 'path', "
                    "so you may specify either one of them, but not both.")

        # We do not need this variable anymore.
        del file

        num_specified = sum([module is not None, path is not None, this])
        if num_specified > 1:
            raise error.ArmarXSetupError("You may specify either 'module', 'path' or 'this', but not more than one.")

        # Resolve.
        if module is not None:
            module = self.module_graph.get_module_with_name(module)
        module: ty.Optional[Module]

        if this:
            path = os.getcwd()

        if path is not None:
            path = os.path.abspath(os.path.expandvars(path))
            if verb:
                console.print(f"Requested to {verb} module explaining the path '{path}'.")
            module = self.module_graph.find_module_explaining_path(path, ws_path=self.path)


        if module is not None:
            console.print(f"Found module {prefix_color_map.module_to_colored_text(module.name)}.")
            if no_deps:
                chain = [module]
            else:  # Modules with their dependencies.
                chain = self.module_graph.get_dependency_chain_to_module(module)

            if sort:
                chain = sorted(chain, key=lambda m: m.name)
            return {
                module.name: module_fn(module, **kwargs)
                for module in chain
            }

        else:
            if sort:
                return self.for_each_module_in_alphabetical_order(module_fn, **kwargs)
            else:
                return self.for_each_module_in_dependency_chain(module_fn, **kwargs)


    def install(self, **kwargs):
        self.prepare_local_axii_config_path()

        self._step_with_options(self.install_module, verb="install", **kwargs)

    def install_module(self, module: "Module", ignore_errors=False):
        from armarx_setup.core.util import prefix_color_map

        if module.install:
            try:
                console.print()
                console.print(f"[b]Install module[/] {prefix_color_map.module_to_colored_text(module.name)} ...")
                module.install()
            except error.CommandFailed as e:
                self.notify(f"Installing {module.name} failed", str(Text.from_markup(f"{e}")), "failure")
                console.print()
                console.print(f"[yellow]Installing {module.name} failed: \n{e}[/]")
                if not ignore_errors:
                    raise

    def conclude(self):
        self.for_each_module_in_dependency_chain(self.conclude_module)

    def conclude_module(self, module: "Module"):
        from armarx_setup.core.util import prefix_color_map

        if module.conclude:
            try:
                console.print()
                console.print(f"[b]Conclude with module[/] {prefix_color_map.module_to_colored_text(module.name)} ...")
                module.conclude()
            except error.CommandFailed as e:
                self.notify(f"Concluding with {module.name} failed", str(Text.from_markup(f"{e}")), "failure")
                console.print()
                console.print(f"[yellow]Conclude with {module.name} failed: \n{e}[/]")

    def clean(self, **kwargs):
        self.for_each_module_in_dependency_chain(lambda m: m.clean())

    def test(self, **kwargs):
        from rich.table import Table
        from armarx_setup.core.util import prefix_color_map

        results = self.for_each_module_in_dependency_chain(self.test_module, **kwargs)

        table = Table("Module", "Result", title="Summary of Test Results")
        for module_name, result in results.items():
            if result is not None:
                if result:
                    result = "[green]Success[/]"
                else:
                    result = "[red]Failed[/]"
                table.add_row(
                    prefix_color_map.module_to_colored_text(module_name),
                    str(result),
                )
        console.print(table)

    def test_module(self, module: "Module", build: bool, **kwargs):
        from armarx_setup.core.util import prefix_color_map

        if module.prepare is not None and not module.name.startswith("deps/"):
            if build and module.build:
                self.build_module(module)

            console.print()
            console.print(f"[b]Test module[/] {prefix_color_map.module_to_colored_text(module.name)} ...")
            return module.prepare.test(**kwargs)

    def generate_env(self):
        global_module: "Module" = self.config.global_
        if global_module is not None and global_module.install is not None:
            global_module.install.generate_env(self.env_file)

        self.export_module_paths(self.env_file)

        console.print(Rule(title="Setup environment"))

        def gen_steps_env(steps: List["Hook"]):
            for step in steps:
                if step is not None:
                    step.generate_env(self.env_file)

        self.for_each_module_in_dependency_chain(
            lambda m: gen_steps_env([m.system]))
        self.for_each_module_in_dependency_chain(
            lambda m: gen_steps_env([m.update, m.prepare, m.build, m.install]))

        self.env_file.add("PATH", "$ARMARX_WORKSPACE/.axii/bin:$PATH", by="workspace executables")

        console.print(Rule())
        console.print()

        self.env_file.write()

    def export_module_paths(self, env: "EnvFile"):
        console.print(Rule(title="Export module root paths"))

        def per_module(module: "Module"):
            if module.update:
                var_name = module.name
                var_name = var_name.replace("/", "__")
                # Replace any non-alphanumeric characters by '_'.
                var_name = re.compile(r"\W").sub("_", var_name)
                var_name = var_name + "__PATH"
                env.add(var_name, module.path, by="module graph", write=True)

        self.for_each_module_in_dependency_chain(per_module)
        console.print(Rule())
        console.print()

    def upgrade(
            self,
            env=True,
            system=True,
            update=True,
            prepare=True,
            build=True,
            install=True,
            conclude=True,
            test=False,
            ignore_errors=False,
            parallel_update=True,
            **kwargs,
            ):
        from armarx_setup.core.util import cli, prefix_color_map

        self.prepare_local_axii_config_path()

        if env:
            self.generate_env()

        if system:
            self.system(**kwargs,)

        if update:
            self.update(parallel=parallel_update, **kwargs)

        def upgrade_module(module: "Module"):
            if not any([module.prepare, module.build, module.install]):
                return

            color = prefix_color_map.module_to_color(module.name)
            module_text = prefix_color_map.module_to_colored_text(module.name)
            cli.print_full_width_heading(f"[b]Upgrade module[/] {module_text} ...", sep_color=color)

            try:
                if prepare:
                    self.prepare_module(module)
                if build:
                    self.build_module(module)
                if install:
                    self.install_module(module)
            except error.CommandFailed:
                if not ignore_errors:
                    raise

        try:
            self._step_with_options(upgrade_module, verb="upgrade", **kwargs)

        except error.CommandFailed as e:
            pass

        else:
            if conclude:
                self.conclude()

            if test:
                self.test(build=False)

    def auto_remove(self, dry_run=False):
        def module_filter(m):
            return os.path.join(self.path, m.name)

        paths_to_remove = []
        legit_paths = [m for m in self.for_each_module_in_dependency_chain(module_filter).values()
                       if m is not None]
        legit_paths += [os.path.join(self.path, ".axii"), os.path.join(self.path, "armarx_config")]

        for root, paths, files in os.walk(self.get_active_workspace_dir()):
            for legit_path in legit_paths:
                if root.startswith(legit_path) or root in legit_path:
                    break
            else:
                for path_to_remove in paths_to_remove:
                    if root.startswith(path_to_remove):
                        break
                else:
                    paths_to_remove.append(root)

        if not dry_run:
            import shutil
            for path_to_remove in paths_to_remove:
                shutil.rmtree(path_to_remove)

        return paths_to_remove

    def exec(
            self,
            command: str,
            in_: Optional[str] = None,
            exclude: Optional[str] = None,
            git=False,
    ):
        from armarx_setup.core.util import commands

        rules = [
            lambda m: os.path.isdir(m.path),
        ]
        if in_:
            rules.append(lambda m: m.name.startswith(in_))
        if exclude:
            rules.append(lambda m: not m.name.startswith(exclude))
        if git:
            rules.append(lambda m: m.update is not None and m.update.git is not None)

        def exec_in_module(module: "Module"):
            if all([rule(module) for rule in rules]):
                try:
                    commands.run(command, cwd=module.path)
                except error.CommandFailed as e:
                    console.print(e)
        self.for_each_module_in_dependency_chain(exec_in_module)

    def notify(self, title, body, icon):
        map = {
            "success": "axii_status_icon.png",
            "failure": "axii_status_icon_red.png"
        }
        if icon in map:
            icon = map[icon]

        icon_path = os.path.join(PACKAGE_ROOT, "resources", icon)

        try:
            import notify2  # apt install libdbus-glib-1-dev; pip install notify2 dbus-python
            notify2.init("Axii")
            n = notify2.Notification(title, body, icon_path)
            n.set_urgency(notify2.URGENCY_LOW)
            n.show()
        except:
            pass


if __name__ == '__main__':
    from armarx_setup.core.module import Module
    from armarx_setup.core.module_graph import ModuleGraph
    from armarx_setup.core.workspace.config import Config
    from armarx_setup.core.workspace.env import EnvFile

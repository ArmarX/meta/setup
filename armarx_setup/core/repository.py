import os

import dataclasses as dc

from typing import Dict, Optional, Tuple

from armarx_setup.core.util.environ_context import EnvironContext


from armarx_setup import console


@dc.dataclass
class Repository:

    git_url: str = ""
    http_url: str = ""
    private: bool = False
    default_checkout_ref: Optional[str] = None
    gcc_version: Optional[str] = None
    env: Dict[str, str] = dc.field(default_factory=dict)

    root_path: Optional[str] = None
    _cmake_project_name: Optional[str] = None

    @classmethod
    def make_example(cls) -> "Repository":
        return cls(
            git_url="git@gitlab.com:Example/example.git",
            http_url="https://gitlab.com/Example/example",
            private=False,
        )

import networkx as nx

from armarx_setup import console


from typing import Callable, Dict, Tuple


from armarx_setup.core.module_graph.module_graph import ModuleGraph


LayoutFn = Callable[[ModuleGraph], Dict[str, Tuple[float, float]]]


def mutlipartite(graph: ModuleGraph):
    import os

    subsets: Dict[str, int] = dict()
    for n, attr in graph.nodes.items():
        key = os.path.split(attr["module"].name)[0]
        if key not in subsets:
            subsets[key] = len(subsets)
        attr["subset"] = subsets[key]

    return nx.layout.multipartite_layout(graph, subset_key="subset", align="horizontal")


def graphviz(graph: ModuleGraph):
    # Requires apt package "graphviz" and python package "pygraphviz"
    # sudo apt install graphviz
    # For Python >3.7: pip install pygraphviz
    # For Python 3.6: pip install pygraphviz==1.6
    from networkx.drawing.nx_agraph import graphviz_layout

    # Clean node attributes as they contain modules, which cause
    # infinite recursion when trying to get their repr().
    clean_graph = graph.copy()
    for node in clean_graph.nodes:
        clean_graph.nodes.data()[node].clear()

    return graphviz_layout(clean_graph, prog="dot")


def layout_to_nx(ig_graph: "ig.Graph", layout: "ig.Layout",
                 invert_y=False):
    pos = {}
    for node, coords in zip(ig_graph.vs, layout.coords):
        if invert_y:
            coords[1] *= -1
        pos[node["module"].name] = tuple(coords)
    return pos


def igraph_dag(graph: ModuleGraph, **kwargs):
    import igraph as ig
    ig_graph = ig.Graph.from_networkx(graph)

    # For DAGs
    # Suits a DAG, but a lot of overlap of nodes.
    layout = ig_graph.layout_sugiyama(hgap=1, vgap=1, maxiter=int(1e4), **kwargs)
    return layout_to_nx(ig_graph, layout, invert_y=True)


def igraph_tree(graph: ModuleGraph):
    import igraph as ig
    ig_graph = ig.Graph.from_networkx(graph)

    # For (almost) trees
    # Promising. This places nodes on a grid, so there is no overlap between nodes.
    # It is actually meant for trees, so BFS is used to build a tree of non-tree graphs.

    root = []
    for node in ig_graph.vs:
        node: ig.Vertex
        if node.indegree() == 0:
            root.append(node.index)

    layout = ig_graph.layout_reingold_tilford(mode="out", root=root)
    # layout = ig_graph.layout_reingold_tilford_circular()  # bad

    return layout_to_nx(ig_graph, layout, invert_y=True)


def igraph_test(graph: ModuleGraph):
    import igraph as ig
    ig_graph = ig.Graph.from_networkx(graph)

    layout = (
        ig_graph
        # General
        # .layout_davidson_harel()  # bad
        # .layout_drl()  # super bad
        # .layout_fruchterman_reingold()  # bad
        # .layout_grid()  # Well, a grid.
        # .layout_graphopt()  # bad
        # .layout_auto()  # bad
        # .layout_kamada_kawai()  # bad
        # .layout_lgl()  # hmm
        # .layout_mds()  # no

        # No not make sense in the first place.
        # .layout_star()
        # .layout_circle()
    )

    return layout_to_nx(ig_graph, layout, invert_y=True)


def make_layout_fns(weights=None) -> Dict[str, LayoutFn]:
    layouts = dict()

    # NetworkX built-in layouts.
    layouts["multipartite"] = mutlipartite

    layouts["planar"] = lambda graph: nx.planar_layout(graph)
    layouts["shell"] = lambda graph: nx.shell_layout(graph)
    layouts["spectral"] = lambda graph: nx.spectral_layout(graph)
    layouts["spring"] = lambda graph: nx.spring_layout(graph)

    # Requires extra apt packages.
    layouts["graphviz"] = graphviz

    # Requires python-igraph
    layouts["igraph-tree"] = igraph_tree
    layouts["igraph-dag"] = lambda graph: igraph_dag(graph, weights=weights)

    return layouts


def import_error_msg(e: ImportError, feature: str):
    return f"{e}. Install {e.name} to enable {feature}."


def plot(
        graph: ModuleGraph,
        all=False,
        layout=None,
        reduce=False,
        node_colors=True,
        ):
    try:
        import matplotlib.pyplot as plt
        import PyQt5
    except ImportError as e:
        console.print(import_error_msg(e, "graph plotting"))
        return

    fig: plt.Figure
    ax: plt.Axes
    fig, ax = plt.subplots(dpi=200)
    ax.set_axis_off()

    options = dict(
        # Nodes
        node_color="#CCCCCC",
        node_size=400,
        node_shape="o",  # "os",
        linewidths=0,

        font_size=4,
        font_weight="normal",

        # Edges
        width=0.5,
        edge_color="#444444",
    )

    if not all:
        graph = graph.subgraph(filter(lambda n: not n.startswith("apt/"), graph.nodes))
        graph = graph.subgraph(filter(lambda n: not n.startswith("tools/"), graph.nodes))

    if reduce:
        from networkx.algorithms.dag import transitive_reduction
        graph = transitive_reduction(graph)

    edge_weights = None
    edge_weight_by_out_degree = True
    if edge_weight_by_out_degree:
        try:
            import numpy as np
        except ImportError as e:
            console.print(import_error_msg(e, "edge weights"))
        else:
            out_degrees = np.array([graph.out_degree(u) for u, v in graph.edges])
            edge_weights = 1 - (out_degrees / out_degrees.max())
            for (u, v), weight in zip(graph.edges, edge_weights):
                graph[u][v]["weight"] = weight

    if edge_weights is not None:
        try:
            from matplotlib.cm import get_cmap
        except ImportError as e:
            console.print(import_error_msg(e, "edge coloring"))
        else:
            options["edge_color"] = edge_weights
            options["edge_cmap"] = get_cmap("gray_r")
            options["edge_vmin"] = -0.5  # Lower this value to make weak edges weaker.
            options["edge_vmax"] = 1.2   # Increase this value to make strong edges stronger.

    if node_colors:
        from armarx_setup.core.util.prefix_color_map import module_to_color
        options["node_color"] = [
            np.array(module_to_color(str(m), rgb=True)) / 255.
            for m in graph.nodes
        ]

    layouts = make_layout_fns(weights=edge_weights if edge_weights is not None else None)
    if layout is not None:
        if layout not in layouts:
            console.print(f"Graph layout '{layout}' is unknown. Known are:", ", ".join([f"'{l}'" for l in layouts]))
            return
        candidates = [layout]
    else:
        candidates = [
            "graphviz",
            # "igraph-dag",
            "igraph-tree",
            "multipartite",  # This should always work.
        ]

    prefix = "\t"
    pos = None
    for candidate in candidates:
        layout_fn = layouts[candidate]
        try:
            pos = layout_fn(graph)
        except ImportError as e:
            msg = f"{prefix}Layout '{candidate}': {e}."
            if e.name == "igraph":
                msg += " Install the python package 'python-igraph' to enable igraph layouts."
            console.print(msg)
        except Exception as e:
            console.print(f"{prefix}Layout '{candidate}': error: {e}.")
        else:
            console.print(f"{prefix}Using '{candidate}' layout.")
            break

    if pos is None:
        return

    assert pos is not None

    wrap_width = 10
    labels = {}
    for n in graph.nodes:
        label: str = n
        label = label.replace("/", "/\n")
        if wrap_width is not None:
            import textwrap
            lines = label.split("\n")
            lines = ["\n".join(textwrap.wrap(line, width=wrap_width)) for line in lines]
            label = "\n".join(lines)
        labels[n] = label

    nx.draw_networkx(graph, ax=ax, pos=pos, with_labels=True, labels=labels, **options)

    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    def test():
        import os
        from armarx_setup.core.workspace import Workspace
        ws = Workspace.load_workspace_by_path(os.path.expanduser("~/code"))
        graph = ws.module_graph
    test()

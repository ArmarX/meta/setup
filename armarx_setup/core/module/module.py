import os
from collections import namedtuple
import dataclasses as dc
import typing as ty

from typing import Dict, List, Optional, Iterable

import armarx_setup as axs
from armarx_setup.core import error
from armarx_setup.core.util import cli, dataclass, json

from armarx_setup.core.module.general import General
from armarx_setup.core.module.context import Context, Hook

from armarx_setup.core.module.system import System
from armarx_setup.core.module.update import Update
from armarx_setup.core.module.prepare import Prepare
from armarx_setup.core.module.build import Build
from armarx_setup.core.module.install import Install
from armarx_setup.core.module.conclude import Conclude

from armarx_setup import console


MODULE_DATA_DIRS_ENV_VAR_NAME = "ARMARX_MODULES"

MODULE_DATA_DIRS = []
MODULE_DATA_DIRS += list(filter(bool, os.environ.get(MODULE_DATA_DIRS_ENV_VAR_NAME, "").split(":")))
# Append this last so other databases can override built-in modules.
MODULE_DATA_DIRS += [os.path.join(axs.PACKAGE_ROOT, "data", "modules")]


@dc.dataclass
class Dependency:

    feature_name: Optional[str] = None
    features: List[str] = dc.field(default_factory=list)


@dc.dataclass
class Module:

    name: str = ""

    general: Optional[General] = None

    system: Optional[System] = None
    update: Optional[Update] = None
    prepare: Optional[Prepare] = None
    build: Optional[Build] = None
    install: Optional[Install] = None
    conclude: Optional[Conclude] = None

    required_modules: Dict[str, Dependency] = dc.field(default_factory=dict)
    features: Dict[str, str] = dc.field(default_factory=dict)

    # If set, this overrides the automatically generated path.
    path: Optional[str] = None

    # Init-only variables.

    ws_path: dc.InitVar[Optional[str]] = ""
    ws_config: dc.InitVar[Optional] = None

    GLOBAL_MODULE_NAME: ty.ClassVar[str] = "global module"

    def __post_init__(
            self,
            ws_path: Optional[str],
            ws_config: Optional["Config"],
            ):

        self.path = (os.path.join(ws_path, self.name) if self.path is None
                     else os.path.expandvars(self.path))
        context = Context(module_name=self.name, path=self.path, ws_path=ws_path, ws_config=ws_config)

        kwargs = dict(ctx=context)

        self.general = dataclass.sanitize_type(self.general, General, **kwargs)
        self.system = dataclass.sanitize_type(self.system, System, **kwargs)
        self.update = dataclass.sanitize_type(self.update, Update, **kwargs)
        self.prepare = dataclass.sanitize_type(self.prepare, Prepare, **kwargs)
        self.build = dataclass.sanitize_type(self.build, Build, **kwargs)
        self.install = dataclass.sanitize_type(self.install, Install, **kwargs)
        self.conclude = dataclass.sanitize_type(self.conclude, Conclude, **kwargs)

        required_modules = {}
        for name, module in self.required_modules.items():
            required_module = dataclass.sanitize_type(module, Dependency)
            required_modules[name] = required_module
        self.required_modules = required_modules

    @classmethod
    def load_info(
            cls,
            name: str,
            ws: Optional["Workspace"] = None,
            extension: Optional[Dict] = None
            ) -> "Module":

        data, file_path = cls._load_module_data(name)
        if extension is not None:
            data = json.extend(data, extension)

        if ws is not None:
            data["ws_path"] = ws.path
            data["ws_config"] = ws.config

        try:
            return cls(**data, name=name)
        except TypeError as e:
            from_file_path = f" from '{file_path}'" if file_path is not None else ""
            console.print(f"[red]Failed to load module '{name}'{from_file_path}:\n {e}[/]")
            raise

    @classmethod
    def _load_module_data(cls, name: str) -> ty.Tuple[ty.Dict, ty.Optional[str]]:
        try:
            file_path = cls.get_info_path(name=name)

        except error.ModuleInfoNotFound:
            if name.startswith("apt/"):
                # Emulate a system/packages/apt module.
                apt_package_name = name[len("apt/"):]
                return cls.emulate_system_packages_apt_module([apt_package_name]), None
            else:
                raise  # Module not found.
        else:
            assert os.path.isfile(file_path), file_path
            try:
                data = json.load(file_path)
            except json.JSONDecodeError as e:
                raise IOError(f"Failed to load description of '{name}' from '{file_path}': \n{e}")
            else:
                return data, file_path

    @classmethod
    def get_info_path(cls, name: str) -> str:
        for module_data_dir in MODULE_DATA_DIRS:
            path = os.path.join(module_data_dir, f"{name}.json")
            if os.path.isfile(path):
                return path

        module_names = cls.get_all_module_names()
        database_module = cls.get_database_module(name, module_names)
        similar_modules = cls.get_similar_module_names(name, module_names)

        raise error.ModuleInfoNotFound(name=name, tried_paths=MODULE_DATA_DIRS, similar_modules=similar_modules,
                                       database_module=database_module)

    @classmethod
    def emulate_system_packages_apt_module(cls, apt_packages: List[str]):
        return {
            "system": {
                "packages": {
                    "apt": apt_packages
                }
            }
        }

    @classmethod
    def get_all_module_names(
            cls,
            ) -> List[str]:
        import glob

        names = []
        for directory in MODULE_DATA_DIRS:
            paths = glob.glob(os.path.join(directory, "**", "*.json"), recursive=True)
            prefix = directory if directory.endswith("/") else directory + "/"

            for path in paths:
                path: str
                assert path.startswith(prefix), f"path: '{path}', prefix: '{prefix}'"
                path = path[len(prefix):]

                name = os.path.splitext(path)[0]
                names.append(name)

        return names

    @classmethod
    def get_database_module(cls, requested_module_name: str, module_names):
        for module_name in module_names:
            m = cls.load_info(module_name)
            try:
                namespace_authority = m.general.database.for_namespace
                if not namespace_authority.endswith("/"):
                    namespace_authority += "/"
            except:
                pass
            else:
                if requested_module_name.startswith(namespace_authority):
                    ReturnNamedTuple = namedtuple('ReturnNamedTuple', 'database_module_name namespace')
                    return ReturnNamedTuple(module_name, namespace_authority)
        return None

    @classmethod
    def get_similar_module_names(
            cls,
            name: str,
            other_names: Iterable[str],
            max_count=5,
            min_ratio=50) -> List[str]:
        try:
            from thefuzz import fuzz
        except ImportError:
            console.print("Install python package 'thefuzz' to enable suggestion of similar module names.")
            return []
        else:
            distances = {other: fuzz.ratio(name.lower(), other.lower()) for other in other_names}
            sorted_names = sorted(distances.keys(), key=lambda m: -distances[m])
            candidates = [m for m in sorted_names[:max_count] if distances[m] > min_ratio]
            return candidates

    def get_phases(self) -> List[Hook]:
        return [self.system, self.update, self.prepare, self.build, self.install]

    def is_system_module(self) -> bool:
        phases = [self.update, self.prepare, self.build, self.install]
        return self.system is not None and all(phase is None for phase in phases)

    @staticmethod
    def say_num_packages_are_installed(num):
        s_packages_are = "package is" if num == 1 else "packages are"
        console.print(f"{num} apt {s_packages_are} already installed.")

    def clean(self):
        if self.build is not None:
            console.print(f"[b]Clean module '{self.name}' ...[/]")
            self.build.clean()

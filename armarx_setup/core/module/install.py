import os
import dataclasses as dc

from typing import Dict, List, Optional

from armarx_setup.core import error
from armarx_setup.core.module import common
from armarx_setup.core.module.context import Hook
from armarx_setup.core.util import commands, environ_context as env_ctx
from armarx_setup.core.module.common import Run


@dc.dataclass
class CMake(Hook):
    
    dest_dir: Optional[str] = None

    def generate_requirements(self) -> List[str]:
        return common.CMAKE_DEPENDENCIES

    def __call__(self, *args, **kwargs):
        build_dir = self.ctx.build_dir
        if not os.path.isdir(build_dir):
            raise IOError(f"Build directory '{build_dir}' does not exist.")

        with env_ctx.EnvironContext(self.ctx.env, verbose=self.ctx.verbose):
            
            vars_expanded = ""
            if self.dest_dir is not None:
                vars_expanded = f"DESTDIR={self.dest_dir}"
                
            commands.run(f"{vars_expanded} cmake --install . ", cwd=build_dir)


class Env(Hook):

    def __init__(self, ctx=None, **kwargs):
        super().__init__(ctx=ctx)

        self.env: Dict[str, str] = kwargs
        for k in self.env:
            v = self.env[k]

            if not (isinstance(k, str) and isinstance(v, str)):
                raise error.ArmarXSetupError(f"Invalid env entry: '{k}': '{v}'")

            self.env[k] = v

    def generate_env(self, env: "EnvFile"):
        for k, v in self.env.items():
            if self.ctx.path:
                env_vars = self.ctx.get_environment()
                for key, val in env_vars.items():
                    v = v.replace(f"${key}", val)
            env.add(k, v, by=self.ctx.name)

    def __repr__(self):
        return ("Env({"
                + ", ".join(f"'{k}': '{v}'" for k, v in self.env.items())
                + "})")


class Executables(Hook):

    def __init__(self, ctx=None, **kwargs):
        super().__init__(ctx=ctx)

        self.execs = kwargs

    def __call__(self, *args, **kwargs):
        def resolve_vars(input):
            output = os.path.expandvars(input.replace('\\$', '--$--')).replace('--$--', "$")
            return output

        local_axii_bin = os.path.join(self.ctx.ws_path, ".axii", "bin")

        for exec_name, exec_config in self.execs.items():
            exec_path = os.path.join(local_axii_bin, exec_name)
            kill_trap = exec_config["kill_trap"] if "kill_trap" in exec_config else False
            fork = "&" if kill_trap else ""
            with env_ctx.EnvironContext(self.ctx.get_environment(), verbose=self.ctx.verbose):
                with open(exec_path, 'w') as f:
                    f.write(f"#!/bin/bash\n")
                    f.write(f"\n")
                    if kill_trap:
                        f.write(f"trap : SIGTERM SIGINT\n")
                        f.write(f"\n")
                    if "pre_exec_commands" in exec_config and len(exec_config["pre_exec_commands"]) > 0:
                        for cmd in exec_config["pre_exec_commands"]:
                            f.write(f"{resolve_vars(cmd)}\n")
                        f.write(f"\n")
                    args = ""
                    if "exec_args" in exec_config:
                        args = " ".join(resolve_vars(x) for x in exec_config['exec_args'])
                    f.write(f"{resolve_vars(exec_config['exec'])} {args} $@ {fork}\n")
                    f.write(f"\n")
                    if kill_trap:
                        f.write(f"pid=$!\n")
                        f.write(f"\n")
                        f.write(f"wait $pid\n")
                        f.write(f"\n")
                        f.write(f"if [[ $? -gt 128 ]]; then\n")
                        f.write(f"    kill -9 $pid\n")
                        f.write(f"fi\n")
                        f.write(f"\n")
                    if "post_exec_commands" in exec_config and len(exec_config["post_exec_commands"]) > 0:
                        for cmd in exec_config["post_exec_commands"]:
                            f.write(f"{resolve_vars(cmd)}\n")
                        f.write(f"\n")

            os.chmod(exec_path, 0o770)


@dc.dataclass
class Install(Hook):

    cmake: Optional[CMake] = None
    env: Optional[Env] = None
    source: Optional[common.Source] = None
    run: Optional[Run] = None
    executables: Optional[Executables] = None

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        self.cmake = sanitize_type(self.cmake, CMake, ctx=self.ctx)
        self.env = sanitize_type(self.env, Env, ctx=self.ctx)
        self.source = sanitize_type(self.source, common.Source, ctx=self.ctx)
        self.run = sanitize_type(self.run, common.Run, ctx=self.ctx)
        self.executables = sanitize_type(self.executables, Executables, ctx=self.ctx)

    def _get_sub_hooks(self) -> List["Hook"]:
        return [self.cmake, self.run, self.env, self.source, self.executables]


if __name__ == '__main__':
    from armarx_setup.core.workspace.env import EnvFile

import os

import dataclasses as dc

from typing import Optional, List

from armarx_setup import console
from armarx_setup.core import error
from armarx_setup.core.util import commands, prefix_color_map, environ_context
from armarx_setup.core.module.context import Hook, Context


CMAKE_DEPENDENCIES = ["tools/cmake/3.21"]


def resolve_path(path: str, ctx: Context) -> str:
    with environ_context.EnvironContext(ctx.get_environment()):
        path = os.path.expanduser(os.path.expandvars(path))

    if os.path.isabs(path):
        absolute_path = path
    else:
        info_dir = os.path.dirname(ctx.module_info_path)
        absolute_path = os.path.join(info_dir, path)

    return absolute_path


@dc.dataclass
class Run(Hook):

    command: Optional[str] = None
    script: Optional[str] = None

    def __call__(self, *args, **kwargs):
        os.makedirs(self.ctx.build_dir, exist_ok=True)
        cwd = self.ctx.build_dir

        with environ_context.EnvironContext(self.ctx.get_environment(), verbose=-1) as env_ctx:
            if self.command:
                self.run_command(cwd=cwd, env_ctx=env_ctx)

            elif self.script:
                self.run_script(cwd=cwd, env_ctx=env_ctx)

            else:
                raise TypeError("No run option configured.")


    def run_command(self, cwd: str, **kwargs):
        assert os.path.isdir(cwd)

        module_text = prefix_color_map.module_to_colored_text(self.ctx.name)
        console.print(f"Run command '{self.command}' requested by module {module_text} in '{cwd}'.")
        commands.run(self.command, cwd=cwd, **kwargs)


    def run_script(self, cwd: str, **kwargs):
        assert os.path.isdir(cwd)

        module_text = prefix_color_map.module_to_colored_text(self.ctx.module_name)

        path = self.script
        absolute_path = resolve_path(path, self.ctx)

        if os.path.isfile(absolute_path):
            console.print(f"Run script '{self.script}' requested by module {module_text} in '{cwd}'.")
            commands.run(absolute_path, cwd=cwd, **kwargs)

        else:
            raise IOError(f"The module {module_text} requests running the script '{self.script}', "
                          f"but it does not exist at: '{absolute_path}'")


class Source(Hook):

    def __init__(self, paths: List[str], ctx=None):
        super().__init__(ctx=ctx)

        self.paths: List[str] = paths
        for path in self.paths:
            if not isinstance(path, str):
                raise error.ArmarXSetupError(f"Invalid path entry: '{path}'")

    def generate_env(self, env: "EnvFile"):
        by = "global variables"
        if self.ctx.name:
            by = self.ctx.name
        for path in self.paths:
            path = resolve_path(path, ctx=self.ctx)
            env.add_source(path=path, by=by, write=False)


if __name__ == '__main__':
    from armarx_setup.core.workspace.env import EnvFile

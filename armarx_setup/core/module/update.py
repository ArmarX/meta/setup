import os
import requests

import dataclasses as dc

from collections import OrderedDict
from typing import List, Optional

from armarx_setup.core import error
from armarx_setup.core.util import commands, environ_context
from armarx_setup.core.module.context import Hook

from armarx_setup import console


git_non_interaction_env_vars = {
    "GIT_SSH_COMMAND": "ssh -oBatchMode=yes",  # Disables interaction in ssh.
    "GIT_TERMINAL_PROMPT": "0",  # Disables interaction in Git, such as HTTPS credentials.
}


@dc.dataclass
class Git(Hook):

    ssh_url: Optional[str] = None
    https_url: Optional[str] = None
    default_checkout_ref: Optional[str] = None
    default_checkout_commit: Optional[str] = None
    submodules: bool = False
    lfs: Optional[bool] = None
    ignore_patterns: Optional[List[str]] = None

    def __call__(self, **kwargs):
        if os.path.isdir(os.path.join(self.ctx.path, ".git")):
            result = self.pull(**kwargs)
        else:
            result = self.clone(**kwargs)

        self.update_ignore_patterns()
        return result

    def clone(self, **kwargs):
        r = None
        exception = None
        for url in [url for url in [self.ssh_url, self.https_url] if url is not None]:
            args = [f"{url} {self.ctx.path}"]
            if self.default_checkout_ref:
                args.append(f" --branch {self.default_checkout_ref}")
            if self.submodules:
                args.append("--recursive")
            command = f"git clone " + " ".join(args)
            
            try:
                with environ_context.EnvironContext(git_non_interaction_env_vars):
                    r = commands.run(command, **kwargs)
            except error.CommandFailed as e:
                # Try the next url.
                exception = e
            else:
                break

        if exception is not None:
            raise exception
        
        if self.default_checkout_commit:
            command = f"git checkout {self.default_checkout_commit}"
            commands.run(command, cwd=self.ctx.path, **kwargs)

        if 'Cloning into' in r:
            summary = 'Cloned repository.'
        else:
            summary = None
        return summary, r

    def pull(self, **kwargs):
        import git

        detached_head_state: Optional[str] = self.get_detached_head_state()

        if detached_head_state is None:
            repo = git.Repo(self.ctx.path)
            active_branch_name: str = repo.active_branch.name
            active_branch = f"'[cyan b]{active_branch_name}[/]'"

            try:
                with environ_context.EnvironContext(git_non_interaction_env_vars, verbose=-1):
                    out = commands.run("git pull --prune --no-edit", cwd=self.ctx.path, **kwargs)

                    if self.submodules:
                        out += "\n" + commands.run("git submodule update --recursive", cwd=self.ctx.path, **kwargs)
                if "Already up-to-date" in out or "Already up to date" in out:
                    summary = f"Up to date on branch {active_branch}."
                elif "Updating" in out:
                    summary = f"Updated repository on branch {active_branch}."
                else:
                    summary = None
                return summary, out

            except error.CommandFailed as e:
                out = e.stderr

                if "You are not currently on a branch" in out:
                    e.summary = ("update.Git.get_detached_head_state() was None but still 'git pull' resulted "
                                 "in 'not on a branch' state. This should not have happened.")
                elif "Your local changes to the following files would be overwritten by merge" in out:
                    e.summary = f"Local changes would be overwritten (on branch {active_branch})."

                raise e
        else:
            return f"Nothing to do.", f"Skipped update because repository is on {detached_head_state}."

    def get_detached_head_state(self) -> Optional[str]:
        kwargs = {
            "quiet": True,
            "cwd": self.ctx.path,
            "capture_output": True
        }

        try:
            head_hash = commands.run("git rev-parse HEAD", **kwargs)
            head_hash_short = commands.run("git rev-parse --short HEAD", **kwargs)

            # Check if repo is on a user-specified tag (no error).
            try:
                tag = commands.run(f"git name-rev --name-only --tags HEAD", **kwargs)
                if tag == self.default_checkout_ref:
                    current_ref_hash = commands.run(f"git rev-list -n 1 {self.default_checkout_ref}", **kwargs)
                    if current_ref_hash == head_hash:
                        return f"tag '{self.default_checkout_ref}'"
            except error.CommandFailed:
                pass

            # Check if repo is on a user-specified commit.
            res = commands.run("git rev-parse --abbrev-ref HEAD", **kwargs)
            if res == "HEAD":
                return f"commit '{head_hash_short}'"

        except error.CommandFailed:  # as e:
            # print("command failed", e)
            pass

    def update_ignore_patterns(self):
        ignore_filename = ".git/info/exclude"
        ignore_filepath = os.path.join(self.ctx.path, ignore_filename)

        if not os.path.isfile(ignore_filepath):
            return

        # Read current file.
        with open(ignore_filepath, "r") as file:
            lines = file.readlines()
        lines: List[str] = [line.rstrip("\n") for line in lines]

        # Apply required changes.
        changed = False
        if self.ignore_patterns is not None:
            for pattern in self.ignore_patterns:
                if pattern not in lines:
                    lines.append(pattern)
                    changed = True

        # Write new file if necessary.
        if changed:
            lines = [line + "\n" for line in lines]
            with open(ignore_filepath, "w") as file:
                file.writelines(lines)


@dc.dataclass
class Archive(Hook):

    @dc.dataclass
    class Extract:
        type: Optional[str] = None
        """Directory created by extraction."""
        created_directory: Optional[str] = None
        extract_into: Optional[str] = None

    url: Optional[str] = None
    extract: Optional[Extract] = None
    make_executable: bool = False

    def __post_init__(self):
        if isinstance(self.extract, bool):
            self.extract = self.Extract()
        else:
            from armarx_setup.core.util import dataclass
            self.extract = dataclass.sanitize_type(self.extract, self.Extract)

        try:
            with environ_context.EnvironContext(self.ctx.get_environment()):
                self.extract.extract_into = os.path.expandvars(self.extract.extract_into)
        except:
            pass

    @property
    def archive_filename(self) -> str:
        return os.path.basename(self.url)

    @property
    def archive_filepath(self) -> str:
        return os.path.join(self.ctx.path, self.archive_filename)

    def __call__(self, *args, **kwargs):
        os.makedirs(self.ctx.path, exist_ok=True)

        summary = None
        actions = []

        refresh_extract = False
        if not os.path.isfile(self.archive_filepath):
            self.download()
            actions.append("downloaded")
            refresh_extract = True

        if self.extract is not None:
            try:
                done = self.do_extract(refresh=refresh_extract)
            except error.ArmarXSetupError as e:
                summary = str(e)
            else:
                if done:
                    actions.append("extracted")

        if self.make_executable and not os.access(self.archive_filepath, os.X_OK):
            os.chmod(self.archive_filepath, 0o750)
            actions.append("made executable")

        if summary is None:
            if actions:
                summary = f"{(' and '.join(actions)).capitalize()} '{self.archive_filename}'."
            else:
                summary = "Nothing to do."

        assert summary is not None
        return summary, summary

    def download(self):
        dl_file = requests.get(self.url)
        with open(self.archive_filepath, 'wb') as file:
            file.write(dl_file.content)
        assert os.path.isfile(self.archive_filepath)

    def do_extract(self, refresh: bool):
        extractors = {
            ".tar.gz": self.extract_tar,
            ".zip": self.extract_zip,
            ".deb": self.extract_deb
        }

        for ext, fn in extractors.items():
            if self.matches_extension(ext):
                if self.extract.extract_into is not None:
                    target_dir = self.extract.extract_into
                elif self.extract.created_directory is not None:
                    target_dir = os.path.join(os.path.dirname(self.archive_filepath),
                                              self.extract.created_directory)
                else:
                    target_dir = self.archive_filepath[:-len(ext)]
                break
        else:
            # Is run when the loop does not break.
            raise error.ArmarXSetupError(
                f"Unknown archive type: '{os.path.splitext(self.archive_filename)}'.\n"
                f"Known are: " + ", ".join(extractors))

        assert None not in [fn, target_dir], [fn, target_dir]

        if refresh or not os.path.isdir(target_dir):
            fn()
            assert os.path.isdir(target_dir), f"{target_dir} does not exist or is not a directory"
            return True
        else:
            return False

    def extract_tar(self):
        import tarfile
        with tarfile.open(self.archive_filepath) as file:
            file.extractall(self.ctx.path)

    def extract_zip(self):
        import zipfile
        with zipfile.ZipFile(self.archive_filepath) as file:
            file.extractall(self.ctx.path)

    def extract_deb(self):
        try:
            path = self.extract.extract_into
        except:
            path = None
        if path is None:
            path = os.path.join(self.ctx.path, self.archive_filepath[:-len('.deb')])
        commands.run(f"dpkg -x {self.archive_filepath} {path}", quiet=True, capture_output=True)

    def matches_extension(self, extension: str) -> bool:
        if self.extract.type is not None and extension.endswith(self.extract.type):
            return True

        elif self.archive_filename.endswith(extension):
            return True

        else:
            return False


@dc.dataclass
class Archives(Hook):

    archives: List[Archive]

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        if self.archives is None:
            self.archives = []
        else:
            self.archives = [sanitize_type(a, Archive, ctx=self.ctx) for a in self.archives]

    def __call__(self, *args, **kwargs):
        summaries = []
        results = []
        for archive in self.archives:
            summary, result = archive()
            if summary != "Nothing to do.":
                summaries.append(summary)
            results.append(result)

        # Eliminate duplicates while preserving order.
        summaries = OrderedDict.fromkeys(summaries)
        if len(summaries) > 0:
            maybe_s = "s" if len(summaries) > 1 else ""
            summary = f"Downloaded and handled {len(summaries)} archive{maybe_s}."
        else:
            summary = "Nothing to do."
        result = "\n".join([f"[i]Archive '{a.archive_filename}':[/] \n{r}"
                            for a, r in zip(self.archives, results)])
        return summary, result


@dc.dataclass
class Update(Hook):

    git: Optional[Git] = None
    archive: Optional[Archive] = None
    archives: Optional[Archives] = None

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        self.git = sanitize_type(self.git, Git, ctx=self.ctx)
        self.archive = sanitize_type(self.archive, Archive, ctx=self.ctx)
        self.archives = sanitize_type(dict(archives=self.archives), Archives, ctx=self.ctx)

    def _get_sub_hooks(self) -> List["Hook"]:
        return [self.git, self.archive, self.archives]

    def _is_exclusive(self) -> bool:
        return True

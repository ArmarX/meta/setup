import os

import dataclasses as dc

from typing import Dict, List, Optional

from armarx_setup.core.util import prefix_color_map


class Context:

    def __init__(
            self,
            module_name: str,
            ws_config,
            ws_path: str,
            path: Optional[str],
            env: Dict[str, str] = None,
            verbose: int = 0,
            build_dir: Optional[str] = None
    ):
        from armarx_setup.core.workspace.config import Config

        self.module_name = module_name
        self.ws_config: Config = ws_config
        self.ws_path = ws_path

        self.path = path
        self.env = env or dict()
        self.verbose = verbose
        self.build_dir = build_dir

    @property
    def name(self) -> str:
        return self.module_name

    @property
    def name_colored(self) -> str:
        return prefix_color_map.module_to_colored_text(self.module_name)

    @property
    def build_dir(self) -> str:
        return self._build_dir or os.path.join(self.path, "build")

    @build_dir.setter
    def build_dir(self, value: str):
        self._build_dir = value

    @property
    def module_info_path(self) -> str:
        from armarx_setup.core.module.module import Module
        return Module.get_info_path(self.module_name)

    def get_environment(self):
        from armarx_setup.core.module import Module

        env = {
            # The module's location inside the workspace.
            "MODULE_PATH": self.path,
        }

        if self.name != Module.GLOBAL_MODULE_NAME:
            module_info_path = self.module_info_path

            # The directory containing the module's .json config file is.
            env["MODULE_CONFIG_DIR"] = os.path.split(module_info_path)[0]
            # The directory which is expected to contain additional resources,
            # e.g. scripts, next to the .json config file.
            env["MODULE_CONFIG_RESOURCE_DIR"] = os.path.splitext(module_info_path)[0]

        return env

    def __repr__(self):
        return f"Context(...)"


@dc.dataclass
class Hook:

    ctx: Context

    def generate_requirements(self) -> List[str]:
        return sum([hook.generate_requirements() for hook in self._get_sub_hooks()
                    if hook is not None], [])

    def generate_env(self, env: "EnvFile"):
        for hook in self._get_sub_hooks():
            if hook:
                hook.generate_env(env)

    def __call__(self, *args, **kwargs):
        for hook in self._get_sub_hooks():
            if hook is not None:
                r = hook(*args, **kwargs)
                if self._is_exclusive():
                    return r

    def _get_sub_hooks(self) -> List["Hook"]:
        return []

    def _is_exclusive(self) -> bool:
        """If this returns true, only the first non-None sub-hook is called."""
        return False


if __name__ == '__main__':
    from armarx_setup.core.workspace.env import EnvFile

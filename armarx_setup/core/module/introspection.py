from typing import List
from rich.tree import Tree

from armarx_setup.core.module import Module


def _add_to_tree_list(parent: Tree, names: List[str], full_names: List[str]):
    current_group = None
    tree = None
    pending = []

    def add_pending():
        nonlocal pending, tree
        if pending:
            _add_to_tree_list(tree, *zip(*pending))
            pending = []

    for name, full in zip(names, full_names):
        if "/" in name:
            split = name.split("/")
            group = split[0]
            sub_name = "/".join(split[1:])

            if current_group != group:
                add_pending()

                tree = parent.add(f"[b]{group}/[/]")
                current_group = group

            pending.append((sub_name, full))

        else:
            add_pending()
            # Leaf
            leaf = f"{name}: [i #777777]{full}[/]"
            # leaf = f"[i]{full}[/]"
            parent.add(leaf)
            tree = None

    if pending:
        add_pending()
        pending = []


def get_module_tree_list(
        module_names: List[str],
        label=None,
        ) -> Tree:
    names = sorted(module_names)
    if label is None:
        label = "Modules:"

    root = Tree(f"[b]{label}[/]")
    _add_to_tree_list(parent=root, names=names, full_names=names)

    return root

import os

import dataclasses as dc

from typing import Dict, List, Optional

from armarx_setup.core.util import commands, environ_context as env_ctx
from armarx_setup.core.module import common
from armarx_setup.core.module.context import Hook


def clear_directory(path: str, hidden=True, exceptions=None):
    import glob, shutil
    assert os.path.isdir(path)

    if exceptions is None:
        exceptions: List[str] = []

    entries = glob.glob(os.path.join(path, "*"))
    if hidden:
        entries += glob.glob(os.path.join(path, ".*"))

    for exception in exceptions:
        exception_path = os.path.join(path, exception)
        if exception_path in entries:
            entries.remove(exception_path)

    for entry in entries:
        if os.path.isfile(entry) or os.path.islink(entry):
            os.unlink(entry)
        elif os.path.isdir(entry):
            shutil.rmtree(entry)


@dc.dataclass
class CMake(Hook):

    env: Dict[str, str] = dc.field(default_factory=dict)

    def generate_requirements(self) -> List[str]:
        return common.CMAKE_DEPENDENCIES

    def __call__(self, *args, **kwargs):
        build_dir = self.ctx.build_dir
        if not os.path.isdir(build_dir):
            raise IOError(f"Build directory '{build_dir}' does not exist.")

        try:
            env = self.ctx.ws_config.global_.build.cmake.env.copy()
        except AttributeError:
            env = {}
        env = {**env, **self.ctx.env, **self.env}

        with env_ctx.EnvironContext(env, verbose=self.ctx.verbose):
            commands.run("nice ionice -c 2 -n 7 cmake --build . --target all", cwd=build_dir)

    def clean(self):
        build_dir = self.ctx.build_dir
        if os.path.isdir(build_dir):
            clear_directory(build_dir, exceptions=[".gitkeep", ".gitignore"])


@dc.dataclass
class Run(common.Run):

    def clean(self):
        pass


@dc.dataclass
class Build(Hook):

    cmake: Optional[CMake] = None
    run: Optional[Run] = None

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        self.cmake = sanitize_type(self.cmake, CMake, ctx=self.ctx)
        self.run = sanitize_type(self.run, Run, ctx=self.ctx)

    def _get_sub_hooks(self) -> List[Hook]:
        return [self.cmake, self.run]

    def _is_exclusive(self) -> bool:
        return True

    def clean(self):
        for option in self._get_sub_hooks():
            if option is not None:
                option.clean()

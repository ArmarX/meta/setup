import os

import dataclasses as dc
import typing as ty

from armarx_setup.core import error
from armarx_setup.core.module.context import Hook
from armarx_setup.core.util import commands



class Makefile:

    TEMPLATE = """
PACKAGE_DIR ?= {package_dir}
MANIFEST_FILE ?= $(PACKAGE_DIR)/{manifest_file}

SYSTEM_PYTHON ?= /usr/bin/python3

VENV_DIR ?= {venv_dir}
VENV_ACTIVATE ?= $(VENV_DIR)/bin/activate
VENV_PYTHON_BIN ?= $(VENV_DIR)/bin/python3
VENV_PIP_BIN ?= $(VENV_DIR)/bin/pip3


all: $(VENV_ACTIVATE)


$(MANIFEST_FILE):


$(VENV_ACTIVATE): $(MANIFEST_FILE)
    echo "Create virtual environment in $(VENV_DIR) ..."
    "$(SYSTEM_PYTHON)" -m venv "$(VENV_DIR)"
    echo "Upgrade pip ..."
    "$(VENV_PIP_BIN)" install --upgrade pip
    echo "Install package dependencies ..."
    "$(VENV_PIP_BIN)" install -e "$(PACKAGE_DIR)"
    echo "Install additional packages in editable mode ..."
    {install_editable_lines}
""".replace("    ", "\t").strip() + "\n"

    INSTALL_EDITABLE_TEMPLATE = '@"$(VENV_PIP_BIN)" install -e "{path}"'

    @classmethod
    def fill_template(
            cls,
            package_dir: str,
            manifest_file: str,
            venv_dir: str,
            install_editable: ty.List[str],
    ) -> str:
        install_editable_lines = [cls.INSTALL_EDITABLE_TEMPLATE.format(
            path=os.path.expandvars(os.path.expanduser(path))
        ) for path in install_editable]

        return cls.TEMPLATE.format(
            package_dir=package_dir,
            manifest_file=manifest_file,
            venv_dir=venv_dir,
            install_editable_lines="\n\t".join(install_editable_lines),
        )


@dc.dataclass
class PythonPackage(Hook):

    relative_path: str

    venv_name: str = ".venv"
    manifest_file: str = None
    install_editable: ty.List[str] = dc.field(default_factory=list)

    def __call__(self, *args, verbose=0, **kwargs):
        """
        We create a Makefile to create the python virtual environment.
        It is set up to only run pip if the environment needs to be updated.
        """
        from armarx_setup import console

        console.print(f"\nPrepare python package '{self.relative_path}' in module '{self.ctx.name_colored}' ...")

        python_package_path = os.path.join(self.ctx.path, self.relative_path)
        venv_dir = os.path.join(python_package_path, self.venv_name)
        manifest_file = self.manifest_file or self.find_manifest_file(python_package_path)

        # Create the venv directory, if it does not exist.
        os.makedirs(venv_dir, exist_ok=True)

        # Create the Makefile if it does not exist.
        # We put it _into_ the venv, so it is ignored automatically.
        makefile_filename = "Makefile"
        makefile_path = os.path.join(venv_dir, makefile_filename)

        if not os.path.exists(makefile_path):
            # Dirs are relative to the venv.
            makefile = Makefile.fill_template(
                package_dir="..",
                manifest_file=manifest_file,
                venv_dir=".",
                install_editable=self.install_editable,
            )
            with open(makefile_path, "w") as file:
                file.write(makefile)

        # Call makefile.
        try:
            commands.run("make", cwd=os.path.dirname(makefile_path))
        except error.CommandFailed:
            # Ignore this error and do not print it (it is in the log anyway).
            pass


    @staticmethod
    def system_python_exec() -> str:
        return "/usr/bin/python3"

    def test(self, **kwargs):
        pass

    @classmethod
    def find_manifest_file(cls, python_package_path: str) -> ty.Optional[str]:
        for manifest_filename in ["pyproject.toml", "requirements.txt"]:
            if os.path.isfile(os.path.join(python_package_path, manifest_filename)):
                return manifest_filename



@dc.dataclass
class Python(Hook):

    packages: ty.Dict[str, PythonPackage] = dc.field(default_factory=dict)

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        self.packages = {path: sanitize_type(dict(relative_path=path, **data), PythonPackage, ctx=self.ctx)
                         for path, data in self.packages.items()}

    def generate_requirements(self) -> ty.List[str]:
        return [
            "apt/python3-venv",
        ]

    def __call__(self, *args, verbose=0, **kwargs):
        for package in self.packages.values():
            package()

    def test(self, **kwargs):
        pass

import os

import dataclasses as dc

from typing import Dict, List, Optional, Tuple

from armarx_setup.core import error
from armarx_setup.core.module import common
from armarx_setup.core.module.context import Hook
from armarx_setup.core.util import commands, environ_context as env_ctx

from armarx_setup.core.module.prepare import python as py



@dc.dataclass
class CMake(Hook):
    project_name: Optional[str] = None
    project_dir: Optional[str] = None

    definitions: Optional[Dict[str, str]] = dc.field(default_factory=dict)
    generator: Optional[Dict] = dc.field(default_factory=dict)

    def __post_init__(self):
        if self.project_name is None and self.ctx is not None:
            self.project_name = self.ctx.name.split("/")[-1].replace('-', '_')
        if self.project_dir is not None:
            self.ctx.build_dir = os.path.join(self.ctx.path, self.project_dir, "build")

    def generate_requirements(self) -> List[str]:
        return common.CMAKE_DEPENDENCIES

    def generate_env(self, env: "EnvFile"):
        if self.project_name:
            var, val = self.get_cmake_package_dir_var()
            env.add(var, val, by=self.ctx.module_name)

    def __call__(self, *args, verbose=0, **kwargs):
        build_dir = self.ctx.build_dir
        os.makedirs(build_dir, exist_ok=True)

        args = []

        # If conda (python) is used, there might be conflicting libraries.
        # Therefore, force cmake to ignore this directory.
        if "CONDA_PREFIX" in os.environ:
            args.append(os.path.expandvars("-DCMAKE_IGNORE_PATH=$CONDA_PREFIX"))

        # merge local and global cmake generator
        try:
            generator = self.ctx.ws_config.global_.prepare.cmake.generator
        except AttributeError:
            generator = {}
        self.generator = {**generator, **self.generator}

        if self.generator and "name" in self.generator:
            args.append(f'-G "{self.generator["name"]}"')

        # merge cmake definitions
        try:
            definitions = self.ctx.ws_config.global_.prepare.cmake.definitions
        except AttributeError:
            definitions = {}
        definitions = {**definitions, **self.definitions}

        for key, value in definitions.items():
            prefix = "-D"
            if key.startswith(prefix):
                key = key[len(prefix):]
            value = os.path.expandvars(value)
            args.append(f'{prefix}{key}="{value}"')

        with env_ctx.EnvironContext(self.ctx.env, verbose=verbose):
            args = " ".join(args)
            commands.run(f"cmake {args} ..", cwd=build_dir)

    def get_cmake_package_dir_var(self) -> Tuple[str, str]:
        return f"{self.project_name}_DIR", f"{self.ctx.build_dir}"

    def test(
            self,
            rerun_failed=False,
            output_on_failure=False,
    ) -> Optional[bool]:
        if os.path.isdir(self.ctx.build_dir):
            try:
                args = ["ctest"]
                args.append("--no-tests=ignore")
                if rerun_failed:
                    args.append("--rerun-failed")
                if output_on_failure:
                    args.append("--output-on-failure")
                commands.run(" ".join(args), cwd=self.ctx.build_dir)
            except error.CommandFailed:
                return False
            else:
                return True


@dc.dataclass
class Run(common.Run):
    pass

    def test(self, **kwargs):
        pass


@dc.dataclass
class Prepare(Hook):
    cmake: Optional[CMake] = None
    run: Optional[Run] = None
    python: Optional[py.Python] = None

    def __post_init__(self):
        from armarx_setup.core.util.dataclass import sanitize_type
        self.cmake = sanitize_type(self.cmake, CMake, ctx=self.ctx)
        self.run = sanitize_type(self.run, Run, ctx=self.ctx)
        self.python = sanitize_type(self.python, py.Python, ctx=self.ctx)

    def _get_sub_hooks(self) -> List["Hook"]:
        return [self.cmake, self.run, self.python]

    def test(self, **kwargs) -> Optional[bool]:
        for hook in self._get_sub_hooks():
            if hook is not None:
                return hook.test(**kwargs)


if __name__ == '__main__':
    from armarx_setup.core.workspace.env import EnvFile

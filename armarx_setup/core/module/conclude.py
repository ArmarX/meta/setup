import dataclasses as dc

from typing import List, Optional

from armarx_setup.core.module import common
from armarx_setup.core.module.context import Hook
from armarx_setup.core.util import dataclass


@dc.dataclass
class Conclude(Hook):

    run: Optional[common.Run] = None

    def __post_init__(self):
        self.run = dataclass.sanitize_type(self.run, common.Run, ctx=self.ctx)

    def _get_sub_hooks(self) -> List[Hook]:
        return [self.run]

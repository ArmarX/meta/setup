from armarx_setup import PACKAGE_ROOT, RELEASE_CHANNEL_STABLE
from armarx_setup.core.module import Module


class AxiiMockModule(Module):
    """
    Mock module of Axii itself.

    Intended for use in several commands, such as interacting with Git repositories etc.
    """

    class Ctx:
        def __init__(self):
            self.path = PACKAGE_ROOT

    class General:
        def __init__(self, ctx):
            self.ctx = ctx
            self.url = "https://git.h2t.iar.kit.edu/sw/armarx/meta/axii"

    class Git:
        def __init__(self, ctx):
            self.ctx = ctx
            self.ssh_url = "git@git.h2t.iar.kit.edu:sw/armarx/meta/axii.git"
            self.https_url = "https://git.h2t.iar.kit.edu/sw/armarx/meta/axii.git"
            self.default_checkout_ref = RELEASE_CHANNEL_STABLE

    class Update:
        def __init__(self, ctx):
            self.ctx = ctx
            self.git = AxiiMockModule.Git(ctx=self.ctx)

    def __init__(self):
        self.name = "Axii"
        self.ctx = self.Ctx()
        self.general = self.General(ctx=self.ctx)
        self.update = self.Update(ctx=self.ctx)


axii_module = AxiiMockModule()

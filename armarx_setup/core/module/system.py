import os

import dataclasses as dc

from typing import Dict, List, Optional

from armarx_setup import console
from armarx_setup.core import error
from armarx_setup.core.module import common
from armarx_setup.core.module.context import Hook
from armarx_setup.core.util import cli, commands, dataclass, prefix_color_map, environ_context


@dc.dataclass
class Run(Hook):

    command: Optional[str] = None
    script: Optional[str] = None

    cwd: Optional[str] = None

    def __call__(self, *args, **kwargs):
        with environ_context.EnvironContext(self.ctx.get_environment()):
            if self.command:
                self.run_command(*args, **kwargs)

            elif self.script:
                self.run_script(*args, **kwargs)

            else:
                raise TypeError("No run step configured.")


    def run_command(self):
        assert self.command
        module_text = prefix_color_map.module_to_colored_text(self.ctx.module_name)

        console.print(f"Run command '{self.command}' requested by module {module_text} "
                      f"{(f' in {self.cwd}' if self.cwd else '')}")
        commands.run(self.command, self.cwd)


    def run_script(self, ask_user=False):
        assert self.script
        module_text = prefix_color_map.module_to_colored_text(self.ctx.module_name)

        info_dir = os.path.dirname(self.ctx.module_info_path)
        absolute_path = os.path.join(info_dir, self.script)
        if os.path.isfile(absolute_path):
            def run():
                commands.run(absolute_path, cwd=self.cwd)

            if ask_user:
                from rich.panel import Panel
                with open(absolute_path, "r") as file:
                    lines = file.readlines()
                panel = Panel("".join(lines), title=absolute_path)

                console.print(f"The module {module_text} requests running the script '{self.script}'"
                              f"{(f' in {self.cwd}' if self.cwd else '')}:")
                console.print(panel)
                from armarx_setup.cli import common
                if common.input_confirm(f"Run '{absolute_path}'?", default=False):
                    run()

            else:
                console.print(f"Run script '{self.script}' requested by module {module_text} "
                              f"{(f' in {self.cwd}' if self.cwd else '')}")
                run()

        else:
            raise IOError(f"The module {module_text} requests running the script '{self.script}', "
                          f"but it does not exist at: '{absolute_path}'")


@dc.dataclass
class Packages(Hook):

    apt: List[str] = dc.field(default_factory=list)

    def check_apt_packages(self) -> Dict[str, List[str]]:
        from armarx_setup.core.util import system
        found = []
        missing = []

        for package in self.apt:
            if system.is_apt_package_installed(package):
                found.append(package)
            else:
                missing.append(package)
        return dict(found=found, missing=missing)


class Env(Hook):

    def __init__(self, ctx=None, **kwargs):
        super().__init__(ctx=ctx)

        self.env: Dict[str, str] = kwargs
        for k in self.env:
            v = self.env[k]

            if not (isinstance(k, str) and isinstance(v, str)):
                raise error.ArmarXSetupError(f"Invalid env entry: '{k}': '{v}'")

            self.env[k] = v

    def generate_env(self, env: "EnvFile"):
        for k, v in self.env.items():
            env.add(k, v, by='global variables', write=False)


@dc.dataclass
class System(Hook):

    run: Optional[Run] = None
    packages: Optional[Packages] = None
    env: Optional[Env] = None
    source: Optional[common.Source] = None

    def __post_init__(self):
        self.run = dataclass.sanitize_type(self.run, Run, ctx=self.ctx)
        self.packages = dataclass.sanitize_type(self.packages, Packages, ctx=self.ctx)
        self.env = dataclass.sanitize_type(self.env, Env, ctx=self.ctx)
        self.source = dataclass.sanitize_type(self.source, common.Source, ctx=self.ctx)

    def _get_sub_hooks(self) -> List[Hook]:
        return [self.run, self.packages, self.env, self.source]

    def check_apt_packages(self) -> Optional[Dict[str, List[str]]]:
        result: Optional[Dict[str, List[str]]] = None

        if self.packages and self.packages.apt:
            check = self.packages.check_apt_packages()
            if result is None:
                result = check
            else:
                for k, v in check.items():
                    result[k] += v

        return result


if __name__ == '__main__':
    from armarx_setup.core.workspace.env import EnvFile

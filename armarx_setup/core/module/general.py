import dataclasses as dc

from typing import Optional


from armarx_setup.core.util import dataclass
from armarx_setup.core.module.context import Hook


@dc.dataclass
class Database(Hook):

    for_namespace: str = ""


@dc.dataclass
class General(Hook):

    url: Optional[str] = None
    database: Optional[Database] = None

    def __post_init__(
            self,
            ):

        self.database = dataclass.sanitize_type(self.database, Database, ctx=self.ctx)

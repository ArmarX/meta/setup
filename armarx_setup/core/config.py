import os
import json

from xdg import xdg_config_home

from armarx_setup.core import error


class Config:

    def __init__(self):
        self.config_dir = os.path.join(xdg_config_home(), "axii")
        self.global_config_file_name = "global_config.json"
        self.global_config_file_path = os.path.join(self.config_dir, self.global_config_file_name)
        self.global_config = {
            "dev_mode": False,
            "shell_dev_mode": False,
            "disable_deprecated_commands": False,
            "default_workspace": ""
        }

        self.ensure_defaults()

    def ensure_defaults(self):
        os.makedirs(self.config_dir, exist_ok=True)

        # Global config.
        if not os.path.exists(self.global_config_file_path):
            with open(self.global_config_file_path, "w") as f:
                json.dump(self.global_config, f, indent=2)

    def load_global_config(self):
        with open(self.global_config_file_path, "r") as f:
            self.global_config = {**self.global_config, **json.load(f)}

    def store_global_config(self):
        with open(self.global_config_file_path, "w") as f:
            json.dump(self.global_config, f, indent=2)

    def set_global(self, name, value):
        type = self.get_type_for_variable(name)
        self.global_config[name] = self.cast_value_to_type(value, type)

    def unset_global(self, name):
        try:
            del self.global_config[name]
        except KeyError:
            raise error.NoSuchConfigVariable(name)

    def get_global(self, name):
        try:
            return self.global_config[name]
        except KeyError:
            raise error.NoSuchConfigVariable(name)

    @staticmethod
    def get_type_for_variable(name):
        """Special type overrides for specific, well-known variables."""
        type_map = {
            "dev_mode": bool,
            "shell_dev_mode": bool,
            "disable_deprecated_commands": bool,
            "default_workspace": str
        }
        return type_map.get(name, str)

    @staticmethod
    def cast_value_to_type(value, type):
        if type == bool:
            if isinstance(value, bool):
                return value
            elif isinstance(value, str):
                return value.lower() in ["true", "yes", "on", "1", "y"]
            elif isinstance(value, int):
                return value == 1
            else:
                return False
        return value


config = Config()
config.load_global_config()

import sys

from armarx_setup import console, PACKAGE_ROOT
from armarx_setup.core.git import repo_status
from armarx_setup.core.config import config as axii_config


def update(only_check: bool):
    from armarx_setup.self import self_update

    dev_mode = axii_config.get_global("dev_mode")

    if dev_mode:
        ref = repo_status(PACKAGE_ROOT, fetch=False).active_branch
        console.print(f"[b]Dev mode[/]: On '{ref}'.")

    console.print("Check for updates...")
    update_status: self_update.SelfUpdateStatus = self_update.check_for_updates()
    if update_status == self_update.SelfUpdateStatus.UP_TO_DATE:
        console.print("Axii is up-to-date.")
    elif update_status == self_update.SelfUpdateStatus.OUTDATED:
        console.print("Axii is out of date.")
        if only_check:
            console.print(f"Run '{sys.argv[0]} self-update' without the '--only-check' option to update.")

    # Actually update if applicable, except if `--only-check` was set.
    if not only_check:
        if update_status == self_update.SelfUpdateStatus.OUTDATED:
            console.print("Update code ...")
            self_update.update()
            console.print("Done updating code.")

            console.print("Update virtual environment ...")
            self_update.update_venv()

        else:
            console.print("No work to do.")


def change_release(release_channel):
    from armarx_setup import PACKAGE_ROOT
    from armarx_setup.core.util.commands import run

    run(f"git checkout {release_channel}", cwd=PACKAGE_ROOT, capture_output=True, quiet=True)

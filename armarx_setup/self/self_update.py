import enum
import os
import re
import subprocess


from armarx_setup import PACKAGE_ROOT, console
from armarx_setup.core.util import commands


class SelfUpdateStatus(enum.Enum):
    UP_TO_DATE = enum.auto()
    OUTDATED = enum.auto()


def check_for_updates() -> SelfUpdateStatus:
    out = commands.run('git fetch origin ; git status', cwd=PACKAGE_ROOT, capture_output=True, quiet=True)
    if re.search(r"Your branch is up to date with '.*'\.", out) is not None:
        return SelfUpdateStatus.UP_TO_DATE
    elif re.search(r"Your branch is behind '.*' by [0-9]+ commits?, and can be fast-forwarded\.", out) is not None:
        return SelfUpdateStatus.OUTDATED


def update():
    commands.run('git pull', cwd=PACKAGE_ROOT)


def find_venv() -> str:
    from armarx_setup import PACKAGE_ROOT

    for directory in [".venv", "venv"]:
        venv = os.path.join(PACKAGE_ROOT, directory)
        if os.path.isdir(venv):
            return venv

    out = subprocess.run("poetry env info --path", cwd=PACKAGE_ROOT, stdout=subprocess.PIPE)
    venv = out.stdout.decode()
    if os.path.isdir(venv):
        return venv
    else:
        return ""


def update_venv():
    venv_dir = find_venv()
    if venv_dir:
        python = os.path.join(venv_dir, "bin", "python")
        requirements = os.path.join(PACKAGE_ROOT, "requirements.txt")

        r = subprocess.run(f"{python} -m pip install --upgrade pip", shell=True, stdout=subprocess.PIPE)
        if r.returncode != 0:
            console.print("Failed to update pip.")
            return

        r = subprocess.run(f"{python} -m pip install -r {requirements}", shell=True, stdout=subprocess.PIPE)
        if r.returncode == 0:
            console.print("Updated environment.")
            return True
        else:
            console.print("Failed to update environment.")
            return False
    else:
        console.print("Could not find virtual environment.")
        return False


import os

from armarx_setup import PACKAGE_ROOT
from armarx_setup.core import error


shell_map = {
    "zsh": {
        "rc_file_path": "~",
        "rc_file_name": ".zshrc"
    },
    "bash": {
        "rc_file_path": "~",
        "rc_file_name": ".bashrc"
    }
}


def shell_list(auto_detect=True, zsh=False, bash=False):
    if not auto_detect and all(not shell for shell in [zsh, bash]):
        auto_detect = True

    if auto_detect:
        shell = os.environ.get("SHELL", "")
        if shell.endswith("/zsh"):
            zsh = True
        elif shell.endswith("/bash"):
            bash = True
        else:
            raise error.UnsupportedShell(shell)

    shells_to_install = []

    if zsh is True:
        shells_to_install.append("zsh")
    if bash is True:
        shells_to_install.append("bash")

    return shells_to_install


def get_install_axii_script_path():
    home = os.path.expanduser("~")
    abs_path = os.path.join(PACKAGE_ROOT, "scripts", "install_axii.sh")
    return abs_path.replace(home, "$HOME")


def is_snippet_installed(shell, rc_file_path, install_axii_script_path) -> bool:
    try:
        with open(rc_file_path) as f:
            rc_contents = f.read()
    except FileNotFoundError as e:
        raise error.NoShellRunComFile(shell=shell, run_com_file=rc_file_path)

    if f"source \"{install_axii_script_path}\"" in rc_contents:
        return True

    return False


def install_snippet_into(shell, rc_file_path, snippet):
    try:
        with open(rc_file_path, 'a') as f:
            for line in snippet:
                f.write(f"{line}\n")
    except FileNotFoundError:
        raise error.NoShellRunComFile(shell=shell, run_com_file=rc_file_path)


def get_rc_file_path(shell):
    rc_file_path = shell_map[shell]["rc_file_path"]
    rc_file_name = shell_map[shell]["rc_file_name"]
    return os.path.join(os.path.expanduser(rc_file_path), rc_file_name)


def get_installation_snippet(file_path):
    return [
        f"",
        f"# Install Axii.",
        f"if [ -f \"{file_path}\" ]; then",
        f"    source \"{file_path}\"",
        f"fi",
        f""
    ]

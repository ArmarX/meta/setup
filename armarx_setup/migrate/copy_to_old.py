import shutil


if __name__ == '__main__':
    import glob
    import os
    from armarx_setup import console
    from armarx_setup.core.module.module import MODULE_DATA_DIR

    paths = glob.glob(os.path.join(MODULE_DATA_DIR, "**", "*.json"))

    count = 0
    for path in paths:
        dest = path + ".old"
        print(f"- Copy '{path}' \n"
              f"    to '{dest}'")
        shutil.copy2(path, dest)
        count += 1

    console.print(f"Copied {count} module .json files.")

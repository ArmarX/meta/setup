from typing import Dict, List


def load_modules_data(paths: List[str]) -> Dict[str, Dict]:
    modules_data = dict()
    for path in paths:
        with open(path, "r") as file:
            try:
                data = json.load(file)
            except json.JSONDecodeError as e:
                console.print(f"\\[x] {path}: {e}")
            else:
                modules_data[path] = data

    return modules_data


if __name__ == '__main__':
    import glob
    import os
    import json
    from armarx_setup import console
    from armarx_setup.core.module.module import MODULE_DATA_DIR, Module

    paths = glob.glob(os.path.join(MODULE_DATA_DIR, "**", "*.json.old"))
    modules_data = load_modules_data(paths)
    console.print(f"Loaded {len(modules_data)} modules")

    for path, data in modules_data.items():
        path: str

        assert path.endswith(".old")
        path = path[:-len(".old")]

        if "apt_packages" in data:
            console.print(f"[b]{path}[/]")
            data["system"] = {
                "packages": {
                    "apt": data["apt_packages"]
                }
            }
            del data["apt_packages"]
            console.print(json.dumps(data, indent=2))

            with open(path, "w") as file:
                json.dump(data, file, indent=2)
                file.write("\n")

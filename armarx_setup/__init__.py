import os

from rich.console import Console

__version__ = "22.10.2.0"

SOURCES_ROOT = os.path.dirname(os.path.abspath(__file__))
PACKAGE_ROOT, APP_NAME = os.path.split(SOURCES_ROOT)

RELEASE_CHANNEL_STABLE = "stable"
RELEASE_CHANNEL_UNSTABLE = "main"

assert os.path.join(PACKAGE_ROOT, APP_NAME) == SOURCES_ROOT, \
    (f"\nSOURCES_ROOT: \t{SOURCES_ROOT}"
     f"\nPACKAGE_ROOT: \t{PACKAGE_ROOT}"
     f"\nAPP_NAME: \t{APP_NAME}\n")

"""The root path of the git repository."""
os.environ["ARMARX_SETUP"] = PACKAGE_ROOT

console = Console()

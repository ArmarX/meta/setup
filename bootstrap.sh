#!/bin/bash

# Default arguments.
shell="bash"
apt=false
dev=false

# Argument parsing.
while [[ $# -gt 0 ]]; do
  case $1 in
    --bash)
      shell="bash"
      shift
      ;;
    --zsh)
      shell="zsh"
      shift
      ;;
    --apt)
      apt=true
      shift
      ;;
    --dev)
      dev=true
      shift
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      #POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

axii_dir=$(dirname "$(readlink -f "$(pwd)"/$0)")
bin_name=axii


if $apt; then

  packages="git build-essential python3-venv libpython3-dev graphviz-dev libssl-dev libbz2-dev jq xclip"
  plotting_packages="graphviz libgraphviz-dev pkg-config"
  sudo apt install $packages $plotting_packages

fi


if ! dpkg -V python3-venv; then
  echo "Please install python3-venv:"
  echo "sudo apt install python3-venv"
  exit
fi

hline="--------------------------------------------------------------------------------------"

# Add SSH keys to known hosts.
known_hosts_source_file="$axii_dir/data/ssh/known_hosts"
known_hosts_target_file="$HOME/.ssh/known_hosts"

echo "$hline"
echo "Adding known hosts from '$known_hosts_source_file' to '$known_hosts_target_file' ..."
echo "$hline"
echo ""

# cat "$known_hosts_source_file" >> "$known_hosts_target_file"


venv_dir="$axii_dir/.venv"
# Setup venv.
echo "$hline"
echo "Create virtual environment in $venv_dir"
echo "$hline"
echo ""

python3 -m venv "$venv_dir"
source "$venv_dir/bin/activate"
pip install --upgrade pip
pip install wheel
pip install -r "$axii_dir/requirements.txt"
pip install -r "$axii_dir/plotting_requirements.txt"
if $dev; then
  pip install -r "$axii_dir/test_requirements.txt"
fi

echo ""
echo "$hline"
echo "Installing Axii by running 'axii self install --no-auto-detect --$shell'."
echo "$hline"
echo ""

"$axii_dir"/bin/"$bin_name" self install --no-auto-detect --$shell

echo ""
echo "$hline"
echo "Run axii --help"
echo "$hline"
echo ""

"$axii_dir"/bin/"$bin_name" --help

# Advanced Users' Documentation

## Extend the Built-In Module Database

The built-in module definitions are stored in [`data/modules`](data/modules). 
A module name is the path to a `.json` file (the module definition) 
relative to the root of the module database 
(see the [Module Author's Documentation](../module_authors) for more details).

You can extend the built-in module database by setting the environment variable `ARMARX_MODULES`, 
which is a colon (`:`) separated list of root directories in which to look for modules
(similar to how you can modify the `PATH` variable to find executables).
This can be done by adding lines like this to the `~/.bashrc`:

```bash
export ARMARX_MODULES="$HOME/armarx-modules:$ARMARX_MODULES"
```

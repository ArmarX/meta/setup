# Module Authors' Documentation

This page contains information relevant for authors of module definitions and module databases.

A **module definition** is a `.json` file in a module database.
Conversely, a **module database** is a directory containing module definitions, 
potentially in a nested directory structure.


## Module Databases

The built-in module database is [`data/modules`](data/modules).
In addition, Axii uses the environment variable `ARMARX_MODULES` to find additional module databases and the modules 
in them.
Similar to `PATH` and `PYTHONPATH`, `ARMARX_MODULES` is a colon-separated list of module databases 
(for example, `$HOME/my-module-db:$HOME/Projects/my-project/axii-module-db`).

When searching for a module definition, 
Axii checks each database and uses the first matching module definition it finds.
The order is as follows:
1. Databases in `ARMARX_MODULES`, from front to back.
2. The built-in module database `data/modules`.

This allows two use cases especially:
- Add new modules to Axii without changing the built-in database.
- Overwrite module definitions from the built-in database.


## Module Definitions

Module definitions are descriptive `.json` files which define 
how a module is fetched/updated, built, installed, etc, and which other modules it requires.

The full top-level structure of a module definition is as follows (all of these are optional):

```json
{
  "general": {...},
  
  "system": {...},
  "update": {...},
  "prepare": {...},
  "build": {...},
  "install": {...},
  "conclude": {...},
  
  "required_modules": {...},
  
}
```

- `general` can have general information, such as a project URL. It is mainly informative,
but can be used to create links in the terminal. 
- `required_modules` specifies which other modules this module requires (_aka_ depends on) 
and is used to build the module graph.
- The other entries refer to each step in an `upgrade`.

### Workspace Upgrade Procedure

When **upgrading** a workspace, the different steps of all modules are executed in a specific order:

![Upgrade Procedure](resources/steps.svg)

The vertical axis (rows) illustrates the modules in order of their flattened dependency chain
(from no dependencies to many dependencies).
The numbers and arrows indicate time.

1. Generate the **environment** (set environment variables, source files). 
   This step does not occur in the module definition, as it is derived from the other steps (e.g. install).  
2. Run the **system** steps for all modules (e.g., install system packages).
3. **Update** all modules (e.g., clone or pull Git repositories, download archives).
4. For each module:
   1. **Prepare** the module (e.g., run `cmake`, create Python virtual environments).
   2. **Build** the module (e.g., run `cmake --build`).
   3. **Install** the module (e.g., run `cmake --install`).
5. Run final **conclude** steps for all modules.

In other words, the main steps **prepare**, **build** and **install** are run _per-module_.
The steps **system** and **update** are run for all modules _in parallel_ to speed up the process.
The **conclude** step is run per-module, but only after all other steps have been run.


### JSON Comments

Axii treats all dictionary keys and (string) list elements starting with a `#` as comments
and removes them after loading before any further interpretation happens.

For example, the JSON file
```json
{
  "update": {...},
  "build": {...},
  
  "# Not necessary anymore": {...},
  "# install": {...},
  
  "required_modules": {
    "other_module_a": {},
    "# other_module_b": {}
  }
}
```
is interpreted as
```json
{
  "update": {...},
  "build": {...},
  
  "required_modules": {
    "other_module_a": {}
  }
}
```


### OS-Specific Definitions

To cover OS-specific differences, any value in a module definition can be written 
in a switch-case like syntax using the `axii.switch_os` key as follows:

- Dictionary value:
  ```json
  {
    "a": "A",
    "b": {
      "axii.switch_os": {
        "bionic": "ubuntu18-B",
        "focal": "ubuntu20-B",
        "jammy": "ubuntu22-B",
        "buster": "debian10-B",
        "default": "B"
      }
    },
    "c": "C"
  }
  ```
- List element:
  ```json
  [
    "A",
    {
      "axii.switch_os": {
        "bionic": "ubuntu18-B",
        "focal": "ubuntu20-B",
        "jammy": "ubuntu22-B",
        "buster": "debian10-B",
        "default": "B"
      }
    },
    "C"
  ]
  ```

The case names `bionic`, `focal`, etc. are
[Linux development codenames](https://wiki.ubuntu.com/DevelopmentCodeNames)
matching the output of `lsb_release -a`. 
For example, on Ubuntu 18.04:
```shell
$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 18.04.6 LTS
Release:	18.04
Codename:	bionic
```

On such a platform (`bionic`), the above JSON constructs would be interpreted as:
- Dictionary value:
  ```json
  {
    "a": "A",
    "b": "ubuntu18-B",
    "c": "C"
  }
  ```
- List element:
  ```json
  [
    "A",
    "ubuntu18-B",
    "C"
  ]
  ```

If the current platform's codename is not listed in an `axii.switch_os` block, 
`default` is used as fallback:
- Dictionary value:
  ```json
  {
    "a": "A",
    "b": "B",
    "c": "C"
  }
  ```
- List element:
  ```json
  [
    "A",
    "B",
    "C"
  ]
  ```

#### Nesting

These constructs can also be nested. For example:
```json
{
  "first": {
    "axii.switch_os": {
      "bionic": {
        "second": "ubuntu18"
      },
      "default": {
        "second": {
          "axii.switch_os": {
            "focal": "general/ubuntu20",
            "default": "general"
          }
        }
      }
    }
  }
}
```
are interpreted according to the platform's code name as:
- `bionic`:
  ```json
  {
    "first": {
      "second": "ubuntu18"
    }
  }
  ```
- `focal`:
  ```json
  {
    "first": {
      "second": "general/ubuntu20"
    }
  }
  ```
- `default`:
  ```json
  {
    "first": {
      "second": "general"
    }
  }
  ```

#### Common Use Cases

A few common use case for this syntax are defining OS-specific ...

- Git branches or tags:
  ```json
  {
    "update": {
      "git": {
        "url": "...",
        "default_checkout_ref": {
          "axii.switch_os": {
            "bionic": "ubuntu18",
            "focal": "ubuntu20",
            "jammy": "ubuntu22",
            "buster": "debian10",
            "default": "main"
          }
        }
      }
    }
  }
  ```

- requirements:
  ```json
  {
    "required_modules": {
      "axii.switch_os": {
        "bionic": {
          "apt/gcc-8": {}        
        },
        "focal": {
          "apt/gcc-9": {}        
        },
        "default": {
        }   
      }
    }
  }
  ```

- system packages:
  ```json
  {   
    "system": {
      "packages": {
        "apt": {
          "axii.switch_os": { 
            "focal": [
              "libnlopt-dev",
              "libnlopt-cxx-dev"
            ],
            "default": [
              "libnlopt-dev"
            ]
          }       
        }
      }
    }
  }
  ```

# Maintainers' Documentation

[[_TOC_]]

## Release Checklist

- [ ] Fix MR title of main-&gt;stable to actual release: "Release &lt;year&gt;.&lt;month&gt;.&lt;# release of month&gt;.&lt;# patch, usually 0&gt;"
- [ ] Scroll through changes, verify. Fix minor things if need be by committing on master
- [ ] Quickly verify basics work on main
- [ ] Bump version number
  - `axii self change-release main`
  - `axii self update`
  - Change version in `armarx_setup/__init__.py`
  - Verify: `axii --version`
  - Commit changes, push changes: `git commit -am "chore: Version bump." && git push`
- [ ] Merge main-&gt;stable 
  - `git checkout stable`
  - `git pull`
  - `git merge main --no-ff`
  - `git push`
  - `git checkout main`
  - `git merge stable`
  - `git push`
- [ ] Update testing branch until in active use: `git push origin stable:testing`
- [ ] Create a tag: `https://gitlab.com/ArmarX/meta/setup/-/tags/new`
  - Tag name = Version only, i.e., "yy.mm.r.p"
  - Create from = "stable"
  - Message = MR title, i.e., "Release yy.mm.r.p"
  - Release notes = MR description (remove empty sections)
  - "Create tag"
  - Keep tab open for URL later
- [ ] Write release notification
  - Copy the template:

  ```text
  Hi everybody,
  
  We just released Axii version <VERSION NUMBER>. You can just update via `axii self update`. 
  
  <OPTIONAL SPOTLIGHT>
  
  For more information and detailed update procedure, see the release notes [1].
  
  As always, if you experience any problems, feel free to create an issue [2] or contact us.
  
  Thanks to everyone who contributed with module definitions and updates.
  
  Best,
  Rainer and Christian
  
  [1] https://gitlab.com/ArmarX/meta/setup/-/tags/<VERSION NUMBER>
  [2] https://gitlab.com/ArmarX/meta/setup/-/issues/new
  ```

  - Paste it into a text editor and replace the variables.
  - Copy it into a new email.
    - To = "armarx@lists.kit.edu"
    - CC = Co-Maintainers
    - Subject = "Axii Release &lt;VERSION NUMBER&gt;"
    - Content => paste

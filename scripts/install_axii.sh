export _axii_installed=true

function _axii_echo()
{
    if $_axii_shell_dev_mode; then
        echo "$@"
    fi
}

function _axii_source_all()
{
    for f in "$1"/*; do
        source "$f"
    done
}

_AXII_INSTALL_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )"/.. &> /dev/null && pwd 2> /dev/null; )"
export _AXII_INSTALL_DIR
export PATH="$_AXII_INSTALL_DIR/bin:$PATH"

_axii_shell_dev_mode=false
if [ "$(_axii_fast_config global_config shell_dev_mode)" = "true" ]; then
    _axii_shell_dev_mode=true
fi

_axii_echo "Source Axii's shell integration (shell-agnostic)."
_axii_source_all "$_AXII_INSTALL_DIR/scripts/agnostic"

_axii_echo "Source all shell-specific integrations."
if [ -n "$ZSH_VERSION" ]; then
    _axii_source_all "$_AXII_INSTALL_DIR/scripts/zsh"
elif [ -n "$BASH_VERSION" ]; then
    _axii_source_all "$_AXII_INSTALL_DIR/scripts/bash"
else
    echo "Axii shell integration disabled due to unknown shell."
fi

_axii_echo "Query config for default workspace."
_axii_default_workspace=$(_axii_fast_config global_config default_workspace)
if [ "$_axii_default_workspace" != "" ]; then
    _axii_echo "Activate workspace $_axii_default_workspace."
    _axii_default_workspace_path="$(_axii_fast_config known_workspaces "$_axii_default_workspace" path)"
    if [ -f "$_axii_default_workspace_path/armarx-workspace.rc" ]; then
        source "$_axii_default_workspace_path/armarx-workspace.rc"
    fi
fi

function _axii_env_reset()
{
    _axii_echo "Reset overridden variables."
    for var value in ${(kv)_axii_env_changes}; do
        _axii_echo "Reset '$var' to '$value'."
        unset "_axii_env_changes[$var]"
        eval "export $var=\"$value\""
    done
    _axii_echo ""

    _axii_echo "Remove values to appended/prepended variables."
    for var value in ${(kv)_axii_env_removals_from_appended_vars}; do
        unset "_axii_env_removals_from_appended_vars[$var]"

        local old_ifs="$IFS"
        IFS=":"

        setopt sh_word_split
        for part in $value; do
            _axii_echo "Remove '$part' from '$var'."
            _axii_env_remove_from "$var" "$part"
        done
        unsetopt sh_word_split

        IFS="$old_ifs"
    done
    _axii_echo ""

    _axii_echo "Unset variables."
    for var ("$_axii_env_added_env_vars[@]"); do
        _axii_echo "Unset '$var'."
        unset $var
    done
    export _axii_env_added_env_vars=()
    _axii_echo ""
}

function _axii_env_remove_from()
{
    local var="${1}"
    local val="${2}"
    local -a vals=()

    local old_ifs="$IFS"
    IFS=":"

    setopt sh_word_split
    values="${(P)var}"
    for part in $values; do
        if [ "$part" != "$val" ]; then
            vals+=("$part")
        fi
    done
    unsetopt sh_word_split

    # Set variable to new value.
    eval "export ${var}=\"${vals[*]}\""

    IFS="$old_ifs"
}

function _axii_env_remove_later()
{
    local var="${1}"
    local val="${2}"

    # If key "$var" not in dict.
    if (( ! ${+_axii_env_removals_from_appended_vars[$var]} )); then
        _axii_env_removals_from_appended_vars[$var]="$val"
    else
        old_value=$_axii_env_removals_from_appended_vars[$var]
        _axii_env_removals_from_appended_vars[$var]="$old_value:$val"
    fi
}

function _axii_env_append_to()
{
    local var="${1}"
    local val="${2}"

    _axii_env_remove_later "$var" "$val"

    # Remove value if already added...
    _axii_env_remove_from "$var" "$val"

    # ... and append it then.
    if [ -n "${(P)var}" ]; then
        _axii_echo "Append '$val' to '$var'."
        eval "export $var=\"${(P)var}:${val}\""
    else
        _axii_echo "Set '$var' to '$val'."
        eval "export $var=\"${val}\""
    fi
}

function _axii_env_prepend_to()
{
    local var="${1}"
    local val="${2}"

    _axii_env_remove_later "$var" "$val"

    # Remove value if already added...
    _axii_env_remove_from "$var" "$val"

    # ... and prepend it then.
    if [ -n "${(P)var}" ]; then
        _axii_echo "Prepend '$val' to '$var'."
        eval "export $var=\"${val}:${(P)var}\""
    else
        _axii_echo "Set '$var' to '$val'."
        eval "export $var=\"${val}\""
    fi
}

# TODO: Remove. This is a legacy alias for migration. Added 2022-07-12.
function _axii_env_export()
{
    _axii_env_set "$1" "$2"
}

function _axii_env_set()
{
    local var=$1
    local old_value="${(P)var}"
    local new_value=$2
    local action="Re-export"

    # If key "$var" not in dict.
    if (( ! ${+_axii_env_changes[$var]} )); then
        if [[ -v "${var}" ]]; then
            _axii_echo "Backup old value of '$var' with value '$old_value'."
            _axii_env_changes[$var]=$old_value
        else
            _axii_env_changes[$var]="unset"
            _axii_env_added_env_vars+=($var)
            action="Export new"
        fi
    else
        _axii_echo "Already backed up '$var'."
    fi

    _axii_echo "$action variable '$var' with value '$new_value'."
    eval "export $var=\"$new_value\""
}

export _axii_env_added_env_vars=()
declare -gA _axii_env_changes
declare -gA _axii_env_removals_from_appended_vars=()

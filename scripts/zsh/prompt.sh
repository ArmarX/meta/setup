function _axii_shell_prompt()
{
    if [ -n "$ARMARX_WORKSPACE_NAME" ]; then
        echo -e "[▽ $ARMARX_WORKSPACE_NAME] "
    fi

    return $1
}

export PS1="\$(_axii_shell_prompt \$?)$PS1"

function _axii_auto_env_refresh()
{
    shcommand="$HOME/.cache/axii/cached_command.sh"
    shcommand_tmp="$shcommand~"

    if mv "$shcommand" "$shcommand_tmp" 2> /dev/null; then
        source "$shcommand_tmp"
        rm "$shcommand_tmp"
    fi

    if [ -f "$ARMARX_WORKSPACE/armarx-workspace.rc" ]; then
        source "$ARMARX_WORKSPACE/armarx-workspace.rc" 2> /dev/null 2>&1
    fi
}

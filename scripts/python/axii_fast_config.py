#!/usr/bin/env python3

try:
    import json
    import os
    import sys
    from xdg import xdg_config_home

    file = sys.argv[1]

    path = os.path.join(xdg_config_home(), "axii", f"{file}.json")

    with open(path, "r") as f:
        config = json.load(f)

    val = config[sys.argv[2]]
    for x in sys.argv[3:]:
        val = val[x]
    if isinstance(val, bool):
        print(f"{val}".lower())
    else:
        print(val)
except:
    sys.exit(1)

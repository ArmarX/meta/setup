function _axii_shell_prompt()
{
    if [ -n "$ARMARX_WORKSPACE_NAME" ]; then
        # Colors (e.g. \e[44m]) seem to cause issues with the prompt.
        # echo -e "\e[44m[▽ $ARMARX_WORKSPACE_NAME]\e[0m "
        echo -e "[▽ $ARMARX_WORKSPACE_NAME] "
    fi
}

export PS1="\$(_axii_shell_prompt)$PS1"

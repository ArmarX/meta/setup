from armarx_setup.core.util import json


def test_extend_empty():
    out = json.extend(
        base={},
        extension={})
    expected = {}
    assert out == expected


def test_extend():
    out = json.extend(
        base={
            "base list": [1, 2, 3],
            "extended list": ["A", "B"],
            "deep": {
                "one": 1,
                "two": 2
            }
        },
        extension={
            "extended list": ["C"],
            "deep": {
                "two": 22,
                "three": 3
            },
            "new": "new"
        })
    expected = {
        "base list": [1, 2, 3],
        "extended list": ["A", "B", "C"],
        "deep": {
            "one": 1,
            "two": 22,
            "three": 3
        },
        "new": "new"
    }
    assert out == expected, f"out: \n{out}\n expected: \n{expected}"

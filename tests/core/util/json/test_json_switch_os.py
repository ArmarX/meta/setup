import copy
import typing as ty


class TestData:

    def __init__(self):

        raw: ty.Dict[str, ty.Any] = {
            "one": "1",
            "two": {
                "axii.switch_os": {
                    "default": "2",
                    "bionic": "bionic2",
                    "focal": "focal2",
                    "jammy": "jammy2",
                    "buster": "buster2"
                },
            },
            "three": 3,
            "list": [
                "first",
                {
                    "axii.switch_os": {
                        "default": "second",
                        "jammy": "jammy_second",
                    }
                },
                "third",
                {
                    "axii.switch_os": {
                        "default": "fourth",
                        "bionic": "bionic_fourth"
                    }
                }
            ],
            "nested": {
                "axii.switch_os": {
                    "default": {
                        "a": "A",
                        "b": {
                            "axii.switch_os": {
                                "default": "default_default_b",
                                "bionic": "default_bionic_b"
                            }
                        }
                    },
                    "jammy": "jammy_nested",
                }
            },
            "optional_keys": {
                "always": {},
                "only_bionic": {
                    "axii.switch_os": {
                        "bionic": {},
                        "default": None
                    }
                },
                "not_on_bionic": {
                    "axii.switch_os": {
                        "bionic": None,
                        "default": {}
                    }
                }
            },
            "optional_list_entries": [
                "always",
                {
                    "axii.switch_os": {
                       "bionic": "only_bionic",
                       "default": None
                    }
                },
                {
                    "axii.switch_os": {
                        "bionic": None,
                        "default": "only_default"
                    }
                },
                "always 2"
            ]
        }


        expected_default = copy.deepcopy(raw)
        expected_default["two"] = "2"
        expected_default["list"][1] = "second"
        expected_default["list"][3] = "fourth"
        expected_default["nested"] = {
            "a": "A",
            "b": "default_default_b"
        }
        del expected_default["optional_keys"]["only_bionic"]
        expected_default["optional_keys"]["not_on_bionic"] = {}
        expected_default["optional_list_entries"][2] = "only_default"
        del expected_default["optional_list_entries"][1]

        expected_bionic = copy.deepcopy(raw)
        expected_bionic["two"] = "bionic2"
        expected_bionic["list"][1] = "second"
        expected_bionic["list"][3] = "bionic_fourth"
        expected_bionic["nested"] = {
            "a": "A",
            "b": "default_bionic_b"
        }
        expected_bionic["optional_keys"]["only_bionic"] = {}
        del expected_bionic["optional_keys"]["not_on_bionic"]
        expected_bionic["optional_list_entries"][1] = "only_bionic"
        del expected_bionic["optional_list_entries"][2]

        self.raw = raw
        self.expected = {
            "default": expected_default,
            "bionic": expected_bionic,
        }


def test_json_resolve_switch_os():
    from armarx_setup.core.util.json.switch_os import resolve_switch_os

    td = TestData()

    for os_name, expected in td.expected.items():
        got = resolve_switch_os(data=td.raw, os_name=os_name)
        assert got == expected

import pytest

from armarx_setup import console
from armarx_setup.core.module import Module
from armarx_setup.core.workspace import Workspace
from armarx_setup.core.util.json import switch_os

ws = Workspace(path="", name="")

os_info_bionic = switch_os.OsInfo(
    distributor_id="Ubuntu", description="Ubuntu 18.04", release="...",
    codename="bionic"
)
os_info_focal = switch_os.OsInfo(
    distributor_id="Ubuntu", description="Ubuntu 20.04", release="...",
    codename="focal"
)

module_name = "meta/default_compiler"

parameters = [
    (os_info_bionic, "$GCC_8", "$GXX_8", "apt/gxx-8"),
    (os_info_focal, "$GCC_9", "$GXX_9", "apt/gxx-9"),
]


@pytest.mark.parametrize("os_info, c_compiler, cxx_compiler, required", parameters)
def test_load_meta_default_compiler_bionic(
        os_info: switch_os.OsInfo,
        c_compiler: str,
        cxx_compiler: str,
        required: str,
):
    switch_os.os_info = os_info

    module = Module.load_info(module_name, ws=ws)
    assert module is not None
    # console.print(module)

    assert module.install is not None, module

    env = module.install.env.env
    # console.print(env)
    assert len(env) > 0, env

    assert env.get("AXII_DEFAULT_C_COMPILER", None) == c_compiler, env
    assert env.get("AXII_DEFAULT_CXX_COMPILER", None) == cxx_compiler, env

    assert required in module.required_modules, module.required_modules

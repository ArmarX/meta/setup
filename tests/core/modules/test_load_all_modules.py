import pytest

from armarx_setup import console
from armarx_setup.core.module import Module
from armarx_setup.core.workspace import Workspace


ws = Workspace(path="", name="")
module_names = (
    Module.get_all_module_names()
    # ["apt/h2t"]
)


@pytest.mark.parametrize("module_name", module_names)
def test_load_module(module_name: str):
    console.print(f"[b]Load '{module_name}'[/]")
    module = Module.load_info(module_name, ws=ws)
    assert module is not None

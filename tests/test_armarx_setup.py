from armarx_setup import __version__


def test_version():
    assert __version__ == "2022.06.2"

Language: Cpp
Standard: c++20

BasedOnStyle: llvm

PointerAlignment: Left

AccessModifierOffset: -4
# BreakConstructorInitializers: BeforeComma
# BreakBeforeParameter: EndsInComma
#ExperimentalAutoDetectBinPacking: true
ExperimentalAutoDetectBinPacking: true
Cpp11BracedListStyle: true

NamespaceIndentation: All

CommentPragmas: "^ q:"

ReflowComments: false

ColumnLimit: 100
SortIncludes: true

AllowShortEnumsOnASingleLine: false

#BreakBeforeBraces: GNU
BreakBeforeBraces: Allman
AllowShortIfStatementsOnASingleLine: false
IndentCaseLabels: true

# AlignConsecutiveAssignments: Consecutive
AlignTrailingComments: false

BinPackArguments: false
BinPackParameters: false

AllowAllParametersOfDeclarationOnNextLine: false

AllowAllConstructorInitializersOnNextLine: true
BreakConstructorInitializers: AfterColon
ConstructorInitializerAllOnOneLineOrOnePerLine: true

BreakInheritanceList: AfterColon

# ReturnTypeBreakingStyle: All
# ReturnTypeBreakingStyle: AllDefinitions
# BreakTemplateDeclarationsStyle: Yes

# breaks return type
AlwaysBreakAfterDefinitionReturnType: All

AllowShortBlocksOnASingleLine: false
AllowShortCaseLabelsOnASingleLine: false
AllowShortFunctionsOnASingleLine: false
AllowShortLoopsOnASingleLine: false

AlwaysBreakTemplateDeclarations: true

BreakBeforeTernaryOperators: true

#ContinuationIndentWidth: 8
IndentWidth: 4
#TabWidth: 4
UseTab: "Never"
MaxEmptyLinesToKeep: 2

AllowAllArgumentsOnNextLine: true

FixNamespaceComments: true
AlignAfterOpenBracket: Align

IncludeBlocks: Regroup

IncludeCategories:
  - Regex: "<wykobi.hpp>"
    Priority: 7
  - Regex: "^(<cartographer/)"
    Priority: 8
  - Regex: "<[[:alnum:,_].]+>"
    Priority: 3
  - Regex: "<[[:alnum:,_].]+>"
    Priority: 3
  - Regex: "<[[:alnum:].]+>"
    Priority: 3
  - Regex: "<[:alnum:]+>"
    Priority: 3
  - Regex: "^(<unordered_map>)"
    Priority: 3
  - Regex: "^(<type_traits>)"
    Priority: 3
  - Regex: "^(<boost/)"
    Priority: 4
  - Regex: "^(<Eigen/)"
    Priority: 5
  - Regex: "^(<pcl/)"
    Priority: 6
  - Regex: "^(<opencv2/)"
    Priority: 7
  - Regex: "^(<range/)"
    Priority: 40

  - Regex: "^(<IceUtil/)"
    Priority: 80
  - Regex: "^(<Ice/)"
    Priority: 80
  - Regex: "^(<VirtualRobot/)"
    Priority: 100
  - Regex: "^(<SimoxUtility/)"
    Priority: 100
  - Regex: '^((<|")armarx/)'
    Priority: 150
  - Regex: '^((<|")ArmarXCore/)'
    Priority: 200
  - Regex: '^((<|")ArmarXGui/)'
    Priority: 210
  - Regex: '^((<|")RobotAPI/)'
    Priority: 220
  - Regex: '^((<|")VisionX/)'
    Priority: 230
  - Regex: '^((<|")RobotComponents/)'
    Priority: 240
  - Regex: '^((<|")ArmarXSimulation/)'
    Priority: 250
  - Regex: '^((<|")RobotSkillTemplates/)'
    Priority: 260
  - Regex: '^((<|")mobile_manipulation/)'
    Priority: 300
  - Regex: '^((<|")ROBDEKON/)'
    Priority: 310
  # - Regex:           '^"(llvm|llvm-c|clang|clang-c)/'
  #   Priority:        2
  #   SortPriority:    2
  #   CaseSensitive:   true
  # - Regex:           '^(<|"(gtest|gmock|isl|json)/)'
  #   Priority:        3
  # # - Regex:           '^(<|"(Eigen)/)'
  # #   Priority:        3
  # - Regex:           '^<VirtualRobot/'
  #   Priority:        4
  # - Regex:           '.*'
  #   Priority:        1
  #   SortPriority:    0

#!/bin/bash

set -e

config_dir="$MODULE_CONFIG_RESOURCE_DIR"

default_format_file="$config_dir/default_clang_format"
armarx_format_file="$config_dir/armarx_clang_format"
armarx_integration_format_file="$config_dir/armarx_integration_clang_format"

default_tidy_file="$config_dir/default_clang_tidy"
armarx_tidy_file="$config_dir/armarx_clang_tidy"
armarx_integration_tidy_file="$config_dir/armarx_integration_clang_tidy"

default_clangd_file="$config_dir/clangd"

function check_file()
{
  file=$1
  if [[ ! -f "$file" ]]; then
    echo "The file '$file' does not exist. Has it been deleted or moved?"
  fi
}

check_file "$default_format_file"
check_file "$armarx_format_file"
check_file "$armarx_integration_format_file"
check_file "$default_tidy_file"
check_file "$armarx_tidy_file"
check_file "$armarx_integration_tidy_file"
check_file "$default_clangd_file"


function check_and_link()
{
  link_dir=$1
  link_filename=$2
  target_path=$3

  if [[ -d "$ARMARX_WORKSPACE/$link_dir" ]]; then
    ln -s -f "$target_path" "$ARMARX_WORKSPACE/$link_dir/$link_filename"
  fi
}

check_and_link "simox"                 ".clang-format"    "$default_format_file"
check_and_link "simox"                 ".clang-tidy"      "$default_tidy_file"
check_and_link "mmm"                   ".clang-format"    "$default_format_file"
check_and_link "mmm"                   ".clang-tidy"      "$default_tidy_file"
check_and_link "research"              ".clang-format"    "$default_format_file"
check_and_link "research"              ".clang-tidy"      "$default_tidy_file"

check_and_link "armarx"                ".clang-format"    "$armarx_format_file"
check_and_link "armarx"                ".clang-tidy"      "$armarx_tidy_file"

check_and_link "armarx_integration"    ".clang-format"    "$armarx_integration_format_file"
check_and_link "armarx_integration"    ".clang-tidy"      "$armarx_integration_tidy_file"

check_and_link "."    ".clangd"      "$default_clangd_file"

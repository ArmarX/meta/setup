#! /bin/bash

# Creates a symlink in SpeechX / armar_tts to this repository.

clone_path="$h2t__data__tts_caches__PATH"
symlink_path="$SpeechX_DIR/../python/armar_tts/armar_tts/caches"

# Handle existing directory.
if [[ -d "$symlink_path" ]]; then
  mv "$symlink_path" "$symlink_path-old"
fi
# Handle existing symlink (be idempotent).
if [[ -h "$symlink_path" ]]; then
  rm "$symlink_path"
fi

# Create the link.
ln -s "$clone_path" "$symlink_path"

#!/bin/bash

set -e

cuda_check_axii_configuration
cuda_cudnn_archives_path="$(axii config --global cuda_cudnn_archives_path)"

install_dir="$MODULE_PATH"
cuda_installer="$cuda_cudnn_archives_path/cuda_11.2.2_460.32.03_linux.run"
cudnn_pkg="$cuda_cudnn_archives_path/cudnn-11.2-linux-x64-v8.1.1.33.tgz"

# Flag-files to indicate installation status.
cuda_done="$install_dir/installed_cuda"
cudnn_done="$install_dir/installed_cudnn"

mkdir -p "$install_dir"

if [[ ! -f "$cuda_done" ]]; then
  echo "Installing CUDA 11.2 ('$cuda_installer') to '$install_dir'.  This might take several minutes."
  sh "$cuda_installer" --silent --defaultroot="$install_dir" --toolkit --toolkitpath="$install_dir" --no-man-page --override
  # Install twice to make sure symlinks work correctly...
  echo "Verifying CUDA 11.2 installation.  This might take several minutes."
  sh "$cuda_installer" --silent --defaultroot="$install_dir" --toolkit --toolkitpath="$install_dir" --no-man-page --override && touch "$cuda_done"
  # Some library files are not put in the correct folder, so copy them manually
  cp -r "$install_dir/targets/x86_64-linux/lib/"* "$install_dir/lib64" 2> /dev/null || true
  echo "Done."
else
  echo "CUDA 11.2 already installed in '$install_dir'."
fi

if [[ ! -f "$cudnn_done" ]]; then
  echo "Installing cuDNN 8.1 ('$cudnn_pkg') to '$install_dir'."
  tar -xf "$cudnn_pkg" --strip-components=1 -C "$install_dir" && touch "$cudnn_done"
  echo "Done."
else
  echo "cuDNN already installed in '$install_dir'."
fi

#!/bin/bash

source_url="https://raw.githubusercontent.com/microsoft/Azure-Kinect-Sensor-SDK/v1.4.1/scripts/99-k4a.rules"
destination_path="/etc/udev/rules.d/99-k4a.rules"

host="$(hostname)"

if [[ "$host" == "armar6a-3" ]]; then
  echo "Skip installing Azure Kinect udev rules for ARMAR-6 vision PC."
  exit
fi

if [[ ! -f "$destination_path" ]]; then
  echo "Installing needed udev rules for the Azure Kinect. Running:"
  echo "sudo wget \"$source_url\" -O \"$destination_path\""
  read -p "Do you want to proceed? y/N " -r
  echo  # new line

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    sudo wget "$source_url" -O "$destination_path"
  fi
fi

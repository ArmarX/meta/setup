#!/usr/bin/env bash

version="3.5"
filename="Godot_v$version-stable_export_templates.tpz"
url="https://downloads.tuxfamily.org/godotengine/$version/$filename"

extracted_dir="templates"
trg_dir="$HOME/.local/share/godot/templates/$version.stable/"


# Check if target directory (1) does not exist or (2) is empty.
if [[ ! -d "$trg_dir" || -z "$(ls -A "$trg_dir")" ]]; then

  cd "$MODULE_PATH" || exit

  # Done by update step.
  # echo "Download Godot export templates ..."
  # wget "$url"

  # Check if extracted files need to be re-extracted.
  # We could delete the $extracted_dir to make Axii extract the archive when
  # required, but then the user would always have multiple copies of these
  # files (and they are about 700 MB).
  if [[ ! -d "$extracted_dir" || -z "$(ls -A "$extracted_dir")" ]]; then
     # Yes, it actually is a zip file.
     echo "Extract Godot export templates ..."
     unzip "$filename"
  fi

  mkdir -p "$trg_dir"

  # We could also copy the files and avoid the re-extraction logic,
  # but then the user would always have a second copy of these files.
  echo "Install Godot export templates ..."
  mv templates/* "$trg_dir"

  echo "Installed Godot export templates."

else
  # echo "Godot export templates are already installed."
  :

fi

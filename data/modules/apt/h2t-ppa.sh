#!/bin/bash

directory="/etc/apt/sources.list.d"
file="$directory/h2t.list"
alt="$directory/h2t_repo.list"

if [[ ! -e "$file" ]] && [[ ! -e "$alt" ]]; then
  echo "This will add the H2T package server to your system."
  read -p "Do you want to proceed? y/N " -r
  echo  # new line

  if [[ $REPLY =~ ^[Yy]$ ]]; then
    # --no-check-certificate: Temporary fix for expired certificate
    wget -O /tmp/h2t-key.pub https://packages.humanoids.kit.edu/h2t-key.pub --no-check-certificate
    sudo apt-key add /tmp/h2t-key.pub
    rm /tmp/h2t-key.pub

    echo -e "deb http://packages.humanoids.kit.edu/bionic/main bionic main\ndeb http://packages.humanoids.kit.edu/bionic/testing bionic testing" | sudo tee "$file" >/dev/null
    sudo apt update
  else
    echo "Aborted."
    exit 1
  fi
# else
  # echo "h2t-ppa already added."
fi

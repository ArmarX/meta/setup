#! /bin/bash

directory="/etc/apt/sources.list.d"
file="$directory/ros-latest.list"

if [[ ! -e "$file" ]]; then

  wget -O /tmp/ros-key.pub https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc
  sudo apt-key add /tmp/ros-key.pub
  rm /tmp/ros-key.pub

  echo -e "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" | sudo tee "$file" >/dev/null

  sudo apt update

else
  echo "ros-ppa already added."

fi

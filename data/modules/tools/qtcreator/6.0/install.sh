#!/bin/bash

set -e

_axii_run_once "$MODULE_PATH/qt-creator-opensource-linux-x86_64-6.0.2.run" "$MODULE_PATH/install" \
               --accept-obligations --accept-licenses --confirm-command \
               --root "$MODULE_PATH/install" \
               --email "humanoids.kit@gmail.com" --password "a9a17bce-730f-4cb7-8552-ca9ffa220cde" --no-save-account \
               install qt.tools.qtcreator

qtproject_source="$MODULE_CONFIG_RESOURCE_DIR/defaultconfig/QtProject"
qtproject_target="$ARMARX_WORKSPACE/.axii/config/qtcreator-6.0-default/QtProject"
qtcreator_folder="qtcreator"

#_axii_install_file "$qtproject_source" "$qtproject_target" "QtCreator.ini"
#_axii_install_file "$qtproject_source/$qtcreator_folder" "$qtproject_target/$qtcreator_folder" "cmaketools.xml"
#_axii_install_file "$qtproject_source/$qtcreator_folder" "$qtproject_target/$qtcreator_folder" "profiles.xml"

#!/bin/bash

set -e

source_dir="$MODULE_CONFIG_RESOURCE_DIR"
dest_dir="/usr/local/sbin"

activate_smt_source="$source_dir/activate_smt"
deactivate_smt_source="$source_dir/deactivate_smt"

activate_smt_dest="$dest_dir/activate_smt"
deactivate_smt_dest="$dest_dir/deactivate_smt"

function check_needs_install()
{
  src="$1"
  dest="$2"

  if [[ ! -f "$dest" ]]; then
    return 0
  fi

  src_hash=$(md5sum "$src" | awk '{ print $1 }')
  dest_hash=$(md5sum "$dest" | awk '{ print $1 }')

  if [[ "$src_hash" != "$dest_hash" ]]; then
    return 0
  fi

  return 1
}

function install_file()
{
  src="$1"
  dest="$2"
  label="$3"
  dry_run="$4"

  echo "Install/upgrade $label executable by running 'sudo cp \"$src\" \"$dest\"'."
  if [[ "$dry_run" != "true" ]]; then
    sudo cp "$src" "$dest"
  fi
}

install_activate=false
if check_needs_install "$activate_smt_source" "$activate_smt_dest"; then
  install_activate=true
fi

install_deactivate=false
if check_needs_install "$deactivate_smt_source" "$deactivate_smt_dest"; then
  install_deactivate=true
fi

if $install_activate || $install_deactivate; then

  echo "Need do perform the following steps which require sudo:"

  if $install_activate; then
    install_file "$activate_smt_source" "$activate_smt_dest" "activate_smt" true
  fi

  if $install_deactivate; then
    install_file "$deactivate_smt_source" "$deactivate_smt_dest" "deactivate_smt" true
  fi

  read -p "Is this okay? y/N: " -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "Acquire sudo permissions..."
    sudo echo "Sudo permissions acquired."

    if $install_activate; then
      install_file "$activate_smt_source" "$activate_smt_dest" "activate SMT"
    fi

    if $install_deactivate; then
      install_file "$deactivate_smt_source" "$deactivate_smt_dest" "deactivate SMT"
    fi
  else
    echo "Scripts not installed"
  fi

else

  echo "SMT executables are up-to-date."

fi


shell=$(basename "$SHELL")
bash_file="$GIT_BASH_COMPLETION_PATH"

if [[ "$shell" == "bash" && -f "$bash_file" ]]; then
  source "$bash_file"
fi

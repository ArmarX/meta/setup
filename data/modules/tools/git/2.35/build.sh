#!/bin/bash

set -e

source_dir="$MODULE_PATH/git-2.35.1"
install_dir="$MODULE_PATH/install"

mkdir -p "$install_dir" || true

configured_git="$install_dir/configured_git"

cd "$source_dir"

if [[ ! -f "$configured_git" ]]; then
  make configure
  ./configure --prefix="$install_dir" && touch "$configured_git"
  make all
  make install
else
  make all > /dev/null
  make install > /dev/null
fi




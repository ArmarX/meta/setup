#!/usr/bin/env bash

executable="$MODULE_PATH/Godot_v3.5-stable_x11.64"

chmod u+x "$executable"
mkdir -p bin
ln -s -f "$executable" bin/godot

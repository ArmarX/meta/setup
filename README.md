<div align="center">
<p><strong>Welcome to <em>Axii</em></strong></p>
<img src="./resources/axii.svg" width="250" height="250">
</div>


# Axii: The ArmarX Integrated Installer

Axii is the setup and maintenance tool for [ArmarX](https://gitlab.com/ArmarX) and its dependencies.


# Documentation

Keep scrolling for general information and usage.
Detailed documentation can be found here:

- [For advanced users](docs/advanced_users)
- [For module authors](docs/module_authors)
- [For developers](docs/developers)
- [For maintainers](docs/maintainers)


# Installation

Clone the repository, e.g. into `~/axii`, using either (1) or (2):
```bash
cd ~  # Or another directory of your choice.

# (1) With SSH access (if you set up SSH with your GitLab account):
git clone -b stable git@gitlab.com:ArmarX/meta/setup.git axii
# (2) Without SSH access:
git clone -b stable https://gitlab.com/ArmarX/meta/setup.git axii

cd axii
```

Run `bootstraph.sh`, see (1) or (2). 
This sets up a virtual python environment `.venv` 
and makes the `axii` command globally available if you follow the suggested instructions:

```bash
# (1) Installation without root access.
./bootstrap.sh
# (2) Installation plus required packages (requires root).
./bootstrap.sh --apt
```

> If you want to install Axii for zsh, run `./bootstrap.sh --zsh` instead. Other shells are currently not supported.

To **update** Axii itself, run (should be done regularly):
```bash
axii self update
```

After updating Axii, please also check the [release notes](https://gitlab.com/ArmarX/meta/setup/-/releases).


# Getting Started

To get going, create a new workspace:
```bash
axii workspace create
```
Unless you know what you do, choose **yes** when you are asked about modifying the `~/.bashrc`.
Afterwards,  **open a new console**.

To run the setup, **upgrade** your workspace:
```bash
axii workspace upgrade
```

By default, this will install the ArmarX framework packages and their dependencies.

For more details, see _Usage_ below.


# Usage

See which commands are available:
```bash
axii --help
```


## Create a Workspace

Create a new workspace:
```bash
axii workspace create
```
This creates a workspace directory at the location of your choice. During initialization, you will also be asked to supply a human-readable name for the workspace you are creating. This can later be used quickly activate an Axii workspace from everywhere.

The created workspace directory contains two files:
- `armarx-workspace.json`: The workspace configuration file
- `armarx-workspace.rc`: The workspace environment file

The **configuration** file `armarx-workspace.json` defines your workspace.
For example, it specifies the added modules and environment variables 
(more on that later in _Manage a Workspace_).

To **activate** a workspace, you can run:
```bash
axii workspace activate <human-readable-workspace-name>
```
Alternatively, the **environment** file `armarx-workspace.rc` can be sourced to activate your workspace. This can be handy if you want to auto-activate a workspace without changing into the workspace as `axii activate ...` would do. Note sourcing the workspace is only safe after sourcing the Axii installation script.

If you chose to source the created `armarx-workspace.rc` in your `~/.bashrc` during the workspace initialization
(recommended for starters),
open a new console or run `source ~/.bashrc` for this to take effect.

You can see **which** workspace is active by running:
```bash
axii workspace which
```


## Manage a Workspace

### Introducing Modules and the Module Graph

Axii is based on **_modules_**.

Let's start with an example:
- There is a module `armarx/ArmarXCore` which clones, configures, builds and installs 
the ArmarX package `ArmarXCore`.
- In addition, the module `armarx/ArmarXCore` depends on, or _requires_, another module named
`simox`, which clones, configures, builds and installs the standalone CMake package `Simox`.
- The module `simox`, in turn, requires the module `apt/eigen3`, which installs 
the linear algebra library `Eigen` via a system package.

This dependency chain can be represented by a directed (acyclic) graph, 
where modules are nodes and dependencies build edges,
```
(armarx/ArmarXCore) --> (simox) --> (apt/eigen3)
```
or, visually:

![Module Graph](resources/module-graph.svg)

This **_module graph_** is the heart of Axii.
In general, modules can clone repositories, build projects, install system packages, and more.
Most importantly, modules have dependencies to other modules (their _required modules_).
They build the module graph, which represents a program of how your workspace should be set up. 

Apropos: Your workspace specifies one or several modules it would like to have installed
in its config file `armarx-workspace.json`.
These modules are added to the module graph, as well as their dependencies, 
and their dependencies, and so on.

You can even visualize the active module graph by running 
(optional arguments in `[]`):
```bash
axii workspace info --graph [--reduce] [--all]
```

By the way, the module name, e.g. `armarx/ArmarXCore`, is also the path inside your workspace
where a module representing a code base will be cloned or downloaded to 
(e.g. `~/code/armarx/ArmarXCore` if your workspace is `~/code`).
At the same time, they are paths to `.json` files in the 
[`data/modules`](https://gitlab.com/ArmarX/meta/setup/-/tree/main/data/modules) directory,
which specifies what the module does. 


### Managing Workspace Modules

You can modify the workspace by **adding** or **removing** modules:
```bash
axii workspace add module/name
axii workspace remove module/name

# Examples:
axii workspace add armarx/ArmarXCore
axii workspace add armarx_integration/robots/armar6
```

Modules may provide **features**. 
Features are optional dependencies that can be explicitly requested 
by the workspace or other modules.
To **add a feature** of a module in the workspace:
```bash
axii workspace add-feature module/name feature-name-a feature-name-b

# Example:
axii workspace add-feature armarx/VisionX azure-kinect openpose
```

To **list** the modules added to the workspace:
```bash
# List just the added modules:
axii workspace list-modules

# List the added modules and their direct and indirect dependencies.
axii workspace list-modules --deps

# List all modules in the database (independent of the workspace):
axii module list
```

To **search** for a module, you can do:
```bash
axii module list | grep -i "search term"
```


To show detailed **info**rmation about the current workspace:
```bash
axii workspace info

# To include pure system modules:
axii workspace info --system

# To plot the module graph (requires additional packages, see setup below):
axii workspace info --graph
axii workspace info --graph --reduce  # See the transitively reduced module graph
```


## Upgrade a Workspace

The most important command is **upgrade**. Use it to
- update the shell **env**ironment,
- install **system** requirements such as apt packages (you are asked beforehand),
- **update** (clone, pull, download) repositories and archives,
- **prepare** (e.g., run cmake, setup python environments),
- **build** (e.g., run cmake --build), and
- **install**

all modules and their (direct and indirect) dependencies:
```bash
axii workspace upgrade
```

You can skip any of these steps, for example (see `axii upgrade --help`):
```bash
axii workspace upgrade --no-system --no-update
```

You can also directly run one of these steps:
```bash
axii workspace system
axii workspace update  # Add '--no-parallel' to update modules sequentially.
axii workspace prepare
axii workspace build
axii workspace install
```

All of these commands offer a set of `--options` you can use to 
specify the modules that should be affected (see `<cmd> --help`):
- `--module` (`-m`): Target the specified module (e.g. `armarx/ArmarXCore`)
- `--path` (`-p`): Target the module managing the given path 
  (e.g. `~/code/armarx/ArmarXCore`, or `ArmarXCore` if you are in `~/code/armarx/`)
- `--this` (`-t`, flag): Target the module managing the current directory (e.g. when inside `~/code/armarx/ArmarXCore`)
- `--no-deps` (`-n`, flag): Only operate the targeted module, but not its dependencies. 

Some useful examples are:
- `axii workspace build -tn`: Build only the module managing your current working directory.
- `axii workspace update -m armarx/ArmarXCore`: Clone or update only `armarx/ArmarXCore`. 
  This is especially useful when you added a module to the workspace and want to download it
  but not update the rest of your workspace. 


You can also **execute** a custom command in each module's directory inside the workspace
(if it exists):
```bash
axii workspace exec ls
axii workspace exec --in armarx "echo \$(pwd)"
axii workspace exec --git "git status"
```
For example, if you use [autojump](https://github.com/wting/autojump) 
and want to inform autojump about the module directories, you can run: 
```bash
axii workspace exec "autorun -i 1"
```
You can filter the modules the command is executed in by any combination of the following:
```bash
axii workspace exec --in prefix  # Only run in modules under the given prefix (prefix*).
axii workspace exec --exclude prefix  # Do not run in modules under the given prefix (prefix*).
axii workspace exec --git  # Only run in modules with a git update step.
```
In general, `axii exec` will only run the command in modules with existing directories 
(e.g., not for a module `apt/package`, which is not represented by a directory).

To get the **status** of each **git** repository in a nice table, run:
```bash
# Get a compact overview:
axii workspace git status
# See the full list of changes for each module:
axii workspace git status --files  
```


## The Local Workspace Config (`armarx-workspace.json`)

The local workspace config file
(`armarx-workspace.json` in your workspace directory)
contains the added **modules**
as well as **global** module settings that will be used
by all modules (if appropriate):

```json
{
  "modules": {
    ...
  },

  "global": {
    ...
  }
}
```

## Added Modules

The `modules` specify the modules which have been added to your workspace. 
It is usually modified by the `axii add` and `axii remove` commands.
In the configuration file, it is an attributed list like this:

```json
{
  "modules": {
    "simox": {},
    "armarx/ArmarXCore": {}
  }
}
```

The value (`{}` in the example) of each entry is usually an empty dictionary. 
However, it also can be used to achieve two things:

1. **Request a feature** from a module
2. **Overwrite** parts the module definition 


### Request a Feature

Requesting a feature from a module enables optional requirements of that module.
They are usually added using the `axii workspace add-features` command (see above).
In the workspace configuration, requested features look like this:

```json
{
  "modules": {
    "armarx/VisionX": {
      "features": [
        "openpose"
      ]
    }
  }
}
```

### Overwrite Parts of the Module Definition

The module entry's value can be used to overwrite parts of the module's definition
according to the module database(s).
You can overwrite virtually any parts of the module.

Common use cases are:

- Change the **path** where the module is located in your file system:
```json
{
  "modules": {
    "my-module": {
      "path": "$HOME/my/own/path-of/my-module"
    }
  }
}
```
- **Check out another branch** by default:
```json
{
  "modules": {
    "my-module": {
      "update": {
        "git": {
          "default_checkout_ref": "my-branch"
        }
      }
    }
  }
}
```
- Use a specific number of cores to build a module
  (`2` in the example; this takes precedence over the global module shown below):
```json
{
  "modules": {
    "my-module": {
      "build": {
        "cmake": {
          "env": {
            "CMAKE_BUILD_PARALLEL_LEVEL": "2"
          }
        }
      }
    }
  }
}
```

If you want to replace the whole module definition by another one, 
you may want to _Extend the Built-In Module Database_ (see below).


## Global Module Settings

At the moment, you can specify
* CMake definitions during the prepare step (`prepare/cmake/definitions/`)
* The CMake generator (`prepare/cmake/generator/`)
* Environment variables during the build step (`build/cmake/env`)
* Environment variables exported to the `armarx-workspace.rc` file (`install/env`)

If you need additonal settings,
[please create an issue](https://gitlab.com/ArmarX/meta/setup/-/issues/new).
```json
{
  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "KEY": "VALUE",
          ...
        },
        "generator": {
          "name": "Ninja" / "Unix Makefiles",
          "args": "..."
        }
      }
    },
    "build": {
      "cmake": {
        "env": {
          "KEY": "VALUE",
          ...
        }
      }
    },
    "install": {
      "env": {
        "KEY": "VALUE",
         ...
      }
    }
  }
}
```


### Parallel Build

To build with multiple cores, add this to your local workspace config
(replace `7` by the number of cores appropriate for your system):
```json
{
  "global": {
    "build": {
      "cmake": {
        "env": {
          "CMAKE_BUILD_PARALLEL_LEVEL": "7"
        }
      }
    }
  }
}
```

### Cmake Build Type

To enable debugging, set the build type to `RelWithDebInfo`:

```json
{
  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "CMAKE_BUILD_TYPE": "RelWithDebInfo"
        }
      }
    }
  }
}
```


### Disable Tests (ArmarX Projects Only)

To disable tests, add this to your local workspace config:

```json
{
  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "ARMARX_BUILD_TESTS": "OFF"
        }
      }
    }
  }
}
```


### Disable CMake's Global Registry

Some projects, such as all ArmarX packages, might register themselves in the global CMake registry 
found in `~/.cmake/packages`.
This however violates the concept of isolated workspaces.
All modules should expose the CMake project they contain by providing a `project_DIR` environment variable.
Therefore, it is advised to disable the global registration via the following:

```json
{
  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "CMAKE_EXPORT_NO_PACKAGE_REGISTRY": "ON"
        }
      }
    }
  }
}
```

### Use Ninja Instead of Unix Makefiles

To use Ninja as your default CMake generator, simply adjust your global workspace config like so:

```json
{
  "global": {
    "prepare": {
      "cmake": {
        "generator": {
          "name": "Ninja",
          "args": "-k9999"
        }
      }
    }
  }
}
```

Make sure that the apt package `ninja-build` is installed. 


#### Ninja Only: Colored Output

In contrast to `make`, the compiler output won't contain any colors if `ninja` is used.
To overcome this limitation, add the following to the workspace config:


```json
{
  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "FORCE_COLORED_OUTPUT": "ON",
          "ARMARX_CMAKE_ADDON_FORCE_COLORED_OUTPUT": "ON"
        }
      }
    }
  }
}
```


### Use Ccache

To make use of Ccache, depend on the `apt/ccache` module to export the `$CCACHE` variable,
and then set the corresponding CMake variables in the global workspace config as follows:

```json
{
  "modules": {
    "apt/ccache": {},
    ...
  },

  "global": {
    "prepare": {
      "cmake": {
        "definitions": {
          "CMAKE_C_COMPILER_LAUNCHER": "$CCACHE",
          "CMAKE_CXX_COMPILER_LAUNCHER": "$CCACHE"
        }
      }
    }
  }
}
```

(Optional) Define the cache size (example, 100GB) 
(after running `upgrade` or `system` once to install Ccache):
```bash
ccache -M 100G
```


# IDE integration

## VS Code

Define the build tasks in `~/.config/Code/User/tasks.json`. 
The following will build the project belonging to the currently opened file:

```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "label": "ax_build",
      "type": "shell",
      "command": [
        "axii build -n -f",
        "${file}"
      ],
      "problemMatcher": {
        "owner": "cpp",
        "fileLocation": [
          "relative",
          "${workspaceRoot}/build"
        ],
        "pattern": {
          "regexp": "^(.*):(\\d+):(\\d+):\\s+(warning|error):\\s+(.*)$",
          "file": 1,
          "line": 2,
          "column": 3,
          "severity": 4,
          "message": 5
        }
      },
      "group": {
        "kind": "build",
        "isDefault": true
      }
    }
  ]
}
```

You can also update your keybindings (`~/.config/Code/User/keybindings.json`)

```json
{
      "key": "ctrl+b",
      "command": "workbench.action.tasks.build"
}
```

Then just press Ctrl+B to trigger the build.
